#!/bin/bash

create_template_files() {
   uday="DAY${1}"
   #lday=`echo "$1" | tr '[:upper:]' '[:lower:]'`
   lday="day${1}"
   echo "Uday: $uday  Lday: $lday"

touch src/data/$lday.txt
touch src/data/$lday.sample.txt

cat > src/days/$lday.h <<EOF
#ifndef __${uday}_H__
#define __${uday}_H__

#include <string>
#include <vector>

struct Day$1Data {
   // TODO: Set data type
   typedef int Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day$1Part1 : public Day$1Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day$1Part2 : public Day$1Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day$1Spec {
   using Data_Spec = Day$1Data;
   using Part_1 = Day$1Part1;
   using Part_2 = Day$1Part2;
   static constexpr const char * day() { return "Day$1"; }
   static constexpr const char * data_file_name() { return "data/day$1.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day$1.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * expected_sample_part_2_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * solved_part_1_result() { return "undefined"; }
   static constexpr const char * solved_part_2_result() { return "undefined"; }
};

#endif // __${uday}_H__
EOF

cat > src/days/$lday.cc <<EOF
#include "$lday.h"
#include "spdlog/spdlog.h"
#include "../framework/solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day$1Spec>();
}

const bool _was_registered = _do_registration();

}

std::string Day$1Part1::name() const {
   return "Day$1-Part1";
}

std::string Day$1Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}


std::string Day$1Part2::name() const {
   return "Day$1-Part2";
}

std::string Day$1Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}

EOF

}



#
# Start of script
#

declare -a DAYS=(
   "01"
   "02"
   "03"
   "04"
   "05"
   "06"
   "07"
   "08"
   "09"
   "10"
   "11"
   "12"
   "13"
   "14"
   "15"
   "16"
   "17"
   "18"
   "19"
   "20"
   "21"
   "22"
   "23"
   "24"
   "25"
)

for day in ${DAYS[@]} ; do
   echo "Building template for ${day}"
   create_template_files $day
done