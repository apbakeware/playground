#include "types/day21_utils.h"

#include <catch2/catch_test_macros.hpp>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "utils/grid/grid.h"
#include "utils/grid/grid_location.h"
#include "utils/grid/grid_utils.h"

namespace {

const std::vector<std::string> PART_1_SAMPLE_LINES{
    "...........", ".....###.#.", ".###.##..#.", "..#.#...#..",
    "....#.#....", ".##..S####.", ".##..#...#.", ".......##..",
    ".##.#.####.", ".##..##.##.", "..........."};

}

TEST_CASE("Day21_Utils::build_iteration_grid", "[Day21_Utils]") {
  SECTION("test grid sizes") {
    const auto sut = Day21_Utils::build_iteration_grid(PART_1_SAMPLE_LINES);

    CHECK(sut.map_grid.number_of_rows() == 11);
    CHECK(sut.map_grid.number_of_cols() == 11);
    CHECK(sut.dist_grid.number_of_rows() == 11);
    CHECK(sut.dist_grid.number_of_cols() == 11);
  }

  SECTION("test staring location in queue") {
    const auto sut = Day21_Utils::build_iteration_grid(PART_1_SAMPLE_LINES);

    REQUIRE(sut.location_q.size() == 1);
    CHECK(sut.location_q.front().row == 5);
    CHECK(sut.location_q.front().col == 5);
  }

  SECTION("test distance grid initializes to 0") {
    const auto sut = Day21_Utils::build_iteration_grid(PART_1_SAMPLE_LINES);

    auto check_op = [](__attribute__((unused)) const auto &loc,
                       const auto val) { CHECK(val == 0); };

    sut.dist_grid.for_each_cell(check_op);
  }
}

TEST_CASE("Day21_Utils::walk_step", "[Day21_Utils]") {
  SECTION("Sample grid first step iteration") {
    auto sut = Day21_Utils::build_iteration_grid(PART_1_SAMPLE_LINES);

    auto new_locations = Day21_Utils::walk_step(sut, 1);

    CHECK(new_locations.size() == 2);
    // TODO: Assert locations?
  }

  SECTION("Sample grid two step iterations") {
    auto sut = Day21_Utils::build_iteration_grid(PART_1_SAMPLE_LINES);

    walk_iterations(sut, 2);

    // TODO: Assert locations?
  }

  SECTION("Sample grid 6 step iterations") {
    const unsigned int EXPECTED = 16;
    auto sut = Day21_Utils::build_iteration_grid(PART_1_SAMPLE_LINES);

    walk_iterations(sut, 6);

    unsigned int actual =
        Day21_Utils::determine_possible_destination_at_step(sut.dist_grid, 6);
    REQUIRE(actual == EXPECTED);
  }
}
