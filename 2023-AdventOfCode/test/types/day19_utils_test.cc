
#include "types/day19_utils.h"

#include <catch2/catch_test_macros.hpp>
#include <sstream>

#include "spdlog/spdlog.h"

namespace {

// Build an stringstream which can be used for parsing
// the Day19 sample input
std::stringstream create_sample_data_stream() {
  std::vector<std::string> LINES{"px{a<2006:qkq,m>2090:A,rfg}",
                                 "pv{a>1716:R,A}",
                                 "lnx{m>1548:A,A}",
                                 "rfg{s<537:gd,x>2440:R,A}",
                                 "qs{s>3448:A,lnx}",
                                 "qkq{x<1416:A,crn}",
                                 "crn{x>2662:A,R}",
                                 "in{s<1351:px,qqz}",
                                 "qqz{s>2770:qs,m<1801:hdj,R}",
                                 "gd{a>3333:R,R}",
                                 "hdj{m>838:A,pv}",
                                 "",
                                 "{x=787,m=2655,a=1222,s=2876}",
                                 "{x=1679,m=44,a=2067,s=496}",
                                 "{x=2036,m=264,a=79,s=2244}",
                                 "{x=2461,m=1339,a=466,s=291}",
                                 "{x=2127,m=1623,a=2188,s=1013}"};

  std::stringstream instr;

  for (const auto& line : LINES) {
    instr << line << "\n";
  }

  return instr;
}

}  // namespace

TEST_CASE("Day19_Utils::Rule", "[Day19_Utils]") {
  SECTION("Test evaluate") {
    const Day19_Utils::Rule_Result SUCCESS =
        Day19_Utils::Rule_Result::make_workflow_redirection_result("ABCD");
    const Day19_Utils::Rule_Result FAILURE =
        Day19_Utils::Rule_Result::make_indeterminate_result();

    const Day19_Utils::Part_Spec SPEC_X1{1, 2, 3, 4};
    const Day19_Utils::Part_Spec SPEC_X3{3, 2, 3, 4};

    const auto x_factor_accessor =
        [](const Day19_Utils::Part_Spec& spec) -> int { return spec.x_factor; };

    const auto eql_1_predicate = [](int value) -> bool { return value == 1; };

    const Day19_Utils::Rule SUT(x_factor_accessor, eql_1_predicate, SUCCESS,
                                FAILURE);

    const auto evaluation_spec_x1 = SUT.evaluate(SPEC_X1);
    const auto evaluation_spec_x3 = SUT.evaluate(SPEC_X3);

    CHECK(evaluation_spec_x1.get_resulting_workflow() ==
          SUCCESS.get_resulting_workflow());
    CHECK(evaluation_spec_x3.get_resulting_workflow() ==
          FAILURE.get_resulting_workflow());
  }

  SECTION("Test has accept condition") {
    using namespace Day19_Utils;
    CHECK(has_accept_condition_path(
              parse_workflow("px{a<2006:qkq,m>2090:A,rfg}")) == true);

    CHECK(has_accept_condition_path(parse_workflow("pv{a>1716:R,A}")) == true);

    CHECK(has_accept_condition_path(parse_workflow("lnx{m>1548:A,A}")) == true);

    CHECK(has_accept_condition_path(parse_workflow("in{s<1351:px,qqz}")) ==
          false);

    CHECK(has_accept_condition_path(
              parse_workflow("qqz{s>2770:qs,m<1801:hdj,R}")) == false);

    CHECK(has_accept_condition_path(parse_workflow("gd{a>3333:R,R}")) == false);
  }
}

TEST_CASE("Day19_Utils::parse_ methods", "[Day19_Utils]") {
  SECTION("parse_part_spec '{x=787,m=2655,a=1222,s=2876}'") {
    const std::string LINE = "{x=787,m=2655,a=1222,s=2876}";

    const auto& SUT = Day19_Utils::parse_part_spec(LINE);

    CHECK(SUT.x_factor == 787);
    CHECK(SUT.m_factor == 2655);
    CHECK(SUT.a_factor == 1222);
    CHECK(SUT.s_factor == 2876);
  }

  SECTION("parse_rule sample 1 fragment 'a<2006:qkq'") {
    const std::string LINE = "a<2006:qkq";

    const Day19_Utils::Part_Spec EXP_SUCCESS{-1, -1, 1985, -1};
    const Day19_Utils::Part_Spec EXP_FAILURE{-1, -1, 2006, -1};

    const auto sut = Day19_Utils::parse_rule(LINE);
    const auto exp_eval_success = sut.evaluate(EXP_SUCCESS);
    const auto exp_eval_failure = sut.evaluate(EXP_FAILURE);

    CHECK(exp_eval_success.is_workflow_redirection() == true);
    CHECK(exp_eval_success.get_resulting_workflow() == "qkq");
    CHECK(exp_eval_failure.is_indeterminate());
  }

  SECTION("parse_rule immediate workflow redirection") {
    const std::string LINE = "rfg";

    const Day19_Utils::Part_Spec EXP_SUCCESS{-1, -1, -1, -1};

    const auto sut = Day19_Utils::parse_rule(LINE);
    const auto exp_eval_success = sut.evaluate(EXP_SUCCESS);

    CHECK(exp_eval_success.is_workflow_redirection() == true);
    CHECK(exp_eval_success.get_resulting_workflow() == "rfg");
  }

  SECTION("parse_rule immediate accept") {
    const std::string LINE = "A";

    const Day19_Utils::Part_Spec EXP_SUCCESS{-1, -1, -1, -1};

    const auto sut = Day19_Utils::parse_rule(LINE);
    const auto exp_eval_success = sut.evaluate(EXP_SUCCESS);

    CHECK(exp_eval_success.is_accept() == true);
  }

  SECTION("parse_rule immediate reject") {
    const std::string LINE = "R";

    const Day19_Utils::Part_Spec EXP_SUCCESS{-1, -1, -1, -1};

    const auto sut = Day19_Utils::parse_rule(LINE);
    const auto exp_eval_success = sut.evaluate(EXP_SUCCESS);

    CHECK(exp_eval_success.is_reject() == true);
  }

  SECTION("parse_workflow 'px{a<2006:qkq,m>2090:A,rfg}'") {
    const std::string LINE = "px{a<2006:qkq,m>2090:A,rfg}";

    const auto SUT = Day19_Utils::parse_workflow(LINE);

    CHECK(SUT.name == "px");
    CHECK(SUT.rules.size() == 3);
  }
}

TEST_CASE("Day19_Utils::Day19_Workflow_Runner", "[Day19_Utils]") {
  SECTION("test sample data part number 1") {
    const bool EXP_ACCEPT = true;
    const std::vector<std::string> EXP_PATH{"in", "qqz", "qs", "lnx"};

    std::stringstream instr = create_sample_data_stream();
    Day19_Utils::Day19_Workflow_Runner sut;
    Day19_Utils::Part_Spec part{787, 2655, 1222, 2876};
    instr >> sut;
    const auto result = sut.execute_single_part(part);

    SPDLOG_DEBUG("Workflow sequence: {}",
                 fmt::join(result.workflow_path, "  "));

    CHECK(result.was_accepted == EXP_ACCEPT);
    CHECK(result.workflow_path == EXP_PATH);
  }

  SECTION("test sample data") {
    std::stringstream instr = create_sample_data_stream();
    Day19_Utils::Day19_Workflow_Runner sut;

    instr >> sut;
    const auto counts = sut.execute();

    CHECK(counts == 19114);
  }
}
