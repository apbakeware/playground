#include "types/day15_utils.h"

#include <algorithm>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

using namespace Catch::Matchers;

TEST_CASE("Test Day15_Utils.hash") {
  SECTION("test hash") {
    const std::string SUT = "HASH";
    const int EXPECTED = 52;

    REQUIRE(EXPECTED == Day15_Utils::hash(SUT));
  }
}

TEST_CASE("Test Day15_Utils.parse_instruction") {
  SECTION("test remove string") {
    const std::string SUT = "cm-";
    auto result = Day15_Utils::parse_instruction(SUT);

    CHECK(result.second == Day15_Utils::Operation::REMOVE);
    CHECK(result.first.label == "cm");
  }

  SECTION("test update string") {
    const std::string SUT = "rn=1";
    auto result = Day15_Utils::parse_instruction(SUT);

    CHECK(result.second == Day15_Utils::Operation::UPDATE);
    CHECK(result.first.label == "rn");
    CHECK(result.first.focal_length == 1);
  }
}

TEST_CASE("Test Day15_Utils.Lens_Box") {
  SECTION("test update with new label adds lens") {
    const Day15_Utils::Lens_Record lens_spec{"ABCD", 17};
    Day15_Utils::Lens_Box sut;

    sut.update(lens_spec);

    REQUIRE(sut.size() == 1);
    CHECK(*(sut.begin()) == lens_spec);
  }

  SECTION("test update with existing label is updated") {
    Day15_Utils::Lens_Record lens_spec{"ABCD", 17};
    Day15_Utils::Lens_Box sut;

    sut.update(lens_spec);
    lens_spec.focal_length = 28;
    sut.update(lens_spec);

    REQUIRE(sut.size() == 1);
    CHECK(*(sut.begin()) == lens_spec);
  }

  SECTION("test remove on empty is still empty") {
    Day15_Utils::Lens_Record lens_spec{"ABCD", 17};
    Day15_Utils::Lens_Box sut;

    sut.remove(lens_spec);

    REQUIRE(sut.size() == 0);
  }

  SECTION("test remove on non empty box when found") {
    const Day15_Utils::Lens_Record lens_spec{"ABCD", 17};
    Day15_Utils::Lens_Box sut;

    sut.update(lens_spec);
    sut.remove(lens_spec);

    REQUIRE(sut.size() == 0);
  }
}