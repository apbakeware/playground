#include "utils/grid/grid_distance.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <iostream>

#include "spdlog/spdlog.h"
#include "utils/grid/grid.h"
#include "utils/grid/grid_utils.h"

namespace {

// Create a test grid for testing
//
//  12345
//  ..-..
//  .[.].
//  ..+..
//  ABCDE
//

using Test_Grid_Type = Fixed_Sized_Grid<char>;

std::vector<std::string> grid_5x5_lines{"12345", "..-..", ".[X].", "..+..",
                                        "ABCDE"};

Test_Grid_Type create_test_grid() {
  return create_char_grid_from_string_rows(grid_5x5_lines.begin(),
                                           grid_5x5_lines.end());
}

constexpr int NOT_FOUND = -1;

}  // namespace

TEST_CASE("Test Grid distance_to", "[grid]") {
  SECTION("test distance_up_to does not find original location value") {
    const auto& grid = create_test_grid();
    const Grid_Location origin{2, 2};
    const char VALUE_TO_FIND = 'X';
    auto pred = [=](const char val) { return VALUE_TO_FIND == val; };

    int dist = distance_up_to(grid, origin, pred);
    REQUIRE(-1 == dist);
  }

  SECTION("test distance_up_to where predicate fails") {
    const auto& grid = create_test_grid();
    const Grid_Location origin{2, 2};
    const char VALUE_TO_FIND = '0';
    auto pred = [=](const char val) { return VALUE_TO_FIND == val; };

    int dist = distance_up_to(grid, origin, pred);
    REQUIRE(-1 == dist);
  }

  SECTION("test distance_up_to where predicate suceeds") {
    const auto& grid = create_test_grid();
    const Grid_Location origin{2, 2};
    const char VALUE_TO_FIND = '3';
    auto pred = [=](const char val) { return VALUE_TO_FIND == val; };

    int dist = distance_up_to(grid, origin, pred);
    REQUIRE(2 == dist);
  }

  SECTION("test distance_right_to where predicate fails") {
    const auto& grid = create_test_grid();
    const Grid_Location origin{2, 2};
    const char VALUE_TO_FIND = '0';
    auto pred = [=](const char val) { return VALUE_TO_FIND == val; };

    int dist = distance_right_to(grid, origin, pred);
    REQUIRE(-1 == dist);
  }

  SECTION("test distance_right_to where predicate suceeds") {
    const auto& grid = create_test_grid();
    const Grid_Location origin{2, 2};
    const char VALUE_TO_FIND = ']';
    auto pred = [=](const char val) { return VALUE_TO_FIND == val; };

    int dist = distance_right_to(grid, origin, pred);
    REQUIRE(1 == dist);
  }

  SECTION("test distance_down_to where predicate fails") {
    const auto& grid = create_test_grid();
    const Grid_Location origin{2, 2};
    const char VALUE_TO_FIND = '@';
    auto pred = [=](const char val) { return VALUE_TO_FIND == val; };

    int dist = distance_down_to(grid, origin, pred);
    REQUIRE(-1 == dist);
  }

  SECTION("test distance_down_to where predicate suceeds") {
    const auto& grid = create_test_grid();
    const Grid_Location origin{2, 2};
    const char VALUE_TO_FIND = '+';
    auto pred = [=](const char val) { return VALUE_TO_FIND == val; };

    int dist = distance_down_to(grid, origin, pred);
    REQUIRE(1 == dist);
  }

  SECTION("test distance_right_to where predicate fails") {
    const auto& grid = create_test_grid();
    const Grid_Location origin{2, 2};
    const char VALUE_TO_FIND = '0';
    auto pred = [=](const char val) { return VALUE_TO_FIND == val; };

    int dist = distance_right_to(grid, origin, pred);
    REQUIRE(-1 == dist);
  }

  SECTION("test distance_left_to where predicate suceeds") {
    const auto& grid = create_test_grid();
    const Grid_Location origin{2, 2};
    const char VALUE_TO_FIND = '[';
    auto pred = [=](const char val) { return VALUE_TO_FIND == val; };

    int dist = distance_left_to(grid, origin, pred);
    REQUIRE(1 == dist);
  }
}