#ifndef __LR_NAMED_GRAPH_PATH_FINDER_H__
#define __LR_NAMED_GRAPH_PATH_FINDER_H__

#include <stdexcept>
#include <string>
#include <vector>

#include "../utils/graph/named_route_graph.h"

// TODO: template collection type
class Cyclic_Direction_List {
 public:
  explicit Cyclic_Direction_List(const std::string& directions);

  char next();

 private:
  std::string _directions;
  std::string::const_iterator _iter;
};

// Execute a route
struct Lr_Named_Graph_Path_Finder {
  using LR_Named_Route_Graph_Type = Named_Route_Graph<2>;

  // Throws if graph cannot be traversed with directions
  static size_t distance_between(const LR_Named_Route_Graph_Type& graph,
                                 const std::string& start_node_label,
                                 const std::string& dest_node_label,
                                 Cyclic_Direction_List& directions);
};

struct Lr_Named_Graph_Concurrent_Path_Finder {
  using LR_Named_Route_Graph_Type = Named_Route_Graph<2>;

  static bool all_locations_end_with_z(
      const std::vector<std::string>& locations);

  // Throws if graph cannot be traversed with directions
  // TODO: make a predicate to test when all the locations are set
  static size_t distance_between(
      const LR_Named_Route_Graph_Type& graph,
      const std::vector<std::string>& starting_node_labels,
      Cyclic_Direction_List& directions);
};

#endif /* __LR_NAMED_GRAPH_PATH_FINDER_H__ */