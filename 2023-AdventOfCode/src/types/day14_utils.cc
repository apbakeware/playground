#include "day14_utils.h"

#include <queue>
#include <unordered_set>

#include "../utils/grid/grid_traversal.h"
#include "../utils/grid/grid_utils.h"

namespace Day14_Utils {

void do_tilt_up(Fixed_Sized_Grid<char>& grid, size_t col) {
  const auto grid_top = top_of_column(grid, col);

  // Test the grid_top.second...

  std::queue<Grid_Location> open_queue;

  auto tilter = [&](const Grid_Location& location, char value) {
    if (value == OPEN) {
      open_queue.push(location);
    } else if (value == ROUND_ROCK) {
      if (!open_queue.empty()) {
        grid.set(open_queue.front(), ROUND_ROCK);
        grid.set(location, OPEN);
        open_queue.pop();
        open_queue.push(location);
      }
    } else if (value == CUBE_ROCK) {
      while (!open_queue.empty()) {
        open_queue.pop();
      }
    }
  };

  walk_down(grid, grid_top.first, tilter);
}

void do_tilt_up(Fixed_Sized_Grid<char>& grid) {
  for (ssize_t col = 0; col < grid.number_of_cols(); col++) {
    do_tilt_up(grid, col);
  }
}

int compute_load(const Fixed_Sized_Grid<char>& grid) {
  int load = 0;

  auto load_computer = [&](const auto& loc, const char value) {
    load += value == ROUND_ROCK ? loc.row + 1 : 0;
  };

  grid.for_each_cell(load_computer);

  return load;
}

int tilt_and_evaluate_load(Fixed_Sized_Grid<char>& grid) {
  do_tilt_up(grid);
  return compute_load(grid);
}

void do_spin_cycle(Fixed_Sized_Grid<char>& grid) {
  // Test the grid_top.second...

  std::queue<Grid_Location> open_queue;

  auto tilter = [&](const Grid_Location& location, char value) {
    if (value == OPEN) {
      open_queue.push(location);
    } else if (value == ROUND_ROCK) {
      if (!open_queue.empty()) {
        grid.set(open_queue.front(), ROUND_ROCK);
        grid.set(location, OPEN);
        open_queue.pop();
        open_queue.push(location);
      }
    } else if (value == CUBE_ROCK) {
      while (!open_queue.empty()) {
        open_queue.pop();
      }
    }
  };

  for (ssize_t col = 0; col < grid.number_of_cols(); ++col) {
    open_queue = {};
    Grid_Location loc{grid.number_of_rows() - 1, col};
    walk_down(grid, loc, tilter);
  }

  for (ssize_t row = 0; row < grid.number_of_rows(); ++row) {
    open_queue = {};
    Grid_Location loc{row, 0};
    walk_right(grid, loc, tilter);
  }

  for (ssize_t col = 0; col < grid.number_of_cols(); ++col) {
    open_queue = {};
    Grid_Location loc{0, col};
    walk_up(grid, loc, tilter);
  }

  for (ssize_t row = 0; row < grid.number_of_rows(); ++row) {
    open_queue = {};
    Grid_Location loc{row, grid.number_of_cols() - 1};
    walk_left(grid, loc, tilter);
  }
}

int load_after_spin_cycles(Fixed_Sized_Grid<char>& grid,
                           unsigned long int cycles) {
  // Find a cycle within the motion.
  // Count the offset to the beginning of the cycle
  // Store the load for each element in the cycle
  //
  // Index the load list by (num_cycles - cycle start - 1) % loop_size

  std::string first_duplicate;
  std::unordered_set<std::string> grid_repr_states;
  unsigned long int start_of_loop_cycle = 0;
  std::vector<int> loop_weights;

  for (unsigned long int cycle = 0; cycle < cycles; ++cycle) {
    do_spin_cycle(grid);

    std::string grid_repr;
    grid.for_each_cell([&](__attribute__((unused)) const auto& loc,
                           const char val) { grid_repr += val; });

    auto insert_attempt = grid_repr_states.insert(grid_repr);
    if (!insert_attempt.second) {
      if (first_duplicate.empty()) {
        first_duplicate = grid_repr;
        start_of_loop_cycle = cycle;
        loop_weights.push_back(compute_load(grid));
      } else if (first_duplicate == grid_repr) {
        break;
      } else {
        loop_weights.push_back(compute_load(grid));
      }
    }
  }

  ssize_t weight_loop_index =
      (cycles - start_of_loop_cycle - 1) % loop_weights.size();

  return loop_weights.at(weight_loop_index);
}

}  // namespace Day14_Utils