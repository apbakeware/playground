#ifndef __DAY18_UTILS_H__
#define __DAY18_UTILS_H__

#include <functional>
#include <iosfwd>
#include <string>
#include <utility>
#include <vector>

#include "../utils/grid/grid.h"

namespace Day18_Utils {

struct Dig_Instructions {
  char direction;
  int distance;
  std::string color_code;
};

std::pair<char, int> decode_part2_color_string(const Dig_Instructions& obj);

std::istream& operator>>(std::istream& instr, Dig_Instructions& obj);

//////////////////////////////////////////////////////////////
// Dynamic grid not implemented so create
// a large size grid and use the center of it as the center
// of the dig site

using Dig_Grid_Type = Fixed_Sized_Grid<char>;

struct Dig_Site_Type {
  Dig_Grid_Type dig_grid;
  Grid_Location origin;
};

// The upper, left, right, and bottom most points
// in the dig.
struct Dig_Bounds {
  Grid_Location upper_most_location;
  Grid_Location left_most_location;
  Grid_Location lower_most_location;
  Grid_Location right_most_location;
};

struct Dig_Site_Frame_Of_Reference {
  ssize_t origin_row_offset;
  ssize_t origin_col_offset;
  ssize_t grid_row_height;
  ssize_t grid_col_width;
};

// Run the instruction set, decoding the direction and
// distance from the color code (Day18.2) to determine
// the extents of the dig site.
Dig_Bounds find_dig_bounds_using_decoded_color_code(
    const std::vector<Dig_Instructions>& instructions);

Dig_Site_Frame_Of_Reference create_dig_site_reference(const Dig_Bounds& bounds);

Dig_Site_Type create_dig_site(int grid_square_size);

// Detect off grid?
// day16 movement to grid_utils
// Return the ending location
Grid_Location dig_trench(Dig_Grid_Type& dig_grid,
                         const Dig_Instructions& instruction,
                         const Grid_Location& origin);

void dig_trench(Dig_Site_Type& dig_site,
                const std::vector<Dig_Instructions>& instructions,
                ssize_t origin_row_tanslation = 0,
                ssize_t origin_col_translation = 0);

/******************* Part 2 vertical trench approach ***********/

struct Line_Segment {
  Grid_Location start;
  Grid_Location end;
};

bool form_vertical_segment(const Grid_Location& first,
                           const Grid_Location& second);

std::vector<Line_Segment> find_vertical_trenches(
    const std::vector<Dig_Instructions>& instructions,
    std::function<std::pair<char, int>(Dig_Instructions)> move_decoder);

void sort_trenches(std::vector<Line_Segment>& vert_trenches);

void combine_close_loop(std::vector<Line_Segment>& vert_trenches);

bool vertical_segment_crosses_row(const Line_Segment& segment, ssize_t row);

}  // namespace Day18_Utils
#endif /* __DAY18_UTILS_H__ */