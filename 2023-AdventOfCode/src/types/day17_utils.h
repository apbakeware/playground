#ifndef __DAY17_UTILS_H__
#define __DAY17_UTILS_H__

#include <limits>
#include <vector>

#include "../utils/grid/grid.h"
#include "../utils/grid/grid_direction.h"
#include "../utils/grid/grid_location.h"

namespace Day17_Utils {

using Heat_Loss_Grid = Fixed_Sized_Grid<int>;
using Visitation_Grid = Fixed_Sized_Grid<bool>;
using Path_Value = Location_Value<Heat_Loss_Grid::value_type>;
using Path = std::vector<Path_Value>;
// using Path = std::vector<Heat_Loss_Grid::value_type>;

constexpr static int UNACCEPTABLE_MOVEMENT = std::numeric_limits<int>::max();

struct Origin_Destination_Location {
  Grid_Location origin;
  Grid_Location destination;
};

struct Grid_Movement {
  const Heat_Loss_Grid& grid;
  Visitation_Grid& visitation_grid;
  Path& path;
  Orthogonal_Direction::Direction prior_move_dir;
  int move_in_direction_count;
};

Origin_Destination_Location get_origin_and_destination_location(
    const Heat_Loss_Grid& grid);

Visitation_Grid create_visitation_grid(const Heat_Loss_Grid& grid);

// Greedy selection of next least cost node in the heat grid.
// No backtracking or repeat visits.
//
// This only results in a path with local minimal decisions,
// not the least total cost. Need to treat the grid as
// a graph with weighted path costs and then use Dijkstra's
// least path cost algorithm.
Path create_least_cost_path(const Heat_Loss_Grid& heat_grid,
                            const Origin_Destination_Location& endpoints);

int path_heat_const(const Path& path);

// Get the heat direction in the requested movement location.
// This methods checks:
//   * to make sure the location is on the grid
//   * the location has not been visited
int heat_value_if_unvisited_direction(
    const Heat_Loss_Grid& grid, const Visitation_Grid& visits,
    const Grid_Location& location, Orthogonal_Direction::Direction direction);

}  // namespace Day17_Utils

#endif /* __DAY17_UTILS_H__ */