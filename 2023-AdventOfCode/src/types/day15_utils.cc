#include "day15_utils.h"

#include <algorithm>

#include "spdlog/spdlog.h"
namespace Day15_Utils {

int hash(const std::string& value) {
  int result = 0;
  for (char ch : value) {
    result += static_cast<int>(ch);
    result *= 17;
    result = result % 256;  // divide by 256
  }

  return result;
}

bool operator==(const Lens_Record& first, const Lens_Record& second) {
  return first.focal_length == second.focal_length &&
         first.label == second.label;
}

std::pair<Lens_Record, Operation> parse_instruction(const std::string& field) {
  std::pair<Lens_Record, Operation> result{{"", 0}, Operation::UPDATE};

  size_t str_pos = field.find('-');
  if (str_pos != std::string::npos) {
    result.second = Operation::REMOVE;
    result.first.label = field.substr(0, str_pos);
  }

  // TODO: in else
  str_pos = field.find('=');
  if (str_pos != std::string::npos) {
    std::string numstr = field.substr(str_pos + 1);
    result.second = Operation::UPDATE;
    result.first.label = field.substr(0, str_pos);
    result.first.focal_length = stoi(numstr);
  }

  return result;
}

void Lens_Box::update(const Lens_Record& record) {
  auto pred = [&](const auto& obj) -> bool {
    return obj.label == record.label;
  };

  auto iter = std::find_if(lenses_.begin(), lenses_.end(), pred);

  if (iter == lenses_.end()) {
    lenses_.push_back(record);
  } else {
    iter->focal_length = record.focal_length;
  }
}

void Lens_Box::remove(const Lens_Record& record) {
  auto pred = [&](const auto& obj) -> bool {
    return obj.label == record.label;
  };

  lenses_.remove_if(pred);
}

int Lens_Box::focal_power() const {
  int lens_number = 1;
  int focal_power = 0;

  for (const auto& lens : lenses_) {
    focal_power += lens_number * lens.focal_length;
    ++lens_number;
  }

  return focal_power;
}

}  // namespace Day15_Utils