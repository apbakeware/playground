#ifndef __DAY16_UTILS_H__
#define __DAY16_UTILS_H__

#include <queue>
#include <set>
#include <string>
#include <vector>

#include "../utils/grid/grid.h"
#include "../utils/grid/grid_utils.h"

namespace Day16_Utils {

// TODO: move to grid_utils

enum class Movement_Direction {
  NONE = 0x0,
  UP = 0x1,
  RIGHT = 0x2,
  DOWN = 0x4,
  LEFT = 0x8
};

struct Grid_Mover {
  Grid_Location location;
  Movement_Direction direction;
  bool in_motion;

  operator bool() const { return in_motion; }
};

class Movement_Record {
 public:
  void record_movement(Movement_Direction direction);

  bool has_recorded_movement(Movement_Direction direction) const;

  // uint32_t value() const { return m_movement_record; }

 private:
  uint32_t m_movement_record;
};

using Beam_Grid_Type = Fixed_Sized_Grid<char>;
using Movement_Grid_Type = Fixed_Sized_Grid<Movement_Record>;

// Also the movement character to update
Beam_Grid_Type create_from_input_data(const std::vector<std::string>& input);

// return collection of next location as a result of moving onto the current
// location of type '.'.
// if the move ran off the grid, return the same location
// with movement false and no direction
std::vector<Grid_Mover> do_movement_maintain_direction(
    const Beam_Grid_Type& grid, const Grid_Mover& current_location);

// return collection of next location as a result of moving onto the current
// location of type '-'.
// if the move ran off the grid, return the same location
// with movement false and no direction
std::vector<Grid_Mover> do_movement_horizontal_splitter(
    const Beam_Grid_Type& grid, const Grid_Mover& current_location);

// return collection of next location as a result of moving onto the current
// location of type '|'.
// if the move ran off the grid, return the same location
// with movement false and no direction
std::vector<Grid_Mover> do_movement_vertical_splitter(
    const Beam_Grid_Type& grid, const Grid_Mover& current_location);

// '\'
std::vector<Grid_Mover> do_movement_fwd_slash_angle(
    const Beam_Grid_Type& grid, const Grid_Mover& current_location);

// '/'
std::vector<Grid_Mover> do_movement_bck_slash_angle(
    const Beam_Grid_Type& grid, const Grid_Mover& current_location);

// Return the number of energized sections  firing the beam. Each cell contains
// the direction of movement of the beam on the way out of the cell
int do_beam_walking(const Beam_Grid_Type& grid,
                    const Grid_Mover& starting_point,
                    Movement_Grid_Type& movement_grid);

}  // namespace Day16_Utils

#endif /* __DAY16_UTILS_H__ */