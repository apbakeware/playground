#ifndef __DAY11_UTILS_H__
#define __DAY11_UTILS_H__

#include <utility>
#include <vector>

#include "../utils/grid/grid.h"

namespace Day11_Utils {

using Galaxy_Grid = Fixed_Sized_Grid<char>;
using Galaxy_Value_Type = unsigned long long int;

static const char GALAXY = '#';

static auto has_galaxy_pred = [](const char value) { return value == '#'; };

struct Galaxy_Location_Type {
  Galaxy_Value_Type row;
  Galaxy_Value_Type col;

  Galaxy_Location_Type();
  Galaxy_Location_Type(const Grid_Location& loc);
  Galaxy_Location_Type(Galaxy_Value_Type in_row, Galaxy_Value_Type in_col);
};

bool operator<(const Galaxy_Location_Type& lhs,
               const Galaxy_Location_Type& rhs);
bool operator==(const Galaxy_Location_Type& lhs,
                const Galaxy_Location_Type& rhs);
bool operator!=(const Galaxy_Location_Type& lhs,
                const Galaxy_Location_Type& rhs);

std::istream& operator>>(std::istream& istr, Galaxy_Location_Type record);

Galaxy_Value_Type galaxy_distance(const Galaxy_Location_Type& orig,
                                  const Galaxy_Location_Type& dest);

Galaxy_Grid create_galaxy_grid(std::vector<std::string>::const_iterator start,
                               std::vector<std::string>::const_iterator end);

std::vector<Galaxy_Value_Type> get_rows_without_galaxies(
    const Fixed_Sized_Grid<char>& grid);

std::vector<Galaxy_Value_Type> get_cols_without_galaxies(
    const Fixed_Sized_Grid<char>& grid);

std::vector<Galaxy_Location_Type> get_galaxy_locations(
    const Fixed_Sized_Grid<char>& grid);

// Working from the back of each list, get the
// set of galaxy rows greater than the current
// expansion point and increment them.
//
// I think we can move directly as we're moving
// from the back...otherwise we'd have to worry bumping
// past expansion numbers after the increment

void do_galaxy_expansion(std::vector<Galaxy_Location_Type>& galaxy_locations,
                         const std::vector<Galaxy_Value_Type>& rows_to_expand,
                         const std::vector<Galaxy_Value_Type>& cols_to_expand,
                         Galaxy_Value_Type expansion = 1);

/**
 * Create a collection of pair combinations from a collection
 * of objects.
 *
 * @param values Collection (ex vector) of data type T.
 * @return Vector of T pairs.
 */
template <typename T, template <typename, typename...> class V,
          typename... Args>
std::vector<std::pair<T, T>> create_pair_combinations(
    const V<T, Args...>& values) {
  std::vector<std::pair<T, T>> result;

  auto iter = values.begin();
  while (iter != values.end()) {
    auto next_iter = std::next(iter);
    while (next_iter != values.end()) {
      result.push_back({*iter, *next_iter});
      ++next_iter;
    }
    ++iter;
  }

  return result;
}

}  // namespace Day11_Utils

#endif /* __DAY11_UTILS_H__ */