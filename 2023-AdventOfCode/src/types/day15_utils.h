#ifndef __DAY15_UTILS_H__
#define __DAY15_UTILS_H__

#include <list>
#include <string>
#include <utility>

namespace Day15_Utils {

int hash(const std::string& value);

enum class Operation { UPDATE, REMOVE };

struct Lens_Record {
  std::string label;
  int focal_length;
};

bool operator==(const Lens_Record& first, const Lens_Record& second);

std::pair<Lens_Record, Operation> parse_instruction(const std::string& field);

class Lens_Box {
 public:
  using Storage_Type = std::list<Lens_Record>;
  using iterator = Storage_Type::iterator;
  using const_iterator = Storage_Type::const_iterator;

  void update(const Lens_Record& record);

  // Only the label of the lens record is used
  void remove(const Lens_Record& record);

  int focal_power() const;

  ssize_t size() const { return lenses_.size(); }

  iterator begin() { return lenses_.begin(); }
  const_iterator begin() const { return lenses_.begin(); }

  iterator end() { return lenses_.end(); }
  const_iterator end() const { return lenses_.end(); }

 private:
  std::list<Lens_Record> lenses_;
};

}  // namespace Day15_Utils

#endif /* __DAY15_UTILS_H__ */