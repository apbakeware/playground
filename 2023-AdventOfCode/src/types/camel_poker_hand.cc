#include "camel_poker_hand.h"

#include <algorithm>
#include <iostream>
#include <limits>
#include <vector>

#include "spdlog/spdlog.h"

std::istream& operator>>(std::istream& instr, Camel_Poker_Hand& obj) {
  instr >> obj.cards >> obj.bid;
  return instr;
}

// Count the number of each card type
Camel_Poker_Hand_Rank Day07_Part_1_Rank_Policy::rank_hand(
    const std::string& cards) {
  static std::vector<int> card_count(std::numeric_limits<char>::max());

  Camel_Poker_Hand_Rank hand_rank = Camel_Poker_Hand_Rank::UNKNOWN;

  std::fill(card_count.begin(), card_count.end(), 0);
  for (char card : cards) {
    card_count[card]++;
  }

  // sort in ascending order
  std::sort(card_count.begin(), card_count.end());
  auto iter = card_count.rbegin();
  hand_rank = Camel_Poker_Hand_Rank::HIGH_CARD;
  if (*iter == 5) {
    hand_rank = Camel_Poker_Hand_Rank::FIVE_OF_A_KIND;
  } else if (*iter == 4) {
    hand_rank = Camel_Poker_Hand_Rank::FOUR_OF_A_KIND;
  } else if (*iter == 3) {
    ++iter;
    if (*iter == 2) {
      hand_rank = Camel_Poker_Hand_Rank::FULL_HOUSE;
    } else {
      hand_rank = Camel_Poker_Hand_Rank::THREE_OF_A_KIND;
    }
  } else if (*iter == 2) {
    ++iter;
    if (*iter == 2) {
      hand_rank = Camel_Poker_Hand_Rank::TWO_PAIR;
    } else {
      hand_rank = Camel_Poker_Hand_Rank::ONE_PAIR;
    }
  }

  return hand_rank;
}

Camel_Poker_Hand_Rank Day07_Part_2_Rank_Policy::rank_hand(
    const std::string& cards) {
  const char wild = 'J';
  static std::vector<int> card_count(std::numeric_limits<char>::max());

  Camel_Poker_Hand_Rank hand_rank = Camel_Poker_Hand_Rank::UNKNOWN;
  int num_wilds = 0;

  SPDLOG_DEBUG("Cards: {}  Wild: '{}", cards, wild);
  std::fill(card_count.begin(), card_count.end(), 0);

  // Dont insert the jack, keep it separate
  for (char card : cards) {
    if (card == wild) {
      SPDLOG_DEBUG(" * found wild");
      ++num_wilds;
    } else {
      SPDLOG_DEBUG(" incrementing {}", card);
      card_count[card]++;
    }
  }

  std::sort(card_count.begin(), card_count.end());
  SPDLOG_DEBUG("Number of wilds: {}", num_wilds);
  SPDLOG_DEBUG("Count array: {}", fmt::join(card_count, ", "));
  //  TODO: edge case where 4 wilds ??

  // sort in ascending order
  // Only need to sort through the lowest and highest in the range we can
  // expect

  auto iter = card_count.rbegin();
  auto card_count_value = *iter + num_wilds;  // - card_count['J'];
  SPDLOG_DEBUG("  *iter: {}  card_count_value: {}", *iter, card_count_value);
  hand_rank = Camel_Poker_Hand_Rank::HIGH_CARD;
  if (card_count_value == 5) {
    SPDLOG_DEBUG("   - Found FIVE_OF_A_KIND");
    hand_rank = Camel_Poker_Hand_Rank::FIVE_OF_A_KIND;
  } else if (card_count_value == 4) {
    SPDLOG_DEBUG("   - Found FOUR_OF_A_KIND");
    hand_rank = Camel_Poker_Hand_Rank::FOUR_OF_A_KIND;
  } else if (card_count_value == 3) {
    SPDLOG_DEBUG("   - Found count 3");
    ++iter;
    if (*iter == 2) {
      SPDLOG_DEBUG("   - Found FULL_HOUSE");
      hand_rank = Camel_Poker_Hand_Rank::FULL_HOUSE;
    } else {
      SPDLOG_DEBUG("   - Found THREE_OF_A_KIND");
      hand_rank = Camel_Poker_Hand_Rank::THREE_OF_A_KIND;
    }
  } else if (card_count_value == 2) {
    ++iter;
    SPDLOG_DEBUG("   - Found count 2");
    if (*iter == 2) {
      SPDLOG_DEBUG("   - Found TWO_PAIR");
      hand_rank = Camel_Poker_Hand_Rank::TWO_PAIR;
    } else {
      SPDLOG_DEBUG("   - Found ONE_PAIR");
      hand_rank = Camel_Poker_Hand_Rank::ONE_PAIR;
    }
  }

  SPDLOG_DEBUG("hand_rank: {}", (int)hand_rank);

  return hand_rank;
}

// std::istream& operator>>(std::istream& instr, Camel_Poker_Hand& obj) {
//   instr >> obj.cards >> obj.bid;
//   return instr;
// }
