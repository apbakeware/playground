#ifndef __DELIMITED_FIELD_H__
#define __DELIMITED_FIELD_H__

#include <iostream>
#include <sstream>
#include <string>

/**
 * This stores a single field from a character delimited string.
 *
 * Extracting this structure from an input stream parses
 * the field value using getline with the delimiter.
 */
template <char Delim_T>
struct Delimited_Field {
  static constexpr char DELIMITER = Delim_T;

  std::string field;

  friend std::istream& operator>>(std::istream& str,
                                  Delimited_Field<Delim_T>& obj) {
    return getline(str, obj.field, Delim_T);
  }
};

#endif /* __DELIMITED_FIELD_H__ */