#ifndef __PARSE_EXCEPTION_H__
#define __PARSE_EXCEPTION_H__

#include <string>

class Parse_Exception : std::exception {
 public:
  Parse_Exception();
  Parse_Exception(const std::string& msg);

  const char* what() const throw();

 private:
  std::string m_msg;
};

#endif /* __PARSE_EXCEPTION_H__ */