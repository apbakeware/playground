#include "line_string.h"

#include <iostream>

/**
 * Read an entire line from the input stream using getline() and
 * set the string in the object.
 */
std::istream& operator>>(std::istream& instr, Line_String& obj) {
  std::string line;
  std::getline(instr, line);
  obj.set(std::move(line));
  return instr;
}