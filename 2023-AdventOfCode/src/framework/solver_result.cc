#include <iostream>
#include "solver_result.h"

std::ostream &operator<<(std::ostream &ostr, const Solver_Result &record) {

   ostr << record.problem_label
        << "\n+ Input data load time: " << record.data_lad_duration.count()
        << "ms"
        << "\n+ Part 1 duration: " << record.part_1_solve_duration.count()
        << "ms  Solution: " << record.part_1_solution
        << "\n+ Part 2 duration: " << record.part_2_solve_duration.count()
        << "ms  Solution: " << record.part_2_solution;

   return ostr;
}

std::ostream &operator<<(std::ostream &ostr, const Expected_Solver_Result &record) {
   ostr << record.problem_label
        << "\n+ Input data load time: " << record.data_lad_duration.count()
        << "ms"
        << "\n+ Part 1 duration: " << record.part_1_solve_duration.count()
        << "ms  Expected: " << record.part_1_expected_solution
        << "  Actual: " << record.part_1_actual_solution
        << "\n+ Part 2 duration: " << record.part_2_solve_duration.count()
        << "ms  Expected: " << record.part_2_expected_solution
        << "  Actual: " << record.part_2_actual_solution;

   return ostr;
}