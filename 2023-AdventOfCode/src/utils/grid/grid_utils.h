#ifndef __GRID_UTILS_H__
#define __GRID_UTILS_H__
#include <algorithm>
#include <utility>

#include "grid.h"

template <class Grid_T>
bool is_on_grid(const Grid_Location& location, const Grid_T& grid) {
  if (location.row < 0) {
    return false;
  }
  if (location.col < 0) {
    return false;
  }
  if (location.row >= grid.number_of_rows()) {
    return false;
  }
  if (location.col >= grid.number_of_cols()) {
    return false;
  }

  return true;
}

/**
 * Return the grid location at the top of the grid in the specified column.
 *
 * @param grid Grid to get location on.
 * @param column Column number to get the top location for.
 *
 * @return A pair with the grid location in the top row of the specified
 * column on the grid. The second attribute of the inidicates if
 * the grid location is valid (ex invalid if column not on grid).
 */
template <class Grid_T>
std::pair<Grid_Location, bool> top_of_column(const Grid_T& grid,
                                             ssize_t column) {
  std::pair<Grid_Location, bool> result{{0, 0}, false};

  if (column < grid.number_of_cols()) {
    result.first.row = grid.number_of_rows() - 1;
    result.first.col = column;
    result.second = true;
  }

  return result;
}

template <class Grid_T>
std::pair<Grid_Location, bool> find_value(const Grid_T& grid,
                                          typename Grid_T::value_type value) {
  for (ssize_t row = 0; row < grid.number_of_rows(); ++row) {
    for (ssize_t col = 0; col < grid.number_of_cols(); ++col) {
      const Grid_Location loc{row, col};
      if (grid.at(loc) == value) {
        return {loc, true};
      }
    }
  }

  return {{0, 0}, false};
}

template <class Operation_T, class Grid_T, class Location_Iter_T>
void for_each_cell_location(Grid_T& grid, Location_Iter_T begin,
                            Location_Iter_T end, Operation_T op) {
  std::for_each(begin, end, [&](const auto& location) {
    if (is_on_grid(location, grid)) {
      op(location, grid.at(location));
    }
  });
}

template <class String_Row_Iter_T>
Fixed_Sized_Grid<int> create_digit_grid_from_string_rows(
    String_Row_Iter_T begin, String_Row_Iter_T end) {
  const auto iter_dist = std::distance(begin, end);
  const int num_rows = static_cast<int>(iter_dist);
  const int num_cols = static_cast<int>(begin->size());
  Fixed_Sized_Grid<int> grid(num_rows, num_cols);

  int row_num = grid.number_of_rows() - 1;
  std::for_each(begin, end, [&](const auto& row) {
    for (int col = 0; col < num_cols; ++col) {
      const int value =
          row[col] - '0';  // Single char use ASCII..otherwise stoi
      const Grid_Location location = {static_cast<unsigned int>(row_num),
                                      static_cast<unsigned int>(col)};
      grid.set(location, value);
    }
    --row_num;
  });

  return grid;
}

/**
 * Build a grid of 'char' values from a collection of strings.
 * Characters from the strings are placed into the character grid.
 *
 * @tparam String_Row_Iter_T Iterator to a collection of string rows.
 */
template <class String_Row_Iter_T>
Fixed_Sized_Grid<char> create_char_grid_from_string_rows(
    String_Row_Iter_T begin, String_Row_Iter_T end) {
  const auto iter_dist = std::distance(begin, end);
  const int num_rows = static_cast<int>(iter_dist);
  const int num_cols = static_cast<int>(begin->size());
  Fixed_Sized_Grid<char> grid(num_rows, num_cols);

  int row_num = grid.number_of_rows() - 1;
  std::for_each(begin, end, [&](const auto& row) {
    for (int col = 0; col < num_cols; ++col) {
      const Grid_Location location = {static_cast<unsigned int>(row_num),
                                      static_cast<unsigned int>(col)};
      grid.set(location, row[col]);
    }
    --row_num;
  });

  return grid;
}

// Cell type must be constructable from an integer digit
template <class Grid_Cell_T, class String_Row_Iter_T>
Fixed_Sized_Grid<Grid_Cell_T> create_digit_grid_from_string_rows(
    String_Row_Iter_T begin, String_Row_Iter_T end) {
  const auto iter_dist = std::distance(begin, end);
  const int num_rows = static_cast<int>(iter_dist);
  const int num_cols = static_cast<int>(begin->size());
  Fixed_Sized_Grid<Grid_Cell_T> grid(num_rows, num_cols);

  int row_num = grid.number_of_rows() - 1;
  std::for_each(begin, end, [&](const auto& row) {
    for (int col = 0; col < num_cols; ++col) {
      const int digit =
          row[col] - '0';  // Single char use ASCII..otherwise stoi
      Grid_Cell_T cell(digit);
      const Grid_Location location = {static_cast<unsigned int>(row_num),
                                      static_cast<unsigned int>(col)};
      grid.set(location, cell);
    }
    --row_num;
  });

  return grid;
}

template <class Grid_Cell_T>
Fixed_Sized_Grid<Grid_Cell_T> flip_horizontal(
    const Fixed_Sized_Grid<Grid_Cell_T>& orig) {
  const auto num_rows = orig.number_of_rows();
  const auto num_cols = orig.number_of_cols();
  Fixed_Sized_Grid<Grid_Cell_T> flipped(num_rows, num_cols);

  orig.for_each_cell([&](const auto& loc, auto value) {
    Grid_Location flip_loc = {num_rows - loc.row - 1, loc.col};
    flipped.set(flip_loc, value);
  });

  return flipped;
}

template <class Grid_Cell_T>
Fixed_Sized_Grid<Grid_Cell_T> flip_vertical(
    const Fixed_Sized_Grid<Grid_Cell_T>& orig) {
  const auto num_rows = orig.number_of_rows();
  const auto num_cols = orig.number_of_cols();
  Fixed_Sized_Grid<Grid_Cell_T> flipped(num_rows, num_cols);

  orig.for_each_cell([&](const auto& loc, auto value) {
    Grid_Location flip_loc = {loc.row, num_cols - loc.col - 1};
    flipped.set(flip_loc, value);
  });

  return flipped;
}

#endif /* __GRID_UTILS_H__ */