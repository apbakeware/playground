#ifndef __GRID_LOCATION_H__
#define __GRID_LOCATION_H__

#include <unistd.h>

#include <iosfwd>

#include "spdlog/fmt/fmt.h"

/**
 * Stores a Row, Col value for accessing a Grid. Provides value
 * semantic routines to move the location in four ordinal directions
 * on the grid.
 *
 * For Grid movement the underlying grid coordinate scheme uses
 * (0,0) in the lower left as illustrated below. The grid
 * contents should be created accordingly.
 *
 *       10
 *        -            ^
 *        -            |
 *     R  -            u
 *     o  -            p
 *     w  -            |
 *        -
 *        -            +  --right ->
 *        -
 *        -
 *        0
 *         0     |    10     |     20
 *              Col
 */
struct Grid_Location {
  ssize_t row;
  ssize_t col;

  static Grid_Location up(const Grid_Location &loc, ssize_t dist = 1);

  static Grid_Location right(const Grid_Location &loc, ssize_t dist = 1);

  static Grid_Location down(const Grid_Location &loc, ssize_t dist = 1);

  static Grid_Location left(const Grid_Location &loc, ssize_t dist = 1);

  template <typename OStream>
  friend OStream &operator<<(OStream &os, const Grid_Location &rec) {
    return os << "[row: " << rec.row << ", col: " << rec.col << "]";
  }
};

bool operator<(const Grid_Location &lhs, const Grid_Location &rhs);
bool operator==(const Grid_Location &lhs, const Grid_Location &rhs);
bool operator!=(const Grid_Location &lhs, const Grid_Location &rhs);

std::istream &operator>>(std::istream &istr, Grid_Location record);

template <class Value_T>
struct Location_Value {
  using Value_Type = Value_T;
  Grid_Location location;
  Value_T value;

  template <typename OStream>
  friend OStream &operator<<(OStream &os, const Location_Value &rec) {
    return os << "[" << rec.value << " @ " << rec.location << "]";
  }

  friend bool operator==(const Location_Value<Value_T> &lhs,
                         const Location_Value<Value_T> &rhs) {
    return lhs.location == rhs.location && lhs.value == rhs.value;
  }
};

template <>
struct fmt::formatter<Grid_Location> {
  // Parses format specifications of the form ['f' | 'e'].
  constexpr auto parse(format_parse_context &ctx)
      -> format_parse_context::iterator {
    return ctx.begin();
  }

  // Formats the point p using the parsed format specification (presentation)
  // stored in this formatter.
  auto format(const Grid_Location &loc, format_context &ctx) const
      -> format_context::iterator {
    // ctx.out() is an output iterator to write to.
    return fmt::format_to(ctx.out(), "[row: {}, col: {}]", loc.row, loc.col);
  }
};

#endif /* __GRID_LOCATION_H__ */