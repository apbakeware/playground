#ifndef __GRID_DIRECTION_H__
#define __GRID_DIRECTION_H__

#include "grid_location.h"
#include "spdlog/fmt/fmt.h"

namespace Orthogonal_Direction {

/**
 * Values representing 4 cardinal movement directions.
 * Valued to be useful as bitmask if needed.
 */
enum Direction { NONE = 0x0, UP = 0x1, RIGHT = 0x2, DOWN = 0x4, LEFT = 0x8 };

Direction opposite_direction(Direction direction);

template <Direction Direction_T>
struct Opposite_Direction {
  constexpr static Direction direction = NONE;
};

template <>
struct Opposite_Direction<UP> {
  constexpr static Direction direction = DOWN;
};

template <>
struct Opposite_Direction<RIGHT> {
  constexpr static Direction direction = LEFT;
};
template <>
struct Opposite_Direction<DOWN> {
  constexpr static Direction direction = UP;
};

template <>
struct Opposite_Direction<LEFT> {
  constexpr static Direction direction = RIGHT;
};

Grid_Location next_location(const Grid_Location &loc, Direction direction);

}  // namespace Orthogonal_Direction

template <>
struct fmt::formatter<Orthogonal_Direction::Direction> {
  // Parses format specifications of the form ['f' | 'e'].
  constexpr auto parse(format_parse_context &ctx)
      -> format_parse_context::iterator {
    return ctx.begin();
  }

  // Formats the point p using the parsed format specification (presentation)
  // stored in this formatter.
  auto format(Orthogonal_Direction::Direction dir, format_context &ctx) const
      -> format_context::iterator {
    // ctx.out() is an output iterator to write to.
    std::string direction_str = "unknown";
    switch (dir) {
      case Orthogonal_Direction::NONE:
        direction_str = "NONE";
        break;
      case Orthogonal_Direction::UP:
        direction_str = "UP";
        break;
      case Orthogonal_Direction::RIGHT:
        direction_str = "RIGHT";
        break;
      case Orthogonal_Direction::DOWN:
        direction_str = "DOWN";
        break;
      case Orthogonal_Direction::LEFT:
        direction_str = "LEFT";
        break;
    }
    return fmt::format_to(ctx.out(), "{}", direction_str);
  }
};

#endif /* __GRID_DIRECTION_H__ */