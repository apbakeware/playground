#ifndef __INT_BUILDER_H__
#define __INT_BUILDER_H__

/**
 * This class builds an integer by successivly
 * adding digit characters. The first charater
 * added in largest place value.
 *
 * The has_value() method is used to determine if an
 * integer value has been constructed.
 *
 * Example useage below
 * @code
 * Int_Builder ib;
 * ib.has_value(); // false
 * ib.append('1');
 * ib.append('2');
 * ib.has_value(); // true;
 * ib.value(); // 12
 * @endcode
 */
class Int_Builder {
 public:
  /**
   * Standard constructor. No valid integer value is
   * stored.
   */
  Int_Builder();

  /**
   * Append the character to the integer which is being build.
   * If the character is not a [0-9] digit, nothing is done. A
   * valid digit is added to the 'ones' place and the existing
   * digits are shifted left on place.
   *
   * @param digit Character from '0' to '9' to append to the
   * integer.
   *
   * @return True if the character was added (was a digit),
   * otherwise false.
   */
  bool append(char digit);

  /**
   * Return the stored integer value. To check if the valid
   * is valid, use has_value().
   */
  int value() const { return m_val; }

  /**
   * Return the number of digits in the integeer.
   */
  int num_digits() const { return m_num_digits; }

  /**
   * Check if a valid integer value has been built.
   *
   * @return True if characters have been added building
   * an integer, otherwise false.
   */
  bool has_value() const { return m_has_value; }

 private:
  int m_val;
  int m_num_digits;
  bool m_has_value;
};

#endif /* __INT_BUILDER_H__ */