#ifndef __DAY05_H__
#define __DAY05_H__

#include <string>
#include <vector>

#include "../framework/line_string.h"

struct Day05Data {
  typedef Line_String Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day05Part1 : public Day05Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day05Part2 : public Day05Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day05Spec {
  using Data_Spec = Day05Data;
  using Part_1 = Day05Part1;
  using Part_2 = Day05Part2;
  static constexpr const char* day() { return "Day05"; }
  static constexpr const char* data_file_name() { return "data/day05.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day05.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() { return "35"; }
  static constexpr const char* expected_sample_part_2_result() { return "46"; }
  static constexpr const char* solved_part_1_result() { return "825516882"; }
  static constexpr const char* solved_part_2_result() { return "136096660"; }
};

#endif  // __DAY05_H__
