#include "day04.h"

#include <algorithm>
#include <sstream>
#include <valarray>

#include "../framework/solver_registry.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day04Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

// Again, would be better to "getline"
std::istream& operator>>(std::istream& instr, Day04ScratchOff& obj) {
  std::string line;
  getline(instr, line);

  std::string tmp;
  int value;
  obj.game_number = 0;

  // Not sure why these have to be cleared. But it was building off
  // previous set of numbers....
  obj.winning_numbers.clear();
  obj.my_numbers.clear();

  std::istringstream line_str(line);

  line_str >> tmp >> obj.game_number >> tmp;
  while (true) {
    line_str >> value;
    if (line_str.fail()) {
      line_str.clear();
      line_str >> tmp;
      break;
    }
    obj.winning_numbers.push_back(value);
  }

  while (true) {
    line_str >> value;
    if (line_str.fail()) {
      break;
    }
    obj.my_numbers.push_back(value);
  }

  std::sort(obj.winning_numbers.begin(), obj.winning_numbers.end());
  std::sort(obj.my_numbers.begin(), obj.my_numbers.end());

  return instr;
}

std::string Day04Part1::name() const { return "Day04-Part1"; }

std::string Day04Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  int total_worth = 0;
  std::vector<int> common_numbers;
  auto inersection_iter = common_numbers.begin();

  for (const auto& rec : data) {
    int card_value = 0;
    size_t vec_size =
        std::max(rec.winning_numbers.size(), rec.my_numbers.size());

    common_numbers.clear();
    common_numbers.resize(vec_size);
    inersection_iter = common_numbers.begin();

    auto end_of_intersection = std::set_intersection(
        rec.winning_numbers.begin(), rec.winning_numbers.end(),
        rec.my_numbers.begin(), rec.my_numbers.end(), inersection_iter);

    auto common_count =
        std::distance(common_numbers.begin(), end_of_intersection);

    // use binary position for doubling
    card_value = (common_count > 0) ? 0x1 << (common_count - 1) : 0;
    total_worth += card_value;
  }

  return std::to_string(total_worth);
}

std::string Day04Part2::name() const { return "Day04-Part2"; }

std::string Day04Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());
  std::vector<int> common_numbers;
  auto inersection_iter = common_numbers.begin();

  std::valarray<int> card_copies(1, data.size());
  std::valarray<int> to_add(0, data.size());

  for (const auto& rec : data) {
    size_t vec_size =
        std::max(rec.winning_numbers.size(), rec.my_numbers.size());

    common_numbers.clear();
    common_numbers.resize(vec_size);
    inersection_iter = common_numbers.begin();

    auto end_of_intersection = std::set_intersection(
        rec.winning_numbers.begin(), rec.winning_numbers.end(),
        rec.my_numbers.begin(), rec.my_numbers.end(), inersection_iter);

    auto common_count =
        std::distance(common_numbers.begin(), end_of_intersection);

    auto copies_of_this_card = card_copies[rec.game_number - 1];

    if (common_count > 0) {
      // as the game cards are 1 base d and the valarray is 0 based indexing
      // the current game card starts at the "next" value which is what we need
      // increment number of copies.
      auto valarry_to_increment =
          card_copies[std::slice(rec.game_number, common_count, 1)];

      // TODO: need to add 1 to each of the next few slots
      to_add.resize(common_count, copies_of_this_card);
      valarry_to_increment += to_add;
    }
  }

  return std::to_string(card_copies.sum());
}
