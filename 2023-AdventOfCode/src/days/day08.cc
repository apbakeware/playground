#include "day08.h"

#include <regex>

#include "../framework/solver_registry.h"
#include "../types/lr_named_graph_path_finder.h"
#include "../utils/graph/named_route_graph.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day08Spec>();
}

const bool _was_registered = _do_registration();

static const std::regex MAP_LINE_REGEX{
    "^([A-Z]{3}) = .([A-Z]{3}), ([A-Z]{3}).*$"};

}  // namespace

std::string Day08Part1::name() const { return "Day08-Part1"; }

std::string Day08Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  Input_Data_Collection::const_iterator data_citer = data.begin();
  Cyclic_Direction_List directions(data_citer->get());
  Named_Route_Graph<2> graph;
  std::smatch regex_match;

  while (data_citer != data.end()) {
    const std::string& line = data_citer->ref();
    if (std::regex_search(line, regex_match, MAP_LINE_REGEX)) {
      // SPDLOG_ERROR("FOUND REGEX MATCH");
      // SPDLOG_ERROR("  Index: 1 {}", regex_match[1].str());  // 0 is whole
      // thing SPDLOG_ERROR("  Index: 2 {}", regex_match[2].str());
      // SPDLOG_ERROR("  Index: 3 {}", regex_match[3].str());

      auto node = graph.add(regex_match[1].str());
      node->at(0) = regex_match[2].str();
      node->at(1) = regex_match[3].str();
    }

    ++data_citer;
  }

  size_t distance = Lr_Named_Graph_Path_Finder::distance_between(
      graph, "AAA", "ZZZ", directions);

  return std::to_string(distance);
}

std::string Day08Part2::name() const { return "Day08-Part2"; }

//
// Graph traversal ran overnight with 100M+ paths and no solution.
// New idea: for each starting
//   Traverse graph until a reapting substring is found?
//   Need to google repeating substrings...but what if its not
//   the whole thing repeating?
//
std::string Day08Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  return "IN PROGRESS";
  std::vector<std::string> starting_locations;
  Input_Data_Collection::const_iterator data_citer = data.begin();
  Cyclic_Direction_List directions(data_citer->get());
  Named_Route_Graph<2> graph;
  std::smatch regex_match;

  while (data_citer != data.end()) {
    const std::string& line = data_citer->ref();
    if (std::regex_search(line, regex_match, MAP_LINE_REGEX)) {
      auto node = graph.add(regex_match[1].str());
      node->at(0) = regex_match[2].str();
      node->at(1) = regex_match[3].str();

      if (regex_match[1].str().back() == 'A') {
        starting_locations.push_back(regex_match[1].str());
      }
    }

    ++data_citer;
  }

  size_t distance = Lr_Named_Graph_Concurrent_Path_Finder::distance_between(
      graph, starting_locations, directions);

  return std::to_string(0);
}
