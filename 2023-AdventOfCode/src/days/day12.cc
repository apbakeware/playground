#include "day12.h"
#include "spdlog/spdlog.h"
#include "../framework/solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day12Spec>();
}

const bool _was_registered = _do_registration();

}

std::string Day12Part1::name() const {
   return "Day12-Part1";
}

std::string Day12Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}


std::string Day12Part2::name() const {
   return "Day12-Part2";
}

std::string Day12Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}

