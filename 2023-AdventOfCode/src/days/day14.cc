#include "day14.h"

#include "../framework/solver_registry.h"
#include "../types/day14_utils.h"
#include "../utils/grid/grid.h"
#include "../utils/grid/grid_utils.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day14Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day14Part1::name() const { return "Day14-Part1"; }

std::string Day14Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  auto grid = create_char_grid_from_string_rows(data.begin(), data.end());
  int load = Day14_Utils::tilt_and_evaluate_load(grid);

  return std::to_string(load);
}

std::string Day14Part2::name() const { return "Day14-Part2"; }

std::string Day14Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  auto grid = create_char_grid_from_string_rows(data.begin(), data.end());
  int load = Day14_Utils::load_after_spin_cycles(grid, 1000000000ULL);

  return std::to_string(load);
}
