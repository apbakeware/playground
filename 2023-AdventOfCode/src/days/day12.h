#ifndef __DAY12_H__
#define __DAY12_H__

#include <string>
#include <vector>

struct Day12Data {
   // TODO: Set data type
   typedef int Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day12Part1 : public Day12Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day12Part2 : public Day12Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day12Spec {
   using Data_Spec = Day12Data;
   using Part_1 = Day12Part1;
   using Part_2 = Day12Part2;
   static constexpr const char * day() { return "Day12"; }
   static constexpr const char * data_file_name() { return "data/day12.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day12.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * expected_sample_part_2_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * solved_part_1_result() { return "undefined"; }
   static constexpr const char * solved_part_2_result() { return "undefined"; }
};

#endif // __DAY12_H__
