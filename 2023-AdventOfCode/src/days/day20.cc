#include "day20.h"

#include "../framework/solver_registry.h"
#include "../types/day20_utils.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day20Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day20Part1::name() const { return "Day20-Part1"; }

std::string Day20Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  std::vector<std::string> lines(data.size());
  std::transform(data.begin(), data.end(), lines.begin(),
                 [](const auto& val) -> std::string { return val.get(); });

  Day20_Utils::Pulse_Generator_Runner runner;
  auto generators = Day20_Utils::build_generators(lines);
  for (auto gen : generators) {
    runner.add_generator(*gen);
  }

  std::pair<unsigned int, unsigned int> totals{0, 0};
  for (int i = 0; i < 1000; ++i) {
    auto counts = runner.press_button();
    totals.first += counts.first;
    totals.second += counts.second;
  }

  return std::to_string(totals.first * totals.second);
}

std::string Day20Part2::name() const { return "Day20-Part2"; }

std::string Day20Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  return "unimplemented";

  std::vector<std::string> lines(data.size());
  std::transform(data.begin(), data.end(), lines.begin(),
                 [](const auto& val) -> std::string { return val.get(); });

  Day20_Utils::Pulse_Generator_Runner runner;
  auto generators = Day20_Utils::build_generators(lines);
  for (auto gen : generators) {
    runner.add_generator(*gen);
  }

  unsigned int count = 1;
  while (!runner.press_button_looking_for_rx_low_pulse()) {
    if (count % 5000 == 0) {
      std::cout << "Count: " << count << "\n";
    }
    ++count;
  }

  return std::to_string(count);
}
