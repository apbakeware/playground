#include "day22.h"
#include "spdlog/spdlog.h"
#include "../framework/solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day22Spec>();
}

const bool _was_registered = _do_registration();

}

std::string Day22Part1::name() const {
   return "Day22-Part1";
}

std::string Day22Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}


std::string Day22Part2::name() const {
   return "Day22-Part2";
}

std::string Day22Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}

