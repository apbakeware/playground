#include "day10.h"

#include "../framework/solver_registry.h"
#include "../types/day10_traversal_utils.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day10Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day10Part1::name() const { return "Day10-Part1"; }

std::string Day10Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  auto grid = create_char_grid_from_string_rows(data.begin(), data.end());
  auto start_loc = find_value(grid, 'S');

  // Day10_Traversal_Utils::render_unicode_grid(grid);

  int steps =
      Day10_Traversal_Utils::determine_pipe_length(grid, start_loc.first);

  // Divide by 2
  return std::to_string(steps >> 1);
}

std::string Day10Part2::name() const { return "Day10-Part2"; }

std::string Day10Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  return "incorrect";
}
