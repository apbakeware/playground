#ifndef __DAY03_H__
#define __DAY03_H__

#include <string>
#include <vector>

struct Day03Data {
  typedef std::string Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day03Part1 : public Day03Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day03Part2 : public Day03Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day03Spec {
  using Data_Spec = Day03Data;
  using Part_1 = Day03Part1;
  using Part_2 = Day03Part2;
  static constexpr const char* day() { return "Day03"; }
  static constexpr const char* data_file_name() { return "data/day03.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day03.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() {
    return "4361";
  }
  static constexpr const char* expected_sample_part_2_result() {
    return "467835";
  }
  static constexpr const char* solved_part_1_result() { return "527369"; }
  static constexpr const char* solved_part_2_result() { return "73074886"; }
};

#endif  // __DAY03_H__
