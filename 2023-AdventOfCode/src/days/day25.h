#ifndef __DAY25_H__
#define __DAY25_H__

#include <string>
#include <vector>

struct Day25Data {
   // TODO: Set data type
   typedef int Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day25Part1 : public Day25Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day25Part2 : public Day25Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day25Spec {
   using Data_Spec = Day25Data;
   using Part_1 = Day25Part1;
   using Part_2 = Day25Part2;
   static constexpr const char * day() { return "Day25"; }
   static constexpr const char * data_file_name() { return "data/day25.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day25.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * expected_sample_part_2_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * solved_part_1_result() { return "undefined"; }
   static constexpr const char * solved_part_2_result() { return "undefined"; }
};

#endif // __DAY25_H__
