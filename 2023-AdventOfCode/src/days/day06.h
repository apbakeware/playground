#ifndef __DAY06_H__
#define __DAY06_H__

#include <iosfwd>
#include <string>
#include <vector>

#include "../framework/line_string.h"

struct Labeled_Vector {
  std::string label;
  std::vector<unsigned long int> values;
};

std::istream& operator>>(std::istream& instr, Labeled_Vector& obj);

struct Day06Data {
  typedef Line_String Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day06Part1 : public Day06Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day06Part2 : public Day06Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day06Spec {
  using Data_Spec = Day06Data;
  using Part_1 = Day06Part1;
  using Part_2 = Day06Part2;
  static constexpr const char* day() { return "Day06"; }
  static constexpr const char* data_file_name() { return "data/day06.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day06.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() { return "288"; }
  static constexpr const char* expected_sample_part_2_result() {
    return "71503";
  }
  static constexpr const char* solved_part_1_result() { return "293046"; }
  static constexpr const char* solved_part_2_result() { return "35150181"; }
};

#endif  // __DAY06_H__
