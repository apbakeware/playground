#ifndef __DAY19_H__
#define __DAY19_H__

#include <string>
#include <vector>

#include "../framework/line_string.h"
#include "../types/day19_utils.h"

struct Day19Data {
  typedef Line_String Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day19Part1 : public Day19Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day19Part2 : public Day19Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day19Spec {
  using Data_Spec = Day19Data;
  using Part_1 = Day19Part1;
  using Part_2 = Day19Part2;
  static constexpr const char* day() { return "Day19"; }
  static constexpr const char* data_file_name() { return "data/day19.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day19.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() {
    return "19114";
  }
  static constexpr const char* expected_sample_part_2_result() {
    return "167409079868000";
  }
  static constexpr const char* solved_part_1_result() { return "399284"; }
  static constexpr const char* solved_part_2_result() { return "undefined"; }
};

#endif  // __DAY19_H__
