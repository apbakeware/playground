#ifndef __DAY04_H__
#define __DAY04_H__

#include <string>
#include <vector>

struct Day04ScratchOff {
  int game_number;
  std::vector<int> winning_numbers;
  std::vector<int> my_numbers;
};

// Also sorts the numbers
std::istream& operator>>(std::istream& instr, Day04ScratchOff& obj);

struct Day04Data {
  typedef Day04ScratchOff Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day04Part1 : public Day04Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day04Part2 : public Day04Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day04Spec {
  using Data_Spec = Day04Data;
  using Part_1 = Day04Part1;
  using Part_2 = Day04Part2;
  static constexpr const char* day() { return "Day04"; }
  static constexpr const char* data_file_name() { return "data/day04.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day04.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() { return "13"; }
  static constexpr const char* expected_sample_part_2_result() { return "30"; }
  static constexpr const char* solved_part_1_result() { return "27059"; }
  static constexpr const char* solved_part_2_result() { return "5744979"; }
};

#endif  // __DAY04_H__
