#ifndef __GRAPH_UTILS_H__
#define __GRAPH_UTILS_H__

#include <iterator>
#include <string>
#include <unordered_map>
#include <vector>

#include "aoc/graph/graph.h"

namespace aoc::graph {

using Graph_Path = std::vector<std::string>;
using Graph_Paths = std::vector<Graph_Path>;
using Visitation_List = std::unordered_map<std::string, bool>;

template <class Iter_T>
Graph create_graph_from_string_collection(Iter_T begin, Iter_T end) {

  Graph graph;

  while (begin != end) {
    graph.add_connection(begin->origin, begin->destination);
    ++begin;
  }

  return graph;
}

// TODO: visibititation rules
// Inserter for the completed paths

template <class Node_Visitiation_Policy_T>
void _all_paths_dfs(const Graph &graph, const std::string &node,
                    const std::string &destination,
                    Graph_Path &curr_path, // Consider deque later
                    Graph_Paths &complete_paths,
                    Node_Visitiation_Policy_T &visitation_policy) {

  SPDLOG_DEBUG("Traversal -- node '{}'", node);

  if (node == destination) {
    SPDLOG_DEBUG("Found destination node '{}', ending traversal", destination);
    auto complete_path = curr_path;
    complete_path.push_back(node);
    complete_paths.push_back(std::move(complete_path));
    return;
  }

  if (!graph.has_node(node)) {
    SPDLOG_DEBUG("Node '{}' not found in graph", node);
    return;
  }

  if (!visitation_policy.try_visit(node)) {
    SPDLOG_DEBUG("Node '{}' already visited", node);
    return;
  }

  curr_path.push_back(node);
  graph.for_each_connection(node, [&](const auto &connection) {
    _all_paths_dfs(graph, connection, destination, curr_path, complete_paths,
                   visitation_policy);
  });
  curr_path.pop_back();
  visitation_policy.remove_visit(node);
}

// TODO: template options
template <class Node_Visitiation_Policy_T>
Graph_Paths find_all_paths(const Graph &graph, const std::string &origin,
                           const std::string &destination,
                           Node_Visitiation_Policy_T visitation_policy) {

  SPDLOG_INFO("Searching graph for all paths from '{}' to '{}'", origin,
              destination);

  Graph_Path working_path;
  Graph_Paths all_paths;

  _all_paths_dfs(graph, origin, destination, working_path, all_paths,
                 visitation_policy);

  SPDLOG_INFO("All paths between {} and {} ({})", origin, destination,
              all_paths.size());

  std::for_each(all_paths.begin(), all_paths.end(), [](const auto &path) {
    SPDLOG_INFO("Path: {}", fmt::join(path, "-"));
  });

  return all_paths;
}

} // namespace aoc::graph

#endif // __GRAPH_UTILS_H__