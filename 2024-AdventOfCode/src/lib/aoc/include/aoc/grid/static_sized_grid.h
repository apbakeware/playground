#ifndef __STATIC_SIZED_GRID_H__
#define __STATIC_SIZED_GRID_H__

#include "aoc/grid/grid.h"

namespace aoc::grid {

template <class Data_T, size_t Num_Rows, size_t Num_Cols>
class Static_Sized_Grid {
public:
  typedef Data_T value_type;

  constexpr ssize_t number_of_cols() const {
    return static_cast<ssize_t>(Num_Cols);
  }

  constexpr ssize_t number_of_rows() const {
    return static_cast<ssize_t>(Num_Rows);
  }

  Data_T at(const Grid_Location &location) const {
    return m_grid.at(location.row).at(location.col);
  }

  void set(const Grid_Location &location, const Data_T &data) {
    m_grid.at(location.row).at(location.col) = data;
  }

  template <class Op> void for_each_cell(Op op) {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location));
      }
    }
  }

  template <class Op> void for_each_cell(Op op) const {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location));
      }
    }
  }

  template <typename OStream>
  friend OStream &
  operator<<(OStream &os,
             const Static_Sized_Grid<Data_T, Num_Rows, Num_Cols> &grid) {
    for (auto row = grid.m_grid.rbegin(); row != grid.m_grid.rend(); ++row) {
      std::copy(row->begin(), row->end(),
                std::ostream_iterator<value_type>(os, "  "));
      os << "\n";
    }

    return os;
  }

private:
  using Row = std::array<Data_T, Num_Cols>;
  using Grid = std::array<Row, Num_Rows>;

  Grid m_grid;
};

} // namespace aoc::grid

#endif /* __STATIC_SIZED_GRID_H__ */