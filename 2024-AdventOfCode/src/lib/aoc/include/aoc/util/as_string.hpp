#ifndef __AS_STRING_H__
#define __AS_STRING_H__

#include <iostream>
#include <sstream>
#include <string>
#include <type_traits>

namespace aoc::util {

/**
 * std::string as_string(val);
 *
 * Return the string representation of a value or object. To return
 * the string representation a series of overloads or specializations
 * using template rules as follows:
 *
 * Try std::to_string(): Use this if it works for the given type.
 * Try obj.str(): Use this if the type has a .str() member function.
 * Try operator<<: Use this if the type supports streaming with std::ostream.
 * Default Case: Fail with a compile-time error with message
 */

//////////////////////////////////////////////////
// Check if std::to_string is available for a type
template <typename T, typename = void>
struct has_to_string : std::false_type {};

template <typename T>
struct has_to_string<T,
                     std::void_t<decltype(std::to_string(std::declval<T>()))>>
    : std::true_type {};

//////////////////////////////////////////////////
// Check if a type has a .str() method
template <typename T, typename = void>
struct has_str_method : std::false_type {};

template <typename T>
struct has_str_method<T, std::void_t<decltype(std::declval<T>().str())>>
    : std::true_type {};

//////////////////////////////////////////////////
// Check if operator<< is available for a type
template <typename T, typename = void>
struct has_ostream_operator : std::false_type {};

template <typename T>
struct has_ostream_operator<
    T,
    std::void_t<decltype(std::declval<std::ostream &>() << std::declval<T>())>>
    : std::true_type {};

//////////////////////////////////////////////////
// Try std::to_string if available
template <typename T>
std::enable_if_t<has_to_string<T>::value, std::string>
as_string(const T &value) {
  return std::to_string(value);
}

//////////////////////////////////////////////////
// Try obj.str() if available
template <typename T>
std::enable_if_t<!has_to_string<T>::value && has_str_method<T>::value,
                 std::string>
as_string(const T &value) {
  return value.str();
}

//////////////////////////////////////////////////
// Try operator<< if available
template <typename T>
std::enable_if_t<!has_to_string<T>::value && !has_str_method<T>::value &&
                     has_ostream_operator<T>::value,
                 std::string>
as_string(const T &value) {
  std::ostringstream oss;
  oss << value;
  return oss.str();
}

//////////////////////////////////////////////////
// Default case: Compile-time error
template <typename T>
std::enable_if_t<!has_to_string<T>::value && !has_str_method<T>::value &&
                     !has_ostream_operator<T>::value,
                 std::string>
as_string(const T &) {
  static_assert(sizeof(T) == 0, "ERROR: no conversion to string provided");
  return {}; // This line will never be reached
}

} // namespace aoc::util

#endif /* __AS_STRING_H__ */