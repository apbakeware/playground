#ifndef __CONSTRUCTOR_TRAITS_H__
#define __CONSTRUCTOR_TRAITS_H__

#include <string>

namespace aoc::traits {

/**
 * Traits check if a type has a default constructor.
 *
 * use: if constexpr has_default_constructor<T>() {
 * }
 */
template <typename T> struct has_default_constructor {
  // Attempt to construct T with no arguments
  template <typename U, typename = decltype(U())>
  static std::true_type test(int);

  // Fallback if default construction is not valid
  template <typename> static std::false_type test(...);

  // Result is true if the first overload is chosen, otherwise false
  static constexpr bool value = decltype(test<T>(0))::value;
};

template <typename T> struct has_string_constructor {
  // Check if T can be constructed with a std::string
  template <typename U, typename = decltype(U(std::declval<std::string>()))>
  static std::true_type test(int);

  // Fallback if the above is not possible
  template <typename> static std::false_type test(...);

  // Result is true if the first overload is chosen, otherwise false
  static constexpr bool value = decltype(test<T>(0))::value;
};

} // namespace aoc::traits

#endif /* __CONSTRUCTOR_TRAITS_H__ */