#include <catch2/catch_session.hpp>
#include <memory>
#include <vector>

#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

int main(int argc, char *argv[]) {
  // your setup ...

  auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
  console_sink->set_level(spdlog::level::debug);

  auto file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(
      "logs/libaoc_tests.log", true);
  file_sink->set_level(spdlog::level::trace);

  std::vector<spdlog::sink_ptr> sinks{console_sink, file_sink};

  auto logger =
      std::make_shared<spdlog::logger>("aoc_test", sinks.begin(), sinks.end());
  logger->set_level(spdlog::level::debug);
  logger->set_pattern("[%H:%M:%S.%e] [%n] [%l] %v  [%s:%#]");
  spdlog::set_default_logger(logger);

  int result = Catch::Session().run(argc, argv);

  // your clean-up...

  return result;
}