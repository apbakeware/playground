#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <functional>

#include "spdlog/spdlog.h"

#include "aoc/util/apply_adjacent_operation.h"

namespace aoc::util::test {

TEST_CASE("apply_adjacent_operation") {
  SECTION("Test subtracting values with output vector sized appropriately") {
    std::vector<int> in{5, 4, 3, 7};
    std::vector<int> out(3);
    std::vector<int> EXPECTED{-1, -1, 4};

    apply_adjacent_operation(in.begin(), in.end(), out.begin(),
                             std::minus<int>());

    SPDLOG_DEBUG("In: {}   Out: {}", fmt::join(in, ","), fmt::join(out, ","));

    REQUIRE_THAT(out, Catch::Matchers::Equals(EXPECTED));
  }

  SECTION("Test subtracting values using std::back_inserter") {
    std::vector<int> in{5, 4, 3, 7};
    std::vector<int> out;
    std::vector<int> EXPECTED{-1, -1, 4};

    apply_adjacent_operation(in.begin(), in.end(), std::back_inserter(out),
                             std::minus<int>());

    SPDLOG_DEBUG("In: {}   Out: {}", fmt::join(in, ","), fmt::join(out, ","));

    REQUIRE_THAT(out, Catch::Matchers::Equals(EXPECTED));
  }
}

} // namespace aoc::util::test