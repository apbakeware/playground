#include <iostream>

#include "aoc/grid/grid_location.h"
#include "spdlog/spdlog.h"
#include <catch2/catch_test_macros.hpp>

#include "aoc/grid/grid_navigation.h"

namespace aoc::grid::test {

TEST_CASE("Test Grid Orientation", "[grid]") {

  SECTION("rotate_cw") {
    CHECK(rotate_cw(Orientation4::UP) == Orientation4::RIGHT);
    CHECK(rotate_cw(Orientation4::RIGHT) == Orientation4::DOWN);
    CHECK(rotate_cw(Orientation4::DOWN) == Orientation4::LEFT);
    CHECK(rotate_cw(Orientation4::LEFT) == Orientation4::UP);
  }

  SECTION("rotate_ccw") {
    CHECK(rotate_ccw(Orientation4::UP) == Orientation4::LEFT);
    CHECK(rotate_ccw(Orientation4::RIGHT) == Orientation4::UP);
    CHECK(rotate_ccw(Orientation4::DOWN) == Orientation4::RIGHT);
    CHECK(rotate_ccw(Orientation4::LEFT) == Orientation4::DOWN);
  }

  SECTION("test_ahead") {
    const Grid_Location sut = {3, 3};

    CHECK(ahead(Orientation4::UP, sut) == Grid_Location{4, 3});
    CHECK(ahead(Orientation4::RIGHT, sut) == Grid_Location{3, 4});
    CHECK(ahead(Orientation4::DOWN, sut) == Grid_Location{2, 3});
    CHECK(ahead(Orientation4::LEFT, sut) == Grid_Location{3, 2});
  }

  SECTION("test_behind") {
    const Grid_Location sut = {3, 3};

    CHECK(behind(Orientation4::UP, sut) == Grid_Location{2, 3});
    CHECK(behind(Orientation4::RIGHT, sut) == Grid_Location{3, 2});
    CHECK(behind(Orientation4::DOWN, sut) == Grid_Location{4, 3});
    CHECK(behind(Orientation4::LEFT, sut) == Grid_Location{3, 4});
  }
}

} // namespace aoc::grid::test