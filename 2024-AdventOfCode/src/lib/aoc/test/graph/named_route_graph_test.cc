#include <catch2/catch_test_macros.hpp>

#include "aoc/graph/named_route_graph.h"

namespace aoc::graph::test {

TEST_CASE("Named_Route_Graph<2>") {
  using Route_Graph_Type = Named_Route_Graph<2>;

  SECTION("Find node returns null if node not found") {
    Route_Graph_Type sut;
    auto ptr = sut.find("name");
    REQUIRE(ptr == nullptr);
  }

  SECTION("Add and find node") {
    Route_Graph_Type sut;

    sut.add("NAME Label");

    REQUIRE(sut.has_node("NAME Label"));
  }

  SECTION("Add node has empty routes") {
    Route_Graph_Type sut;
    auto routes = sut.add("qwerty");

    REQUIRE(routes->at(0).empty());
    REQUIRE(routes->at(1).empty());
  }

  SECTION("Add node and routes, then find and check routes") {
    const std::string LABEL = "node";
    const std::string ROUTE_0 = "first";
    const std::string ROUTE_1 = "second";
    Route_Graph_Type sut;

    auto routes = sut.add(LABEL);
    routes->at(0) = ROUTE_0;
    routes->at(1) = ROUTE_1;

    auto node = sut.find(LABEL);
    REQUIRE(node->at(0) == ROUTE_0);
    REQUIRE(node->at(1) == ROUTE_1);
  }

  SECTION("Add multiple roads and routes") {
    Route_Graph_Type sut;

    auto routes = sut.add("1");
    routes->at(0) = "2";
    routes->at(1) = "3";

    sut.add("2")->at(0) = "Target";
    sut.add("3");

    auto found = sut.find("1");
    auto found2 = sut.find(found->at(0));

    REQUIRE(found2->at(0) == "Target");
  }

  // Test complete node span
}

} // namespace aoc::graph::test