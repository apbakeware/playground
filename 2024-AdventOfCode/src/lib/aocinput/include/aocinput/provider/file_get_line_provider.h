#ifndef __FILE_GET_LINE_PROVIDER_H__
#define __FILE_GET_LINE_PROVIDER_H__

#include <fstream>
#include <string>

#include "spdlog/spdlog.h"

namespace aoc::input::provider {

// Populated_T the input type populated by line
template <class Populated_T> class FileGetLineProvider {
public:
  using PopulatedType = Populated_T;

  FileGetLineProvider(const std::string &file_path) : m_file_path(file_path) {}

  bool populate(PopulatedType &record) {
    SPDLOG_TRACE("FileGetLineProvider::populate()");
    bool result = false;
    try {
      std::ifstream in_file_stream(m_file_path);
      if (!in_file_stream.is_open()) {
        SPDLOG_ERROR("Failed to open input file: {}", m_file_path);
        return false;
      }

      std::string line;
      while (std::getline(in_file_stream, line)) {
        SPDLOG_DEBUG("getline() input line: '{}'", line);
        record.append(line); // TODO: check this
      }

      result = true;

    } catch (...) {
      SPDLOG_ERROR("Failed to parse inputfile: {}", m_file_path);
      result = false;
    }

    return result;
  }

private:
  std::string m_file_path;
};

} // namespace aoc::input::provider

#endif /* __FILE_GET_LINE_PROVIDER_H__ */