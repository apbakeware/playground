#ifndef __NULL_PROVIDER_H__
#define __NULL_PROVIDER_H__

#include "spdlog/spdlog.h"

namespace aoc::input::provider {

/**
 * The NullProvider implements an null object pattern
 * for data population of input data. The populate() method
 * returns true to simulate a success, without affecting the
 * record.
 */
template <class Populated_T> class NullProvider {
public:
  using PopulatedType = Populated_T;

  /**
   * Return true without updating the record.
   */
  bool populate(PopulatedType &record) {
    SPDLOG_TRACE("NullProvider::populate()");
    SPDLOG_DEBUG("Using NullProvider, record content not updated");

    (void)(record);
    return true;
  }
};

template <> class NullProvider<void> {
public:
  using PopulatedType = void;

  /**
   * Return true without updating the record.
   */
  bool populate() {
    SPDLOG_TRACE("NullProvider<void>::populate()");
    SPDLOG_DEBUG("Using NullProvider<void>, record content not updated");

    return true;
  }
};

} // namespace aoc::input::provider

#endif /* __NULL_PROVIDER_H__ */