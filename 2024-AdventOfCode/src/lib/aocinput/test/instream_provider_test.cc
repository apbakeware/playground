#include <iosfwd>
#include <sstream>
#include <string>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

#include "aocinput/provider/instream_provider.h"
#include "aocinput/value_collection.h"

namespace aoc::input::test {

using aoc::input::provider::InstreamProvider;

TEST_CASE("Instream_Provider value test") {

  SECTION("Test single value") {

    std::string data = "7";
    std::stringstream str(data);
    InstreamProvider<int> sut(str);
    InstreamProvider<int>::PopulatedType value;

    CHECK(sut.populate(value));
    REQUIRE(value == 7);
  }

  SECTION("Test value collection") {

    using CollectionType = ValueCollection<int>;

    const CollectionType::ValuesType expected = {7, -19, 1264};
    std::string data = "7 -19 1264";
    std::stringstream str(data);
    InstreamProvider<CollectionType> sut(str);
    InstreamProvider<CollectionType>::PopulatedType populated;

    CHECK(sut.populate(populated));

    REQUIRE_THAT(populated.values(), Catch::Matchers::Equals(expected));
  }
}

} // namespace aoc::input::test