project(libaocinput-test)

set(TEST_SOURCE_FILES 
  instream_provider_test.cc
  file_instream_provider_test.cc
  file_get_line_provider_test.cc
)

add_executable(${PROJECT_NAME} ${TEST_SOURCE_FILES}) 
target_link_libraries(${PROJECT_NAME}
  PRIVATE 
  spdlog::spdlog
  Catch2::Catch2WithMain
  aocinput
)

add_test(NAME ${PROJECT_NAME} COMMAND ${PROJECT_NAME})
  
