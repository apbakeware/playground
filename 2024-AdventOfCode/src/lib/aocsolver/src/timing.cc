#include <iomanip>
#include <iostream>

#include "aocsolver/timing.h"

namespace aoc::solver {

Timestamp ChronoSystemClockTimestamper::getTimestampNow() {
  return std::chrono::system_clock::now();
}

std::ostream &operator<<(std::ostream &ostr,
                         const WallClockTimestampWriter &obj) {
  auto time = std::chrono::system_clock::to_time_t(obj.timestamp);
  auto ts_us = std::chrono::duration_cast<std::chrono::microseconds>(
                   obj.timestamp.time_since_epoch()) %
               1000000;

  ostr << std::put_time(std::localtime(&time), "%Y-%m-%d %H:%M:%S") << '.'
       << std::setw(6) << std::setfill('0') << ts_us.count();
  return ostr;
}

PipelineDurationRecord
computeDuration(const PipelineTimestampRecord &timestamps) {
  // TODO: need to implment this
  Duration parse_d = std::chrono::duration_cast<Duration>(
      timestamps.parse_complete - timestamps.parse_start);

  Duration solve_d = std::chrono::duration_cast<Duration>(
      timestamps.solve_complete - timestamps.solve_start);

  Duration total_d = std::chrono::duration_cast<Duration>(
      timestamps.pipeline_complete - timestamps.pipeline_start);

  return {parse_d, solve_d, total_d};
}

} // namespace aoc::solver