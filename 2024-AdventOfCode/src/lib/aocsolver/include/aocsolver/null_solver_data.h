#ifndef __NULL_SOLVER_DATA_H__
#define __NULL_SOLVER_DATA_H__

#include <string>

namespace aoc::solver {

/**
 * Structure with no data acting as
 * a null input type. Generally used
 * as a placeholder for the solver.
 */
struct NullSolverData {
  std::string to_string() const { return ""; }
};

} // namespace aoc::solver

#endif /* __NULL_SOLVER_DATA_H__ */