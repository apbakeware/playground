#ifndef __EXPECTED_SOLUTION_PIPELINE_OBSERVER_H__
#define __EXPECTED_SOLUTION_PIPELINE_OBSERVER_H__

#include <map>
#include <string>

#include "../isolver_pipeline_observer.h"

namespace aoc::solver {

class ExpectedSolutionPipelineObserver : public ISolverPipelineObserver {
public:
  virtual ~ExpectedSolutionPipelineObserver();

  virtual void onSolverPipelineStart(const std::string &solver_name,
                                     const Timestamp &timestamp);

  virtual void onSolverPipelineComplete(const Timestamp &timestamp);

  virtual void onSolverParseInputStart(const Timestamp &timestamp);

  virtual void onSolverParseInputComplete(const Timestamp &timestamp);

  virtual void onSolverRunSolverStart(const Timestamp &timestamp);

  virtual void onSolverRunSolverComplete(const Timestamp &timestamp,
                                         const std::string &solution);

  ExpectedSolutionPipelineObserver &
  addExpectedResult(const std::string &solver_name,
                    const std::string &expected);

  template <typename Operation> void for_each_result(Operation op) {
    for (auto &record : m_result_records) {
      const ResultRecord &result = record.second;
      op(record.first, result.expected, result.actual, result.isCorrect());
    }
  }

private:
  struct ResultRecord {
    std::string expected;
    std::string actual;

    bool isCorrect() const { return expected == actual; }
  };

  using ResultTable = std::map<std::string, ResultRecord>;

  std::string m_current_solver;
  ResultTable m_result_records;
};

} // namespace aoc::solver

#endif /* __EXPECTED_SOLUTION_PIPELINE_OBSERVER_H__ */