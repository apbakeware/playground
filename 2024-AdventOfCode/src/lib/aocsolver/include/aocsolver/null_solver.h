#ifndef __NULL_SOLVER_H__
#define __NULL_SOLVER_H__

#include "spdlog/spdlog.h"

#include "aoc/util/null_type.h"
#include "aocsolver/solver.h"

namespace aoc::solver {

/**
 * The NullSolver class provides the Solver interface requirements
 * for the pipeline.
 */
class NullSolver
    : public Solver<NullSolver, aoc::util::NullType, aoc::util::NullType> {
public:
  using InputType = aoc::util::NullType;
  using ResultType = aoc::util::NullType;

  ResultType solve(InputType) {
    SPDLOG_TRACE("NullSovler::solve()");
    SPDLOG_DEBUG("Using null solver");
    return ResultType{};
  }
};

} // namespace aoc::solver

#endif /* __NULL_SOLVER_H__ */