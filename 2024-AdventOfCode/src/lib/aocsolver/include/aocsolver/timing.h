#ifndef __TIMING_H__
#define __TIMING_H__

#include <chrono>
#include <functional>
#include <iosfwd>

namespace aoc::solver {

using Timestamp = std::chrono::time_point<std::chrono::system_clock>;
using Duration = std::chrono::microseconds;

struct ChronoSystemClockTimestamper {
  static Timestamp getTimestampNow();
};

// Consider declaring a function type and passing htis in to control it more
template <typename Timestamper_T = ChronoSystemClockTimestamper>
Timestamp timestampNow() {
  return Timestamper_T::getTimestampNow();
};

struct WallClockTimestampWriter {
  Timestamp timestamp;

  friend std::ostream &operator<<(std::ostream &ostr,
                                  const WallClockTimestampWriter &obj);
};

struct PipelineTimestampRecord {

  Timestamp pipeline_start;
  Timestamp parse_start;
  Timestamp parse_complete;
  Timestamp solve_start;
  Timestamp solve_complete;
  Timestamp pipeline_complete;

  // TODO: know if its in a valid state?
};

struct PipelineDurationRecord {
  Duration parse_duration;
  Duration solve_duration;
  Duration total_pipeline_duration;
};

PipelineDurationRecord
computeDuration(const PipelineTimestampRecord &timestamps);

} // namespace aoc::solver

#endif /* __TIMING_H__ */