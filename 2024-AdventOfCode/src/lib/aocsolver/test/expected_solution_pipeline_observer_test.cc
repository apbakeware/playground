#include <catch2/catch_test_macros.hpp>

#include <algorithm>
#include <chrono>

#include "aocsolver/pipeline_observers/expected_solution_pipeline_observer.h"

namespace {

using Ms = std::chrono::microseconds;

int cb_count;
std::string cb_solver_name;
std::string cb_expected_result;
std::string cb_actual_result;
bool cb_is_correct;

void reset_cb() {
  cb_count = 0;
  cb_solver_name.clear();
  cb_expected_result.clear();
  cb_actual_result.clear();
  cb_is_correct = false;
}

void callback(const std::string &solver_name, const std::string &expected,
              const std::string &actual, bool is_correct) {

  ++cb_count;
  cb_solver_name = solver_name;
  cb_expected_result = expected;
  cb_actual_result = actual;
  cb_is_correct = is_correct;
}

std::chrono::time_point<std::chrono::system_clock> DONT_CARE_TS =
    std::chrono::time_point<std::chrono::system_clock>(Ms(0));

} // namespace

namespace aoc::solver::test {

TEST_CASE("ExpectedSolutionPipelineObserver Test") {
  aoc::solver::ExpectedSolutionPipelineObserver sut;

  SECTION("Test no registered expectations") {
    const std::string solver_name = "Solver Name";
    const std::string expected_result = "ABC";
    const std::string actual_result = "";

    reset_cb();
    sut.onSolverPipelineStart(solver_name, DONT_CARE_TS);
    sut.onSolverRunSolverComplete(DONT_CARE_TS, actual_result);
    sut.for_each_result(callback);

    REQUIRE(cb_count == 0);
  }

  SECTION("Test registered expectations with correct result") {
    const std::string solver_name = "Solver Name";
    const std::string expected_result = "ABC";
    const std::string actual_result = expected_result;

    reset_cb();
    sut.addExpectedResult(solver_name, expected_result);
    sut.onSolverPipelineStart(solver_name, DONT_CARE_TS);
    sut.onSolverRunSolverComplete(DONT_CARE_TS, actual_result);
    sut.for_each_result(callback);

    CHECK(cb_count == 1);
    CHECK(solver_name == cb_solver_name);
    CHECK(expected_result == cb_expected_result);
    CHECK(actual_result == cb_actual_result);
    CHECK(cb_is_correct);
  }

  SECTION("Test registered expectations with incorrect result") {
    const std::string solver_name = "Solver Name";
    const std::string expected_result = "ABC";
    const std::string actual_result = "123";

    reset_cb();
    sut.addExpectedResult(solver_name, expected_result);
    sut.onSolverPipelineStart(solver_name, DONT_CARE_TS);
    sut.onSolverRunSolverComplete(DONT_CARE_TS, actual_result);
    sut.for_each_result(callback);

    CHECK(cb_count == 1);
    CHECK(solver_name == cb_solver_name);
    CHECK(expected_result == cb_expected_result);
    CHECK(actual_result == cb_actual_result);
    CHECK_FALSE(cb_is_correct);
  }
}

} // namespace aoc::solver::test