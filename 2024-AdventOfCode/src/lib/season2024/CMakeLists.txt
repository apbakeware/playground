project(season2024)

set(SOURCE_FILES 
  src/days/day01part1.cc
  src/days/day01part2.cc
  src/days/day02part1.cc
  src/days/day02part2.cc
  src/days/day03part1.cc
  src/days/day03part2.cc
  src/days/day04part1.cc
  src/days/day04part2.cc
  src/days/day05part1.cc
  src/days/day05part2.cc
  src/days/day06part1.cc
  src/days/day06part2.cc
  src/days/day07part1.cc
  src/days/day07part2.cc
  src/days/day08part1.cc
  src/days/day08part2.cc
  src/days/day09part1.cc
  src/days/day09part2.cc
  src/days/day10part1.cc
  src/days/day10part2.cc
  src/days/day11part1.cc
  src/days/day11part2.cc
  src/days/day12part1.cc
  src/days/day12part2.cc
  src/days/day13part1.cc
  src/days/day13part2.cc
  src/days/day14part1.cc
  src/days/day14part2.cc
  src/days/day15part1.cc
  src/days/day15part2.cc
  src/days/day16part1.cc
  src/days/day16part2.cc
  src/days/day17part1.cc
  src/days/day17part2.cc
  src/days/day18part1.cc
  src/days/day18part2.cc
  src/days/day19part1.cc
  src/days/day19part2.cc
  src/days/day20part1.cc
  src/days/day20part2.cc
  src/days/day21part1.cc
  src/days/day21part2.cc
  src/days/day22part1.cc
  src/days/day22part2.cc
  src/days/day23part1.cc
  src/days/day23part2.cc
  src/days/day24part1.cc
  src/days/day24part2.cc
  src/days/day25part1.cc
  src/days/day25part2.cc
  src/types/bathroom_robot.cc
  src/types/chronospatial_computer.cc
  src/types/claw_machine.cc
  src/types/disk.cc
  src/types/fruit_monitor.cc
  src/types/integer_column_data.cc
  src/types/lanternfish_warehouse.cc
  src/types/lan_party.cc
  src/types/monkey_market.cc
  src/types/easter_bunny_transmitter.cc
  src/types/onsen_towels.cc
  src/types/patrol_guard.cc
  src/types/pluto_stones.cc
  src/types/ram_run.cc
  src/types/racetrack.cc
  src/types/reindeer_maze.cc
  src/types/rope_bridge_equation.cc
  src/types/safety_manual_printer.cc
  src/types/tumbler_lock.cc
)

add_library(${PROJECT_NAME} ${SOURCE_FILES})

add_library(libs::${PROJECT_NAME} ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
	PUBLIC
		$<INSTALL_INTERFACE:include>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
	PRIVATE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/>
)


target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS} spdlog aoc aocsolver aocinput)

##################
# Testing
if(BUILD_TESTS)
	add_subdirectory(test)
endif()

