#include "season2024/types/tumbler_lock.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day25part1.h"

namespace aoc::season2024 {

Day25Part1::ResultType Day25Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day25Part1::solve()");
  int count = 0;
  for (const auto &key : input.keys) {
    for (const auto &lock : input.locks) {
      if (types::keyFitsInLock(key, lock)) {
        ++count;
      }
    }
  }

  return count;
}

} // namespace aoc::season2024
