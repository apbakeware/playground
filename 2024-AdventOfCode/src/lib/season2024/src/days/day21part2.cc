#include "spdlog/spdlog.h"

#include "season2024/days/day21part2.h"

namespace aoc::season2024 {

Day21Part2::ResultType Day21Part2::solve(InputType & input) {
  SPDLOG_TRACE("Day21Part2::solve()");
  (void)(input);
  return ResultType{};
}

} // namespace aoc::season2024

