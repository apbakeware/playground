#include <algorithm>
#include <set>
#include <vector>

#include "spdlog/fmt/ostr.h"
#include "spdlog/spdlog.h"

#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_navigation.h"
#include "aoc/grid/grid_utils.h"

#include "season2024/days/day06part1.h"
#include "season2024/types/patrol_guard.h"

namespace aoc::season2024 {

Day06Part1::ResultType Day06Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day06Part1::solve()");

  const char GUARD = '^';

  auto grid = aoc::grid::flip_horizontal(input);
  auto find_guard = aoc::grid::find_value(grid, GUARD);

  if (!find_guard.second) {
    SPDLOG_ERROR("Guard not found!!!!");
    return -1;
  }

  PatrolGuard guard = {find_guard.first, aoc::grid::Orientation4::UP};
  const auto patrol_navigation = patrolAreaUntilExit(guard, grid);

  std::set<grid::Grid_Location> unique_locations;
  auto location_inserter = [&unique_locations](const auto &patrol) {
    unique_locations.insert(patrol.location);
  };

  std::for_each(patrol_navigation.begin(), patrol_navigation.end(),
                location_inserter);

  return unique_locations.size();
}

} // namespace aoc::season2024
