#include "spdlog/spdlog.h"

#include "season2024/days/day20part2.h"

namespace aoc::season2024 {

Day20Part2::ResultType Day20Part2::solve(InputType & input) {
  SPDLOG_TRACE("Day20Part2::solve()");
  (void)(input);
  return ResultType{};
}

} // namespace aoc::season2024

