#include "spdlog/spdlog.h"

#include "season2024/days/day25part2.h"

namespace aoc::season2024 {

Day25Part2::ResultType Day25Part2::solve(InputType & input) {
  SPDLOG_TRACE("Day25Part2::solve()");
  (void)(input);
  return ResultType{};
}

} // namespace aoc::season2024

