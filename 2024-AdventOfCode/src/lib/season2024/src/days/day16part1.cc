

#include "spdlog/spdlog.h"

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"

#include "season2024/days/day16part1.h"
#include "season2024/types/reindeer_maze.h"

namespace aoc::season2024 {

Day16Part1::ResultType Day16Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day16Part1::solve()");

  const auto &cost_grid = input.createCostGrid();
  return cost_grid.at(input.end);
}

} // namespace aoc::season2024
