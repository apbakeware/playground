#include <string>
#include <unordered_set>

#include "season2024/types/chronospatial_computer.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day17part2.h"

namespace aoc::season2024 {

Day17Part2::ResultType Day17Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day17Part2::solve()");

  return Day17Part2::ResultType{};

  // std::unordered_set<std::string> results;
  // ChronospatialComputer::ValueType val = 0;
  // std::vector<ChronospatialComputer::ValueType> output;
  // auto output_builder = [&output](const auto value) -> void {
  //   output.push_back(value);
  // };

  // bool keep_running = true;
  // while (keep_running) {
  //   output.clear();
  //   input.reset(val, 0, 0);
  //   input.executeProgram(output_builder);

  //   const auto pgm_output =
  //       fmt::format("{}", fmt::join(output.begin(), output.end(), ","));
  //   auto try_insert = results.insert(pgm_output);
  //   if (!try_insert.second) {
  //     fmt::print("Val: {} duplicate output: {}\n", val, pgm_output);
  //   }
  //   ++val;
  //   if (val == 2000) {
  //     break;
  //   }
  // }

  // bool keep_running = true;
  // while (keep_running) {
  //   output.clear();
  //   input.reset(val, 0, 0);
  //   input.executeProgram(output_builder);

  //   const auto pgm_output =
  //       fmt::format("{}", fmt::join(output.begin(), output.end(), ","));
  //   fmt::print("Val: {} duplicate output: {}\n", val, pgm_output);
  //   val++;
  //   if (val == 2000) {
  //     break;
  //   }
  // }

  // Pattern recognized by using this...
  //  - 8 values in a row return the same result, so only need to val + 8 for
  //    unique values
  //  - the first 0-7 output is 1 value, 8-15 is 2...each 8 more adds 1 set
  //    of output values
  //  - the leading values increment by 1 and "roll over" to the next value at 8

  // ChronospatialComputer::ValueType val = 0;
  // const ChronospatialComputer::ValueType initial_a = input.registerA();
  // while (!input.runToSeeIfOutputMatchesProgram(val, 0, 0)) {
  //   if (val % 100 == 0) {
  //     fmt::print("Val: {}\n", val);
  //   }
  //   val += 8;
  //   if (initial_a == val) {
  //     val += 8;
  //   }
  // }

  // return val;
}

} // namespace aoc::season2024
