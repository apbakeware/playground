#include <deque>
#include <iostream>
#include <iterator>
#include <limits>
#include <map> // todo hashable

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_traversal.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day18part1.h"

namespace aoc::season2024 {

Day18Part1::ResultType Day18Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day18Part1::solve()");

  // using LocationDistance = grid::Location_Value<int>;

  // const aoc::grid::Grid_Location start = {0, 0};
  // // This is different between sample and real and solver doesnt know which
  // const aoc::grid::Grid_Location dest = {70, 70};

  // // Mark visited and store the distance to the location
  // std::map<grid::Grid_Location, int> visited_map;
  // std::deque<LocationDistance> location_stack;
  // std::array<grid::Grid_Location, 4> neighbors;

  // grid::Fixed_Sized_Grid<char> vis_grid(72, 72, '.');

  // auto block_iter = input.locations.begin();
  // while (block_iter != input.locations.end() &&
  //        std::distance(input.locations.begin(), block_iter) < 1024) {
  //   visited_map[*block_iter] = 99999;
  //   ++block_iter;
  // }

  // location_stack.push_back({start, 0});
  // int last_value = -1;
  // int stopper = 100000;
  // while (!location_stack.empty()) {
  //   assert(--stopper > 0);
  //   const auto node = location_stack.front();
  //   location_stack.pop_front();

  //   // drop the block, this may be "on your next location, but your faster?"
  //   // This works for example, but does this all the neighbors at the same
  //   // level correctly? it does neighbors for node a ta level, but what about
  //   // fan levels?
  //   // if (node.value != last_value) {
  //   //   last_value = node.value;
  //   //   if (std::distance(input.locations.begin(), block_iter) < 1024) {
  //   //     visited_map[*block_iter] = 99999;
  //   //     ++block_iter;
  //   //     vis_grid.set(*block_iter, '#');
  //   //   }
  //   //   fmt::print("level: {}\n{}\n", last_value, vis_grid);
  //   // }

  //   if (visited_map.count(node.location) == 0) {
  //     vis_grid.set(node.location, 'O');
  //     visited_map.insert({node.location, node.value});

  //     // fmt::print("Visiting: {}  value: {}\n", node.location, node.value);

  //     if (node.location == dest) {
  //       break;
  //     }

  //     neighbors[0] = grid::Grid_Location::up(node.location);
  //     neighbors[1] = grid::Grid_Location::right(node.location);
  //     neighbors[2] = grid::Grid_Location::down(node.location);
  //     neighbors[3] = grid::Grid_Location::left(node.location);

  //     for (const auto &neighbor : neighbors) {
  //       bool add_neighbor = grid::is_within_bound(neighbor, start, dest) &&
  //                           visited_map.count(neighbor) == 0;
  //       if (add_neighbor) {
  //         location_stack.push_back({neighbor, node.value + 1});
  //       }
  //     }
  //   }
  // }

  // return visited_map[dest];

  // Real values
  const aoc::grid::Grid_Location start = {0, 0};
  const aoc::grid::Grid_Location dest = {70, 70};
  const size_t drop_count = 1024;

  // Test values
  // const aoc::grid::Grid_Location start = {0, 0};
  // const aoc::grid::Grid_Location dest = {6, 6};
  // const size_t drop_count = 12;

  auto ram_grid = input.runRamGrid(start, dest, drop_count);
  return ram_grid.cost_grid.at(dest);
}

} // namespace aoc::season2024
