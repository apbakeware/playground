#include "spdlog/spdlog.h"

#include "season2024/days/day07part1.h"

namespace {

template <typename Iter_T>
int perform_operators_recursively(Iter_T begin, Iter_T end, int current,
                                  int target) {

  if (begin == end) {
    return (current == target) ? 1 : 0;
  }

  int value = *begin;
  ++begin;

  return perform_operators_recursively(begin, end, current + value, target) +
         perform_operators_recursively(begin, end, current * value, target);
}

} // namespace

namespace aoc::season2024 {

Day07Part1::ResultType Day07Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day07Part1::solve()");

  ResultType count = 0;
  for (const auto &eqn : input.equations) {
    auto begin = eqn.operands.begin();
    auto end = eqn.operands.end();
    const int possibilities = perform_operators_recursively(
        ++begin, end, eqn.operands.front(), eqn.answer);

    count += possibilities > 0 ? eqn.answer : 0;
  }

  return count;
}

} // namespace aoc::season2024
