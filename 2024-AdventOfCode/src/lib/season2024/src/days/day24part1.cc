#include <functional>
#include <list>

#include "season2024/types/fruit_monitor.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day24part1.h"

namespace {

uint64_t computeZWireValues(
    const aoc::season2024::fruit_monitor::WireTable &wire_table) {

  uint64_t result = 0;
  std::vector<aoc::season2024::fruit_monitor::WireIdType> z_wires;
  for (const auto &wire : wire_table) {
    if (wire.first.front() == 'z') {
      z_wires.push_back(wire.first);
    }
  }

  std::sort(z_wires.begin(), z_wires.end());
  for (size_t idx = 0; idx < z_wires.size(); ++idx) {
    const auto wire_id = z_wires[idx];
    const auto iter = wire_table.find(wire_id);
    if (iter == wire_table.end()) {
      SPDLOG_ERROR("unexpected end of wires");
      continue;
    }

    if (iter->second == aoc::season2024::fruit_monitor::ONE) {
      result |= (0x1ull << idx);
    }
    fmt::print("WireID: {}  {:>48b} {}\n", wire_id, result, iter->second);
  }

  return result;
}

} // namespace

namespace aoc::season2024 {

Day24Part1::ResultType Day24Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day24Part1::solve()");

  std::list<fruit_monitor::Gate> pending_gates(input.gates.begin(),
                                               input.gates.end());

  auto iter = pending_gates.begin();

  while (iter != pending_gates.end()) {
    if (iter->trySolve(input.wire_table)) {
      iter = pending_gates.erase(iter);
    } else {
      ++iter;
    }

    if (iter == pending_gates.end()) {
      iter = pending_gates.begin();
    }
  }

  return computeZWireValues(input.wire_table);
}

} // namespace aoc::season2024
