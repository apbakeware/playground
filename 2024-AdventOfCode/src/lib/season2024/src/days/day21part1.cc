#include "spdlog/spdlog.h"

#include "season2024/days/day21part1.h"

namespace aoc::season2024 {

Day21Part1::ResultType Day21Part1::solve(InputType & input) {
  SPDLOG_TRACE("Day21Part1::solve()");
  (void)(input);
  return ResultType{};
}

} // namespace aoc::season2024

