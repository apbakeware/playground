#include "spdlog/spdlog.h"

#include "season2024/days/day12part1.h"

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_utils.h"

namespace aoc::season2024 {

// pair of area and perimeter
void walkGardenArea(const grid::Fixed_Sized_Grid<char> &garden,
                    const grid::Grid_Location &location,
                    grid::Fixed_Sized_Grid<bool> &vistation_grid, int &area,
                    int &perimiter) {

  // fmt::print("Checking: {}\n", location);
  if (vistation_grid.at(location)) {
    // fmt::print("--> has been visited\n");
    return;
  }

  const std::vector<grid::Grid_Location> surrounding = {
      grid::Grid_Location::up(location), grid::Grid_Location::right(location),
      grid::Grid_Location::down(location), grid::Grid_Location::left(location)};

  vistation_grid.set(location, true);
  const auto my_value = garden.at(location); // This could be passed in
  for (const auto &neighbor : surrounding) {
    // fmt::print("  Checking neighbor: {}", neighbor);
    if (grid::is_on_grid(neighbor, garden)) {
      if (garden.at(neighbor) != my_value) {
        // fmt::print("--> different crop, adding perimeter\n");
        ++perimiter;
      } else {
        if (!vistation_grid.at(neighbor)) {
          walkGardenArea(garden, neighbor, vistation_grid, area, perimiter);
        } else {
          // fmt::print("--> same crop, already visited\n");
        }
      }
    } else {
      // fmt::print("--> not on grid, adding perimeter\n");
      ++perimiter;
    }
  }

  ++area;
}

Day12Part1::ResultType Day12Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day12Part1::solve()");

  const auto &lines = input.lines();
  const auto &grid =
      aoc::grid::create_char_grid_from_string_rows(lines.begin(), lines.end());

  grid::Fixed_Sized_Grid<bool> visited_grid(grid.number_of_rows(),
                                            grid.number_of_cols(), false);

  // fmt::print("Garden:\n{}\n", grid);

  // Is there a way to nicely use the for_each_cell?
  int price = 0;
  for (auto row = 0; row < grid.number_of_rows(); ++row) {
    for (auto col = 0; col < grid.number_of_cols(); ++col) {
      const aoc::grid::Grid_Location location = {row, col};
      if (!visited_grid.at(location)) {
        int area = 0;
        int perimeter = 0;

        walkGardenArea(grid, location, visited_grid, area, perimeter);
        const int my_price = area * perimeter;
        // fmt::print("Crop: {}  Price: {}\n", grid.at(location), my_price);
        price += my_price;
      }
    }
  }

  return price;
}

} // namespace aoc::season2024
