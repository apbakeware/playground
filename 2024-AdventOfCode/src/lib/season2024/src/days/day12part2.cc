#include "spdlog/spdlog.h"

#include "season2024/days/day12part2.h"

namespace aoc::season2024 {

Day12Part2::ResultType Day12Part2::solve(InputType & input) {
  SPDLOG_TRACE("Day12Part2::solve()");
  (void)(input);
  return ResultType{};
}

} // namespace aoc::season2024

