#include <algorithm>
#include <iostream>
#include <iterator>

#include "spdlog/spdlog.h"

#include "season2024/types/safety_manual_printer.h"

namespace aoc::season2024 {

void SafteyManualPrintRules::addRule(int page_number, int dependency) {

  SPDLOG_DEBUG("Added print rule: {} before {}", page_number, dependency);

  m_dependencies.insert({page_number, dependency});
}

bool SafteyManualPrintRules::validatePrintQueue(
    const std::vector<int> &print_queue) {

  for (auto pq_iter = print_queue.begin(); pq_iter != print_queue.end();
       ++pq_iter) {
    const int page = *pq_iter;

    // SPDLOG_DEBUG("check page: {}", page);

    // Get the dependencies of the page
    auto dependencies = m_dependencies.equal_range(page);
    for (auto dep_iter = dependencies.first; dep_iter != dependencies.second;
         ++dep_iter) {

      // Did the depenent page number occur prior to this point in the queue?
      const auto dependency = dep_iter->second;
      // SPDLOG_DEBUG("Dependency: {}", dependency);
      const auto found_dep =
          std::find(print_queue.begin(), pq_iter, dependency);

      const bool dependency_violation = found_dep != pq_iter;

      if (dependency_violation) {
        // SPDLOG_DEBUG("Dependency violation: {} must occur before {}", page,
        //              dependency);
        return false;
      }
    }
  }

  return true;
}

bool SafteyManualPrintRules::fixPrintQueue(const std::vector<int> &print_queue,
                                           std::vector<int> &out_fixed_queue) {

  int checks = 0;

  bool fix_was_made = false;
  out_fixed_queue = print_queue;

  auto pq_iter = out_fixed_queue.begin();

  std::advance(pq_iter, 1);
  while (pq_iter != out_fixed_queue.end()) {
    ++checks;
    assert(checks < 1000);
    const int page = *pq_iter;
    auto dependencies = m_dependencies.equal_range(page);
    bool dependency_violation = false;

    for (auto dep_iter = dependencies.first;
         dep_iter != dependencies.second && !dependency_violation; ++dep_iter) {

      // Did the depenent page number occur prior to this point in the queue?
      const auto dependency = dep_iter->second;
      auto found_dep = std::find(out_fixed_queue.begin(), pq_iter, dependency);
      dependency_violation = found_dep != pq_iter;

      if (dependency_violation) {
        fix_was_made = true;
        std::iter_swap(pq_iter, found_dep);
        pq_iter = found_dep;
        if (pq_iter == out_fixed_queue.begin()) {
          std::advance(pq_iter, 1);
        }
      }
    }

    if (!dependency_violation) {
      ++pq_iter;
    }
  }
  return fix_was_made;
}

std::istream &operator>>(std::istream &instr, SafetyManualPrintJobs &rec) {

  std::string line;
  int page;
  int page_before;
  char sep;

  SPDLOG_DEBUG("Parsing order rules");
  while (std::getline(instr, line)) {
    if (line.empty()) {
      break;
    }
    std::istringstream sstr(line);
    sstr >> page >> sep >> page_before;
    rec.rules.addRule(page, page_before);
  }

  SPDLOG_DEBUG("Parsing queue");
  while (std::getline(instr, line)) {
    SafetyManualPrintJobs::PrintQueue print_queue;
    std::replace(line.begin(), line.end(), ',', ' ');
    std::istringstream sstr(line);
    while (sstr >> page) {
      print_queue.push_back(page);
    }

    SPDLOG_DEBUG("Print queue: {}",
                 fmt::join(print_queue.begin(), print_queue.end(), " "));
    rec.print_queues.emplace_back(print_queue);
  }

  return instr;
}

} // namespace aoc::season2024