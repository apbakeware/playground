#include <deque>
#include <iostream>
#include <limits>
#include <map>
#include <string>
#include <vector>

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_navigation.h"
#include "aoc/grid/grid_utils.h"

#include "season2024/types/reindeer_maze.h"

namespace {

struct GridPosNavCost {
  aoc::grid::Orientation4 orient;
  aoc::grid::Grid_Location loc;
  int cost;
};

void printCostGrid(const aoc::grid::Fixed_Sized_Grid<int> &grid) {

  for (auto row = grid.number_of_rows(); row > 0; --row) {
    for (auto col = 0; col < grid.number_of_cols(); ++col) {
      const aoc::grid::Grid_Location location = {row - 1, col};
      const auto value = grid.at(location);
      if (value == std::numeric_limits<int>::max()) {
        fmt::print("  ? ");
      } else {
        fmt::print("{:>3} ", value);
      }
    }
    fmt::print("\n");
  }
}

using VistationStore = std::map<aoc::grid::Grid_Location, bool>;

// Assumes loc is always on grid
void dfsLeastCostPaths(const aoc::grid::Grid_Location &loc,
                       const aoc::grid::Grid_Location &dest,
                       const aoc::grid::Fixed_Sized_Grid<int> &cost_grid,
                       const int dest_cost) {

  fmt::print("Location: {} -- cost: {}\n", loc, cost_grid.at(loc));

  if (loc == dest) {
    fmt::print("Found destination\n");
  }

  std::array<aoc::grid::Grid_Location, 4> neighbors = {
      aoc::grid::Grid_Location::up(loc),
      aoc::grid::Grid_Location::right(loc),
      aoc::grid::Grid_Location::down(loc),
      aoc::grid::Grid_Location::left(loc),
  };

  for (const auto &neighbor : neighbors) {
    if (cost_grid.at(neighbor) <= dest_cost &&
        cost_grid.at(neighbor) > cost_grid.at(loc)) {
      dfsLeastCostPaths(neighbor, dest, cost_grid, dest_cost);
    }
  }

  // When to unvisit
}

} // namespace

template <>
struct fmt::formatter<GridPosNavCost> : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the MyType instance
  template <typename FormatContext>
  auto format(const GridPosNavCost &obj, FormatContext &ctx) const
      -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    return fmt::format_to(ctx.out(),
                          "GridPosNav(orient: {}  pos: {}  cost: {})",
                          obj.orient, obj.loc, obj.cost);
  }
};

namespace aoc::season2024 {

bool ReindeerMaze::isOpen(const aoc::grid::Grid_Location &location) const {
  return maze.at(location) == ReindeerMaze::OPEN;
}

bool ReindeerMaze::isWall(const aoc::grid::Grid_Location &location) const {
  return maze.at(location) == ReindeerMaze::WALL;
}

bool ReindeerMaze::isEnd(const aoc::grid::Grid_Location &location) const {
  return maze.at(location) == ReindeerMaze::GOAL;
}

grid::Fixed_Sized_Grid<int> ReindeerMaze::createCostGrid() const {
  std::deque<GridPosNavCost> queue;
  aoc::grid::Fixed_Sized_Grid<int> cost_grid(maze.number_of_rows(),
                                             maze.number_of_cols(),
                                             std::numeric_limits<int>::max());

  // Compute the lowest cost to each grid location
  queue.push_back({aoc::grid::Orientation4::RIGHT, start, 0});
  while (!queue.empty()) {

    const auto nav = queue.front();
    queue.pop_front();

    if (nav.cost < cost_grid.at(nav.loc)) {
      cost_grid.set(nav.loc, nav.cost);

      const auto ahead = aoc::grid::ahead(nav.orient, nav.loc);
      if (!isWall(ahead)) {
        queue.push_back(
            {nav.orient, ahead, nav.cost + ReindeerMaze::MOVE_COST});
      }

      const auto cw_orient = aoc::grid::rotate_cw(nav.orient);
      const auto ahead_cw = aoc::grid::ahead(cw_orient, nav.loc);
      const auto ccw_orient = aoc::grid::rotate_ccw(nav.orient);
      const auto ahead_ccw = aoc::grid::ahead(ccw_orient, nav.loc);

      if (!isWall(ahead_cw)) {
        queue.push_back(
            {cw_orient, ahead_cw,
             nav.cost + ReindeerMaze::ROTATE_COST + ReindeerMaze::MOVE_COST});
      }

      if (!isWall(ahead_ccw)) {
        queue.push_back(
            {ccw_orient, ahead_ccw,
             nav.cost + ReindeerMaze::ROTATE_COST + ReindeerMaze::MOVE_COST});
      }
    }

    // printCostGrid(cost_grid);
    // fmt::print("done with step\n\n");
  }

  return cost_grid;
}

void ReindeerMaze::renderCostGridPath(
    const grid::Fixed_Sized_Grid<int> &cost_grid) {
  auto render_maze = maze;

  auto addPath = [&render_maze](const auto &loc, const auto val) {
    if (val != std::numeric_limits<int>::max()) {
      render_maze.set(loc, 'O');
    }
  };

  cost_grid.for_each_cell(addPath);
  fmt::print("{}\n", render_maze);
}

void ReindeerMaze::findAllLeastCostPaths(
    const grid::Fixed_Sized_Grid<int> &cost_grid) const {

  fmt::print("Finding\n");
  dfsLeastCostPaths(start, end, cost_grid, cost_grid.at(end));
}

std::istream &operator>>(std::istream &instr, ReindeerMaze &obj) {

  std::vector<std::string> lines;
  std::string line;

  while (std::getline(instr, line)) {
    if (line.empty()) {
      break;
    }
    lines.push_back(line);
  }

  obj.maze =
      grid::create_char_grid_from_string_rows(lines.begin(), lines.end());

  const auto &start = grid::find_value(obj.maze, ReindeerMaze::START);
  if (start.second) {
    obj.start = start.first;
  } else {
    SPDLOG_ERROR("Could not find maze starting point");
  }

  const auto &goal = grid::find_value(obj.maze, ReindeerMaze::GOAL);
  if (goal.second) {
    obj.end = goal.first;
  } else {
    SPDLOG_ERROR("Could not find maze end point");
  }

  return instr;
}

} // namespace aoc::season2024
