#include <iostream>
#include <regex>
#include <string>

#include "spdlog/spdlog.h"

#include "season2024/types/claw_machine.h"

namespace {

std::regex button_regex(R"(\bX\+(\d+),\s*Y\+(\d+))");
std::regex prize_regex(R"(\bX\=(\d+),\s*Y\=(\d+))");

} // namespace

namespace aoc::season2024 {

std::istream &operator>>(std::istream &instr, ClawMachine &obj) {

  std::string line;
  std::smatch matches;

  std::getline(instr, line);
  if (line.empty()) {
    std::getline(instr, line);
  }

  if (std::regex_search(line, matches, button_regex)) {
    // matches[0] is the entire match, matches[1] is X, matches[2] is Y
    if (matches.size() == 3) {
      obj.m_button_a_movement = {std::stoi(matches[2].str()),
                                 std::stoi(matches[1].str())};
    }
  }

  std::getline(instr, line);
  if (std::regex_search(line, matches, button_regex)) {
    // matches[0] is the entire match, matches[1] is X, matches[2] is Y
    if (matches.size() == 3) {
      obj.m_button_b_movement = {std::stoi(matches[2].str()),
                                 std::stoi(matches[1].str())};
    }
  }

  std::getline(instr, line);
  if (std::regex_search(line, matches, prize_regex)) {
    // matches[0] is the entire match, matches[1] is X, matches[2] is Y
    if (matches.size() == 3) {
      obj.m_prize_location = {std::stoi(matches[2].str()),
                              std::stoi(matches[1].str())};
    }
  }

  return instr;
} // namespace aoc::season2024

} // namespace aoc::season2024