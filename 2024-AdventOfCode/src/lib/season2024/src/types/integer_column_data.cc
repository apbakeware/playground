#include <iostream>

#include "season2024/types/integer_column_data.h"

namespace aoc::season2024::types {

std::istream &operator>>(std::istream &instr, IntegerColumnData &record) {
  int left = 0;
  int right = 0;

  while (instr >> left >> right) {
    record.left.push_back(left);
    record.right.push_back(right);
  }
  return instr;
}

} // namespace aoc::season2024::types