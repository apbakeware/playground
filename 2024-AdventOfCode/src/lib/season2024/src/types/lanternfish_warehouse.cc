#include <functional>
#include <iostream>

#include "aoc/grid/grid_traversal.h"
#include "spdlog/spdlog.h"

#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_utils.h"

#include "season2024/types/lanternfish_warehouse.h"

namespace {

using MovementFunction =
    std::function<aoc::grid::Grid_Location(const aoc::grid::Grid_Location &)>;

MovementFunction movementForRobotCommand(char command) {
  if (command == '^') {
    return std::bind(aoc::grid::Grid_Location::up, std::placeholders::_1, 1);
  } else if (command == '>') {
    return std::bind(aoc::grid::Grid_Location::right, std::placeholders::_1, 1);
  } else if (command == '<') {
    return std::bind(aoc::grid::Grid_Location::left, std::placeholders::_1, 1);
  } else if (command == 'v') {
    return std::bind(aoc::grid::Grid_Location::down, std::placeholders::_1, 1);
  }

  return aoc::grid::Grid_Location::stay;
  SPDLOG_ERROR("Unknown robot movement command '{}'", command);
}

} // namespace

namespace aoc::season2024 {

char LanternfishWarehouse::peekInDirection(char movement) {
  auto cmd = movementForRobotCommand(movement);
  const auto peek_at = cmd(robot_location);
  return warehouse_grid.at(peek_at);
}

ValidGridLocation LanternfishWarehouse::findOpenInDirection(char direction) {

  ValidGridLocation result = {{0, 0}, false};

  auto noop = [](const grid::Grid_Location &location, char value) {
    (void)location;
    (void)value;
  };

  auto stop_predicate = [&result](const grid::Grid_Location &location,
                                  char value) {
    if (value == LanternfishWarehouse::OPEN) {
      result.location = location;
      result.is_valid = true;
      return true;
    }

    return value == WALL;
  };

  auto mvmt = movementForRobotCommand(direction);
  grid::walk_until(warehouse_grid, robot_location, mvmt, noop, stop_predicate);

  return result;
}

void LanternfishWarehouse::executeRobotCommand(char command) {
  auto mvmt = movementForRobotCommand(command);
  auto open_space_in_direction = findOpenInDirection(command);

  if (open_space_in_direction) {
    if (peekInDirection(command) == OPEN) {
      warehouse_grid.set(robot_location, OPEN);
      robot_location = open_space_in_direction.location;
      warehouse_grid.set(robot_location, ROBOT);
    } else {
      warehouse_grid.set(robot_location, OPEN);
      robot_location = mvmt(robot_location);
      warehouse_grid.set(robot_location, ROBOT);
      warehouse_grid.set(open_space_in_direction.location, CRATE);
    }
  }
}

std::vector<aoc::grid::Grid_Location>
LanternfishWarehouse::allCrateLocations() const {

  return aoc::grid::find_all_locations_of(warehouse_grid, CRATE);
}

std::istream &operator>>(std::istream &instr, LanternfishWarehouse &obj) {

  // Likely wasteful creating the lines vector...revist as time allows
  // but wanna use the helper function
  std::vector<std::string> lines;
  std::string line;

  while (std::getline(instr, line)) {
    if (line.empty()) {
      break;
    }
    lines.push_back(line);
  }

  obj.warehouse_grid =
      grid::create_char_grid_from_string_rows(lines.begin(), lines.end());

  while (std::getline(instr, line)) {
    obj.robot_commands += line;
  }

  const auto robot =
      grid::find_value(obj.warehouse_grid, LanternfishWarehouse::ROBOT);

  if (!robot.second) {
    SPDLOG_ERROR("Robot not found");
  }

  obj.robot_location = robot.first;

  return instr;
}

} // namespace aoc::season2024