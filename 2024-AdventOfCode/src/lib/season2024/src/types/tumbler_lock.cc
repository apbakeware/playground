#include <iostream>
#include <string>

#include "season2024/types/tumbler_lock.h"

namespace aoc::season2024::types {

bool keyFitsInLock(const HeightList &key, const HeightList &lock) {

  for (size_t idx = 0; idx < static_cast<size_t>(LocksAndKeys::TUMBLER_HEIGHT);
       ++idx) {

    if (key[idx] + lock[idx] > LocksAndKeys::TUMBLER_HEIGHT) {
      return false;
    }
  }

  return true;
}

std::istream &operator>>(std::istream &instr, LocksAndKeys &obj) {

  std::string line;

  while (getline(instr, line)) {
    HeightList *current = nullptr;

    // Start at -1 for key because the bottom
    // inidcator row will be all # and counted
    // where as the lock's header will be discarded
    if (line == "#####") {
      obj.locks.push_back({0, 0, 0, 0, 0});
      current = &obj.locks.back();
    } else {
      obj.keys.push_back({-1, -1, -1, -1, -1});
      current = &obj.keys.back();
    }

    while (getline(instr, line)) {
      if (line.empty()) {
        break;
      }
      for (size_t idx = 0; idx < line.size(); ++idx) {
        if (line[idx] == '#') {
          current->at(idx) += 1;
        }
      }
    }
  }

  return instr;
}

// In this example, converting both locks to pin heights produces:

// 0,5,3,4,3
// 1,2,0,5,3
// Converting all three keys to heights produces:

// 5,0,2,1,3
// 4,3,4,0,2
// 3,0,2,0,1

} // namespace aoc::season2024::types