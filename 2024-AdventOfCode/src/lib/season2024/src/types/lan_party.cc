#include <algorithm>
#include <array>
#include <iostream>
#include <string>
#include <unordered_set>

#include "spdlog/fmt/bundled/core.h"

#include "season2024/types/lan_party.h"

namespace aoc::season2024 {

std::string unordered_triangle(const std::string &ptA, const std::string &ptB,
                               const std::string &ptC) {
  std::array<std::string, 3> ary = {ptA, ptB, ptC};
  std::sort(ary.begin(), ary.end());
  return ary[0] + ary[1] + ary[2];
}

// form a triange of point ptA ptB ptC with legs ab ac bc
// epA -- endpoint
int countThreeWayHistorianOptions(const LanParty &lan) {
  // Build a 3 way network a - b - c where a->b b->c c->a
  std::unordered_set<std::string> unique_tris;
  for (const auto &ptA : lan.historian_options) {
    const auto &ptsB = lan.connections.find(ptA)->second;
    for (const auto &ptB : ptsB) {

      if (ptA == ptB) {
        continue;
      }

      const auto &ptsC = lan.connections.find(ptB)->second;
      for (const auto &ptC : ptsC) {

        if (ptB == ptC) {
          continue;
        }

        const auto &ptsA = lan.connections.find(ptC)->second;
        if (ptsA.count(ptA) != 0) {

          const std::string triangle = unordered_triangle(ptA, ptB, ptC);
          if (unique_tris.count(triangle) == 0) {
            unique_tris.insert(triangle);
          }
        }
      }
    }
  }

  // 1150 is too high
  return unique_tris.size();
}

std::istream &operator>>(std::istream &instr,
                         ::aoc::season2024::LanParty &obj) {
  std::string line;
  while (instr >> line) {
    const auto sep = line.find('-');
    const std::string left = line.substr(0, sep);
    const std::string right = line.substr(sep + 1);

    auto iter = obj.connections.find(left);
    if (iter == obj.connections.end()) {
      LanParty::Names names = {right};
      obj.connections.insert(std::make_pair(left, names));
    } else {
      (iter->second).insert(right);
    }

    iter = obj.connections.find(right);
    if (iter == obj.connections.end()) {
      LanParty::Names names = {left};
      obj.connections.insert(std::make_pair(right, names));
    } else {
      (iter->second).insert(left);
    }

    if (left[0] == 't') {
      obj.historian_options.insert(left);
    }

    if (right[0] == 't') {
      obj.historian_options.insert(right);
    }
  }
  return instr;
}

} // namespace aoc::season2024