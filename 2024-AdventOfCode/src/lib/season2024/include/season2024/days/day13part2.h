#ifndef __DAY13PART2_H__
#define __DAY13PART2_H__

#include "aocinput/value_collection.h"
#include "aocsolver/solver.h"
#include "season2024/types/claw_machine.h"

namespace aoc::season2024 {

class Day13Part2
    : public aoc::solver::Solver<
          Day13Part2, aoc::input::ValueCollection<ClawMachine>, long> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY13PART2_H__ */
