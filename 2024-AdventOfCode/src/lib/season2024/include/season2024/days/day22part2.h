#ifndef __DAY22PART2_H__
#define __DAY22PART2_H__

#include "aocinput/value_collection.h"
#include "aocsolver/solver.h"
#include "season2024/types/monkey_market.h"

namespace aoc::season2024 {

class Day22Part2
    : public aoc::solver::Solver<
          Day22Part2, aoc::input::ValueCollection<monkey_market::ValueType>,
          monkey_market::ValueType> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY22PART2_H__ */
