#ifndef __DAY22PART1_H__
#define __DAY22PART1_H__

#include "aocinput/value_collection.h"
#include "aocsolver/solver.h"
#include "season2024/types/monkey_market.h"

namespace aoc::season2024 {

class Day22Part1
    : public aoc::solver::Solver<
          Day22Part1, aoc::input::ValueCollection<monkey_market::ValueType>,
          monkey_market::ValueType> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY22PART1_H__ */
