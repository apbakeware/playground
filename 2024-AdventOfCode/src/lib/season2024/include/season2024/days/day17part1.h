#ifndef __DAY17PART1_H__
#define __DAY17PART1_H__

#include <string>

#include "aocsolver/solver.h"
#include "season2024/types/chronospatial_computer.h"

namespace aoc::season2024 {

class Day17Part1
    : public aoc::solver::Solver<
          Day17Part1, aoc::season2024::ChronospatialComputer, std::string> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY17PART1_H__ */
