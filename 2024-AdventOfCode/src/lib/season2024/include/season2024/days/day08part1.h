#ifndef __DAY08PART1_H__
#define __DAY08PART1_H__

#include "aocsolver/solver.h"
#include "season2024/types/easter_bunny_transmitter.h"

namespace aoc::season2024 {

class Day08Part1
    : public aoc::solver::Solver<Day08Part1, EasterBunnyTransmitters, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY08PART1_H__ */
