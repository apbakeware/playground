#ifndef __DAY11PART1_H__
#define __DAY11PART1_H__

#include "aocinput/value_collection.h"
#include "aocsolver/solver.h"
#include "season2024/types/pluto_stones.h"

namespace aoc::season2024 {

class Day11Part1
    : public aoc::solver::Solver<
          Day11Part1,
          aoc::input::ValueCollection<aoc::season2024::types::StoneValue>,
          aoc::season2024::types::StoneValue> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY11PART1_H__ */
