#ifndef __DAY16PART1_H__
#define __DAY16PART1_H__

#include "aocsolver/solver.h"
#include "season2024/types/reindeer_maze.h"

namespace aoc::season2024 {

class Day16Part1
    : public aoc::solver::Solver<Day16Part1, aoc::season2024::ReindeerMaze,
                                 int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY16PART1_H__ */
