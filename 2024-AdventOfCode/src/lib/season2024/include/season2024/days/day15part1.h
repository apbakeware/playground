#ifndef __DAY15PART1_H__
#define __DAY15PART1_H__

#include "aocsolver/solver.h"
#include "season2024/types/lanternfish_warehouse.h"

namespace aoc::season2024 {

class Day15Part1
    : public aoc::solver::Solver<Day15Part1, LanternfishWarehouse, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY15PART1_H__ */
