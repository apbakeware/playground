#ifndef __DAY02PART1_H__
#define __DAY02PART1_H__

#include "aocinput/line_collection.h"
#include "aocsolver/solver.h"

namespace aoc::season2024 {

class Day02Part1
    : public aoc::solver::Solver<Day02Part1, aoc::input::LineCollection, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY02PART1_H__ */
