#ifndef __ONSEN_TOWELS_H__
#define __ONSEN_TOWELS_H__

#include <iosfwd>
#include <string>
#include <vector>

namespace aoc::season2024 {

struct OnsenTowels {
  std::vector<std::string> towels;
  std::vector<std::string> patterns;
};

bool isPatternPossible(const std::string &pattern,
                       const std::vector<std::string> &towels);

// Return the set of towles which can appear in the pattern
std::vector<std::string>
eliminateTowelsFromPattern(const std::string &pattern,
                           const std::vector<std::string> &towels);

unsigned long countPatternSolutions(const std::string &pattern,
                                    const std::vector<std::string> &towels);

void printOnsenTowelCacheStats();

std::istream &operator>>(std::istream &instr, OnsenTowels &obj);

} // namespace aoc::season2024

#endif /* __ONSEN_TOWELS_H__ */