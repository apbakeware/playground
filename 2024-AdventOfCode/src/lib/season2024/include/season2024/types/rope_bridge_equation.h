#ifndef __ROPE_BRIDGE_EQUATION_H__
#define __ROPE_BRIDGE_EQUATION_H__

#include <cstdint>
#include <iosfwd>
#include <string>
#include <vector>

#include "spdlog/fmt/ostr.h"

namespace aoc::season2024 {

struct RopeBridgeEquation {
  uint64_t answer;
  std::vector<uint64_t> operands;
};

// For use with GetLine Input Provider
struct RopeBridgeEquations {
  std::vector<RopeBridgeEquation> equations;

  void append(const std::string &line);
};

std::istream &operator>>(std::istream &instr, RopeBridgeEquation &obj);

} // namespace aoc::season2024

template <>
struct fmt::formatter<aoc::season2024::RopeBridgeEquation>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::RopeBridgeEquation instance
  template <typename FormatContext>
  auto format(const aoc::season2024::RopeBridgeEquation &obj,
              FormatContext &ctx) const -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    return fmt::format_to(
        ctx.out(), "RopeBridgeEquation(answer: {} = operands: [{}])",
        obj.answer, fmt::join(obj.operands.begin(), obj.operands.end(), " ? "));
  }
};

#endif /* __ROPE_BRIDGE_EQUATION_H__ */