#ifndef __RACETRACK_H__
#define __RACETRACK_H__

#include <iosfwd>
#include <map>
#include <vector>

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"

namespace aoc::season2024 {

struct Racetrack {
  using RacetrackGrid = grid::Fixed_Sized_Grid<char>;
  static constexpr char START = 'S';
  static constexpr char END = 'E';
  static constexpr char WALL = '#';

  void findPath();
  std::vector<int> findCheats(); // path must be found

  aoc::grid::Grid_Location start;
  aoc::grid::Grid_Location end;
  RacetrackGrid racetrack;

  std::vector<aoc::grid::Grid_Location> path;
  std::map<aoc::grid::Grid_Location, int>
      location_cost; // need hashable grid_location
};

std::istream &operator>>(std::istream &instr, Racetrack &obj);

} // namespace aoc::season2024

template <>
struct fmt::formatter<aoc::season2024::Racetrack>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::Racetrack instance
  template <typename FormatContext>
  auto format(const aoc::season2024::Racetrack &obj, FormatContext &ctx) const
      -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    return fmt::format_to(ctx.out(),
                          "Racetrack(\nStart: {}\nEnd: {}\nGrid:\n{})",
                          obj.start, obj.end, obj.racetrack);
  }
};

#endif /* __RACETRACK_H__ */