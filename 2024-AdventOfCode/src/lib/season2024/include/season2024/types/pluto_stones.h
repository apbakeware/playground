#ifndef __PLUTO_STONES_H__
#define __PLUTO_STONES_H__

#include <tuple>
#include <vector>

#include "aocinput/value_collection.h"

namespace aoc::season2024::types {

using StoneValue = uint64_t;
using StoneCollection = aoc::input::ValueCollection<StoneValue>;

using Blink = std::tuple<size_t, StoneValue, StoneValue>;

unsigned long doBlinks(const StoneCollection::ValuesType &values,
                       int blinks_to_do);

Blink blinkStoneValue(StoneValue value);

StoneValue doBlinksFast(const StoneCollection::ValuesType &values,
                        size_t blinks_to_do);

} // namespace aoc::season2024::types

#endif /* __PLUTO_STONES_H__ */