#ifndef __REINDEER_MAZE_H__
#define __REINDEER_MAZE_H__

#include <iosfwd>

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"

namespace aoc::season2024 {

struct ReindeerMaze {
  static constexpr char START = 'S';
  static constexpr char GOAL = 'E';
  static constexpr char WALL = '#';
  static constexpr char OPEN = '.';

  static constexpr int MOVE_COST = 1;
  static constexpr int ROTATE_COST = 1000;

  grid::Fixed_Sized_Grid<char> maze;
  grid::Grid_Location start;
  grid::Grid_Location end;

  bool isOpen(const aoc::grid::Grid_Location &location) const;
  bool isWall(const aoc::grid::Grid_Location &location) const;
  bool isEnd(const aoc::grid::Grid_Location &location) const;

  grid::Fixed_Sized_Grid<int> createCostGrid() const;

  void renderCostGridPath(const grid::Fixed_Sized_Grid<int> &cost_grid);

  void
  findAllLeastCostPaths(const grid::Fixed_Sized_Grid<int> &cost_grid) const;
};

std::istream &operator>>(std::istream &instr, ReindeerMaze &obj);

} // namespace aoc::season2024

template <>
struct fmt::formatter<aoc::season2024::ReindeerMaze>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::ReindeerMaze instance
  template <typename FormatContext>
  auto format(const aoc::season2024::ReindeerMaze &obj,
              FormatContext &ctx) const -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    return fmt::format_to(ctx.out(),
                          "ReindeerMaze(\nMaze:\n{:h}\nstart: {}  end: {})",
                          obj.maze, obj.start, obj.end);
  }
};

#endif /* __REINDEER_MAZE_H__ */