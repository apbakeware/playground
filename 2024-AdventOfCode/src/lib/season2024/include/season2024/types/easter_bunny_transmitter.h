#ifndef __EASTER_BUNNY_TRANSMITTER_H__
#define __EASTER_BUNNY_TRANSMITTER_H__

#include <map>
#include <set>
#include <vector>

#include "aoc/grid/grid_location.h"

namespace aoc::season2024 {

class EasterBunnyTransmitters {
public:
  using FrequencyType = char;
  using GridLocations = std::vector<aoc::grid::Grid_Location>;

  EasterBunnyTransmitters();

  int gridWidth() const { return m_num_grid_cols; }
  int gridHeight() const { return m_num_grid_rows; }

  bool isOnGrid(const aoc::grid::Grid_Location &location) const;

  void append(const std::string &line);
  void print() const;

  /**
   * Call op() for each transmitter. The parameters of
   * op are the frequency ID and the collection of
   * transmitter locations.
   */
  template <typename Operation_T> void forEachTransmitter(Operation_T op) {
    for (const auto &tranmitter : m_transmitters) {
      op(tranmitter.first, tranmitter.second);
    }
  }

private:
  using TransmitterGroups = std::map<FrequencyType, GridLocations>;
  int m_num_grid_rows;
  int m_num_grid_cols;
  TransmitterGroups m_transmitters;
};

void findAllAntinodes(EasterBunnyTransmitters::FrequencyType freq,
                      const EasterBunnyTransmitters::GridLocations &locations,
                      std::set<aoc::grid::Grid_Location> &unique_positions);

void findAllLinearAntinodes(
    EasterBunnyTransmitters::FrequencyType freq,
    const EasterBunnyTransmitters::GridLocations &locations, int grid_width,
    int grid_height, std::set<aoc::grid::Grid_Location> &unique_positions);

std::pair<aoc::grid::Grid_Location, aoc::grid::Grid_Location>
findAntiNodeLocations(const aoc::grid::Grid_Location &l1,
                      const aoc::grid::Grid_Location &l2);

std::vector<aoc::grid::Grid_Location>
findLinearAntiNodeLocations(const aoc::grid::Grid_Location &l1,
                            const aoc::grid::Grid_Location &l2, int grid_width,
                            int grid_height);

} // namespace aoc::season2024

#endif /* __EASTER_BUNNY_TRANSMITTER_H__ */