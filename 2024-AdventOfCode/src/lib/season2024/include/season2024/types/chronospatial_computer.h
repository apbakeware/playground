#ifndef __CHRONOSPATIAL_COMPUTER_H__
#define __CHRONOSPATIAL_COMPUTER_H__

#include <functional>
#include <iosfwd>
#include <vector>

#include "spdlog/fmt/ostr.h"

namespace aoc::season2024 {

class ChronospatialComputer {
public:
  using ValueType = unsigned int;

  using OutputHandler = std::function<void(ValueType)>;

  ChronospatialComputer();
  ChronospatialComputer(ValueType ra, ValueType rb, ValueType rc,
                        const std::vector<ValueType> &pgm);

  void reset(ValueType ra, ValueType rb, ValueType rc);
  bool executeCurrentInstruction(OutputHandler handler);
  void executeProgram(OutputHandler handler);

  bool runToSeeIfOutputMatchesProgram(ValueType ra, ValueType rb, ValueType rc);

  ValueType registerA() const { return m_reg_a; }
  ValueType registerB() const { return m_reg_b; }
  ValueType registerC() const { return m_reg_c; }

  friend std::istream &operator>>(std::istream &instr,
                                  ChronospatialComputer &obj);
  friend fmt::formatter<aoc::season2024::ChronospatialComputer>;

private:
  ValueType getLiteralOperandValue(ValueType operand) const;
  ValueType getComboOperandValue(ValueType operand) const;
  void executeOpcode(OutputHandler handler);

  ValueType m_reg_a;
  ValueType m_reg_b;
  ValueType m_reg_c;
  size_t m_instruction_ptr;
  std::vector<ValueType> m_program;
};

std::istream &operator>>(std::istream &instr, ChronospatialComputer &obj);

} // namespace aoc::season2024

template <>
struct fmt::formatter<aoc::season2024::ChronospatialComputer>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::ChronospatialComputer instance
  template <typename FormatContext>
  auto format(const aoc::season2024::ChronospatialComputer &obj,
              FormatContext &ctx) const -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    return fmt::format_to(
        ctx.out(),
        "ChronospatialComputer(regA: {} regB: {} regC: {}  iPtr: {}  Program: "
        "{})",
        obj.m_reg_a, obj.m_reg_b, obj.m_reg_c, obj.m_instruction_ptr,
        fmt::join(obj.m_program.begin(), obj.m_program.end(), " "));
  }
};

#endif /* __CHRONOSPATIAL_COMPUTER_H__ */