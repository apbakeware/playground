#include <catch2/catch_test_macros.hpp>

#include "season2024/types/chronospatial_computer.h"

namespace aoc::season2024::test {

TEST_CASE("Chronospatial Computer Tests") {

  SECTION("Test case 1") {
    ChronospatialComputer sut(0, 0, 9, {2, 6});
    std::vector<ChronospatialComputer::ValueType> output;

    auto output_builder = [&output](const auto value) -> void {
      output.push_back(value);
    };

    while (sut.executeCurrentInstruction(output_builder)) {
    }

    CHECK(sut.registerB() == 1);
  }

  SECTION("Test case 1") {
    ChronospatialComputer sut(10, 0, 0, {5, 0, 5, 1, 5, 4});

    std::vector<ChronospatialComputer::ValueType> output;

    auto output_builder = [&output](const auto value) -> void {
      output.push_back(value);
    };

    while (sut.executeCurrentInstruction(output_builder)) {
    }

    const std::vector<ChronospatialComputer::ValueType> expected = {0, 1, 2};
    CHECK(output == expected);
  }

  SECTION("Test case 3") {
    ChronospatialComputer sut(2024, 0, 0, {0, 1, 5, 4, 3, 0});
    std::vector<ChronospatialComputer::ValueType> output;

    auto output_builder = [&output](const auto value) -> void {
      output.push_back(value);
    };

    while (sut.executeCurrentInstruction(output_builder)) {
    }

    const std::vector<ChronospatialComputer::ValueType> expected = {
        4, 2, 5, 6, 7, 7, 7, 7, 3, 1, 0};
    CHECK(output == expected);
    CHECK(sut.registerA() == 0);
  }

  SECTION("Test case 4") {
    ChronospatialComputer sut(0, 29, 0, {1, 7});
    std::vector<ChronospatialComputer::ValueType> output;

    auto output_builder = [&output](const auto value) -> void {
      output.push_back(value);
    };

    while (sut.executeCurrentInstruction(output_builder)) {
    }

    CHECK(sut.registerB() == 26);
  }

  SECTION("Test case 5") {
    ChronospatialComputer sut(0, 2024, 43690, {4, 0});
    std::vector<ChronospatialComputer::ValueType> output;

    auto output_builder = [&output](const auto value) -> void {
      output.push_back(value);
    };

    while (sut.executeCurrentInstruction(output_builder)) {
    }

    CHECK(sut.registerB() == 44354);
  }

  SECTION("Test sample input") {
    ChronospatialComputer sut(729, 0, 0, {0, 1, 5, 4, 3, 0});
    std::vector<ChronospatialComputer::ValueType> output;

    auto output_builder = [&output](const auto value) -> void {
      output.push_back(value);
    };

    while (sut.executeCurrentInstruction(output_builder)) {
    }

    const std::vector<ChronospatialComputer::ValueType> expected = {
        4, 6, 3, 5, 6, 3, 5, 2, 1, 0};
    CHECK(output == expected);
  }
}

} // namespace aoc::season2024::test