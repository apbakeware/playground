LIBNAME=$1 # from input
TEST_PROJECT_NAME="lib${LIBNAME}-test"

if [ "x$LIBNAME" == "x" ] ; then
  echo "usage: ./$0 [libname]"
  exit 1
fi

ROOT=src/lib
LIBDIR=${ROOT}/${LIBNAME}

mkdir -p ${LIBDIR}/include/${LIBNAME}
mkdir -p ${LIBDIR}/src
mkdir -p ${LIBDIR}/test

#################################
# Library CMakefile
# 

cat << EOF > ${LIBDIR}/CMakeLists.txt 
project(${LIBNAME})

set(SOURCE_FILES 
  
)

add_library(\${PROJECT_NAME} \${SOURCE_FILES})

add_library(libs::\${PROJECT_NAME} ALIAS \${PROJECT_NAME})

target_include_directories(\${PROJECT_NAME}
	PUBLIC
		$<INSTALL_INTERFACE:include>
		$<BUILD_INTERFACE:\${CMAKE_CURRENT_SOURCE_DIR}/include>
	PRIVATE
    $<BUILD_INTERFACE:\${CMAKE_CURRENT_SOURCE_DIR}/include/${PROJECT_NAME}>
)


target_link_libraries(\${PROJECT_NAME} spdlog)

if(BUILD_TESTS)
	add_subdirectory(test)
endif()

EOF

#################################
# Library Test CMakefile
# 
cat << EOF > ${LIBDIR}/test/CMakeLists.txt 
project(${TEST_PROJECT_NAME})

set(TEST_SOURCE_FILES 
)

add_executable(\${PROJECT_NAME} \${TEST_SOURCE_FILES}) 
target_link_libraries(\${PROJECT_NAME}
  PRIVATE 
  spdlog::spdlog
  Catch2::Catch2WithMain
  ${LIBNAME}
)

add_test(NAME \${PROJECT_NAME} COMMAND \${PROJECT_NAME})
  
EOF