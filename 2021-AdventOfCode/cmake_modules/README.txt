Lots of modules from: https://github.com/bilke/cmake-modules/

Add to top level CMakeLists.txt
   list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules")



To enable a module:
   include(ModuleName)