#ifndef __DAY16_H__
#define __DAY16_H__

#include <string>
#include <vector>

struct Day16Data {
   typedef std::string Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day16Part1 : public Day16Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day16Part2 : public Day16Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day16Spec {
   using Data_Spec = Day16Data;
   using Part_1 = Day16Part1;
   using Part_2 = Day16Part2;
   static constexpr const char * day() { return "Day16"; }
   static constexpr const char * data_file_name() { return "data/day16.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day16.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "31"; }
   static constexpr const char * expected_sample_part_2_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * solved_part_1_result() { return "1007"; }
   static constexpr const char * solved_part_2_result() { return "undefined"; }
};

#endif // __DAY16_H__
