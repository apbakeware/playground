#include <algorithm>
#include <iostream>
#include <map>
#include <sstream>
#include "day05.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"
#include "solver_registry.h"
#include "grid.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day05Spec>();
}

const bool _was_registered = _do_registration();

std::vector<Grid_Location> create_points_on_line( const Line_Spec & line ) {
   
   SPDLOG_DEBUG("Creating line points for: {}", line);
   std::vector<Grid_Location> locations;
   Grid_Location point = line.point_a;
   const auto col_delta = line.point_b.col == line.point_a.col ? 0 : 
      line.point_b.col > line.point_a.col ? 1 : -1;
   const auto row_delta = line.point_b.row == line.point_a.row ? 0 : 
      line.point_b.row > line.point_a.row ? 1 : -1;

   SPDLOG_DEBUG("Deltas -- col {}  row {}", col_delta, row_delta);
   while( point != line.point_b ) {
      SPDLOG_DEBUG("Adding point: {}", point);
      locations.push_back( point );
      point.col += col_delta;
      point.row += row_delta;

   }

   SPDLOG_DEBUG("Adding point: {}", point);
   locations.push_back( point ); // The destination point on the line

   return locations;
}




}

std::istream & operator>>(std::istream & instr, Line_Spec & record ) {
   char sepc;
   std::string sep;
   instr >> record.point_a.col >> sepc >> record.point_a.row >> sep 
      >> record.point_b.col >> sepc >> record.point_b.row;
   return instr;
}

std::string Day05Part1::name() const {
   return "Day05-Part1";
}

std::string Day05Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   std::map<Grid_Location, int> location_count;

   for(auto line : data ) {
      
      SPDLOG_INFO("Read line spec {}", line);

      if( !(line.is_horizontal() || line.is_vertical()) ) {
         continue;
      }

      auto line_points = create_points_on_line( line );
      for( const auto& line_point: line_points) {
         if( location_count.count(line_point) ) {
            location_count[line_point]++;
            SPDLOG_DEBUG("Location {} already exists, incremented to: {}",
               line_point, location_count[line_point]);
         } else {
            location_count[line_point] = 1;
            SPDLOG_DEBUG("Location {} is new, count set to: {}",
               line_point, location_count[line_point]);
         }
         
      }
   }

   auto count = std::count_if(
      location_count.begin(),
      location_count.end(),
      []( const auto & node ) {
         return node.second > 1;
      }
   );
   return std::to_string(count);
}


std::string Day05Part2::name() const {
   return "Day05-Part2";
}

std::string Day05Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   std::map<Grid_Location, int> location_count;

   for(auto line : data ) {
      
      SPDLOG_INFO("Read line spec {}", line);

      auto line_points = create_points_on_line( line );
      for( const auto& line_point: line_points) {
         if( location_count.count(line_point) ) {
            location_count[line_point]++;
            SPDLOG_DEBUG("Location {} already exists, incremented to: {}",
               line_point, location_count[line_point]);
         } else {
            location_count[line_point] = 1;
            SPDLOG_DEBUG("Location {} is new, count set to: {}",
               line_point, location_count[line_point]);
         }
         
      }
   }

   auto count = std::count_if(
      location_count.begin(),
      location_count.end(),
      []( const auto & node ) {
         return node.second > 1;
      }
   );
   return std::to_string(count);
}

