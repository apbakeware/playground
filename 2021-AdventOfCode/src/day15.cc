#include <algorithm>
#include <limits>
#include <functional>
#include <iostream>
#include <vector>
#include "day15.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"
#include "solver_registry.h"
#include "grid.h"
#include "grid_utils.h"
#include <gperftools/profiler.h>

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day15Spec>();
}

const bool _was_registered = _do_registration();

// https://www.geeksforgeeks.org/a-search-algorithm/
// https://en.wikipedia.org/wiki/A*_search_algorithm
// g: the movement cost is the cost in each cell
// h: manhattan distance from the current location to the destination


struct Grid_Location_Hash
{
    std::size_t operator() (const Grid_Location & loc) const
    {
        return ((std::hash<ssize_t>()(loc.row) ^ (std::hash<ssize_t>()(loc.col) << 1)) >> 1);
    }
};

template<class Value_T>
bool value_less( const Value_T & first, const Value_T & second ) {
   return first.value < second.value;
}

struct Min_Heap_Comparitor {

   bool operator()( const Location_Value<int> & a, const Location_Value<int> & b ) const  {
      return a.value > b.value;
   }

};

size_t to_index(const Grid_Location & loc, ssize_t grid_num_cols ) {
   return loc.row * grid_num_cols + loc.col;
}


// TODO: need to profile...its really slow...than what I'd expect
// Is it the Grid operations?

struct Astar_Record {
   int g_score;
   int f_score;
   bool is_open; // in open set
   Grid_Location came_from;
};

int astar_search
( const Fixed_Sized_Grid<int> & grid, const Grid_Location & orig, const Grid_Location & dest ) {

   using Fscore_At = Location_Value<int>;
   
   const int number_of_cells = grid.number_of_rows() * grid.number_of_cols();
   const Astar_Record default_astar_record = {
      std::numeric_limits<int>::max(),
      std::numeric_limits<int>::max(),
      false,
      {-1, -1} 
   };
   auto loc_idx = std::bind (to_index, std::placeholders::_1 ,grid.number_of_cols());

   std::vector<Fscore_At> open_set_heap;
   std::vector<Astar_Record> astar_data( number_of_cells, default_astar_record );

   open_set_heap.reserve(number_of_cells / 3);
   const Fscore_At f_at_orig = {orig, 0};
   open_set_heap.push_back( f_at_orig );
   std::make_heap( open_set_heap.begin(), open_set_heap.end(), Min_Heap_Comparitor() );

   astar_data[loc_idx(orig)] = {
      0,
      manhattan_distance(orig, dest),
      true,
      {-1, -1}
   };

   while(!open_set_heap.empty()) {

      const auto current = open_set_heap.front();
      if( current.location == dest ) {
         return current.value;
      }

      std::pop_heap( open_set_heap.begin(), open_set_heap.end(), Min_Heap_Comparitor());
      open_set_heap.pop_back();
      astar_data[loc_idx(current.location)].is_open = false;
      for_each_orthogonal( grid, current.location, 
         [&](const auto & loc, const auto value ) {
            
            const auto working_score = astar_data[loc_idx(current.location)].g_score + value;
            const auto idx = loc_idx(loc);
            auto & loc_astar_data = astar_data[idx];

            if( working_score < loc_astar_data.g_score) {
               loc_astar_data.came_from = current.location;
               loc_astar_data.g_score = working_score;
               loc_astar_data.f_score = working_score + manhattan_distance(loc, dest);
               

               if( !loc_astar_data.is_open ) {
                  const Fscore_At new_open_set = {loc, working_score};
                  open_set_heap.push_back( new_open_set );
                  std::push_heap( open_set_heap.begin(), open_set_heap.end(), Min_Heap_Comparitor() );
                  loc_astar_data.is_open = true;
               }
            }
         }
      );
   }

   return -1;
}

Fixed_Sized_Grid<int> replicate_chiton_grid( const Fixed_Sized_Grid<int> grid ) {
   constexpr int GROWTH = 5;
   const auto SCAN_GRID_WIDTH = grid.number_of_cols();
   const auto SCAN_GRID_HEIGHT = grid.number_of_rows();

   Fixed_Sized_Grid<int> full_grid( 
      GROWTH * SCAN_GRID_HEIGHT, 
      GROWTH * SCAN_GRID_WIDTH
   );

   grid.for_each_cell(
      [&](const auto & loc, const auto value) {

         for( int row_cnt = 0; row_cnt < GROWTH; ++row_cnt) {

            auto full_grid_loc = Grid_Location::up(loc, (GROWTH - row_cnt - 1) * SCAN_GRID_HEIGHT );

            for( int col_cnt = 0; col_cnt < GROWTH; ++col_cnt ) {

               int offset = row_cnt + col_cnt;
               int value_to_set = value + offset;
               if( value_to_set > 9 ) {
                  value_to_set -= 9;
               }

               full_grid.set(full_grid_loc, value_to_set);
               full_grid_loc = Grid_Location::right(full_grid_loc, SCAN_GRID_WIDTH);
            }
         }
      }
   );

   return full_grid;
}

}

std::string Day15Part1::name() const {
   return "Day15-Part1";
}

std::string Day15Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   const auto & grid = create_digit_grid_from_string_rows<int>
      (data.begin(), data.end());

   const Grid_Location upper_left = {grid.number_of_rows() - 1, 0 };
   const Grid_Location lower_right = {0, grid.number_of_cols() - 1};
   int least_cost = astar_search( grid, upper_left, lower_right );
   return std::to_string(least_cost);
}


std::string Day15Part2::name() const {
   return "Day15-Part2";
}

std::string Day15Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   const auto & grid = create_digit_grid_from_string_rows<int>
      (data.begin(), data.end());

   const auto & full_grid = replicate_chiton_grid( grid );
   const Grid_Location upper_left = {full_grid.number_of_rows() - 1, 0 };
   const Grid_Location lower_right = {0, full_grid.number_of_cols() - 1};

   //ProfilerStart("./astar.profile");
   int least_cost = astar_search( full_grid, upper_left, lower_right );
   //ProfilerStop();
   return std::to_string(least_cost);
}

