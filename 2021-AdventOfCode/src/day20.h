#ifndef __DAY20_H__
#define __DAY20_H__

#include <string>
#include <vector>

struct Day20Data {
   // TODO: Set data type
   typedef int Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day20Part1 : public Day20Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day20Part2 : public Day20Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day20Spec {
   using Data_Spec = Day20Data;
   using Part_1 = Day20Part1;
   using Part_2 = Day20Part2;
   static constexpr const char * day() { return "Day20"; }
   static constexpr const char * data_file_name() { return "data/day20.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day20.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * expected_sample_part_2_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * solved_part_1_result() { return "undefined"; }
   static constexpr const char * solved_part_2_result() { return "undefined"; }
};

#endif // __DAY20_H__
