#ifndef __BINGO_CARD_H__
#define __BINGO_CARD_H__

#include <array>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <unordered_map>

#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"

#include "grid.h"



struct Bingo_Cell {
   int value;
   bool marked;
};

std::ostream &operator<<(std::ostream &ostr, const Bingo_Cell &obj);

class Bingo_Card {
public:
   using Card_Grid = Static_Sized_Grid<Bingo_Cell, 5, 5>;

   // Move Constructor
   explicit Bingo_Card(Card_Grid &&card); // populated card values

   // returns bingo status
   bool value_called(int number);

   bool has_bingo() const;

   // Iterator /// for each row // for each column // for each cell.
   template <class Op> void for_each_cell(Op op) {
      card.for_each_cell(op);
   }

   template <class Op> void for_each_cell(Op op) const {
      card.for_each_cell(op);
   }

   friend std::ostream &operator<<(std::ostream &ostr, const Bingo_Card &obj);

   template <typename OStream>
   friend OStream &operator<<(OStream &os, const Bingo_Card &rec) {
      return os << rec.card;
   }

private:
   // TODO: Inject in;

   void setup_location_cache();

   void check_for_bingo();

   // Stores where numbers are located on the board
   std::unordered_map<int, Grid_Location> location_cache;

   Card_Grid card;

   bool card_has_bingo;
};

std::ostream &operator<<(std::ostream &ostr, const Bingo_Card &obj);

#endif // __BINGO_CARD_H__