#include "table_writer_expected_actual_solver_result_processor.h"
#include "spdlog/fmt/fmt.h"

namespace {

const int LABEL_WIDTH = 20;
const int DURATION_WIDTH = 15;
const int SOLUTION_WIDTH = 20;
const int COMPARISON_WIDTH = 8;
const int FMT_WIDTH = LABEL_WIDTH + DURATION_WIDTH + SOLUTION_WIDTH + SOLUTION_WIDTH + COMPARISON_WIDTH + 6;

const std::string LABEL_COL_SEP = std::string(LABEL_WIDTH, '-');
const std::string DURATION_COL_SEP = std::string(DURATION_WIDTH, '-');
const std::string SOLUTION_COL_SEP = std::string(SOLUTION_WIDTH, '-');
const std::string CORRECT_COL_SEP = std::string(COMPARISON_WIDTH, '-');

const std::string TITLE_FMT = "  {:^{}}  ";
const std::string HDR_FMT = "| {:<{}} | {:>{}} | {:>{}} | {:>{}} | {:<8} |";
const std::string SEP_FMT = "+ {:^{}} + {:^{}} + {:^{}} + {:^{}} +";
const std::string FORMAT = "| {:<{}} | {:>{}} | {:>{}} | {:>{}} | {:<8} |";

std::string create_title(const std::string &title) {
   return fmt::format(TITLE_FMT, title, FMT_WIDTH);
}

std::string create_header_row(const std::string &col_1_label,
  const std::string &col_2_label,
  const std::string &col_3_label,
  const std::string &col_4_label,
  const std::string &col_5_label) {
   return fmt::format(HDR_FMT,
     col_1_label,
     LABEL_WIDTH,
     col_2_label,
     DURATION_WIDTH,
     col_3_label,
     SOLUTION_WIDTH,
     col_4_label,
     SOLUTION_WIDTH,
     col_5_label);
}

std::string create_row(const std::string &label,
  int duration,
  const std::string &expected,
  const std::string &actual) {
   return fmt::format(FORMAT,
     label,
     LABEL_WIDTH,
     duration,
     DURATION_WIDTH,
     expected,
     SOLUTION_WIDTH,
     actual,
     SOLUTION_WIDTH,
     (expected == actual)
    );
}

std::string create_row_sep() {
   return "+ " + 
    LABEL_COL_SEP + " + " + 
    DURATION_COL_SEP + " + " +
    SOLUTION_COL_SEP + " + " +
    SOLUTION_COL_SEP + " + " +
    CORRECT_COL_SEP + 
    " +";
}

} // namespace

Table_Writer_Expected_Actual_Solver_Result_Processor::
  Table_Writer_Expected_Actual_Solver_Result_Processor(
    const std::string &title, std::ostream &outstr)
  : outstr(outstr) {

    outstr 
      << create_title(title) << "\n"
      << create_row_sep() << "\n"
      << create_header_row("Problem", "Duration (ms)", "Expected", "Actual", "Correct") << "\n"
      << create_row_sep() << "\n";
}

void Table_Writer_Expected_Actual_Solver_Result_Processor::process(
  const Expected_Solver_Result &result) {

   const std::string &part_1 = create_row(result.problem_label + " Part 1",
     result.part_1_solve_duration.count(),
     std::string(result.part_1_expected_solution),
     std::string(result.part_1_actual_solution));

   const std::string &part_2 = create_row(result.problem_label + " Part 2",
     result.part_2_solve_duration.count(),
     std::string(result.part_2_expected_solution),
     std::string(result.part_2_actual_solution));

   outstr << part_1 << "\n" << part_2 << "\n" << create_row_sep() << "\n";
}
