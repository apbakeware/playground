#ifndef __DAY08_H__
#define __DAY08_H__

#include <array>
#include <string>
#include <vector>

struct Seven_Digit_Segment_Input_Record {
   std::array<std::string, 10> digits;
   std::array<std::string, 4> illuminated;
};

std::istream & operator>>(std::istream & instr, Seven_Digit_Segment_Input_Record & record );

struct Day08Data {
   typedef Seven_Digit_Segment_Input_Record Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day08Part1 : public Day08Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day08Part2 : public Day08Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day08Spec {
   using Data_Spec = Day08Data;
   using Part_1 = Day08Part1;
   using Part_2 = Day08Part2;
   static constexpr const char * day() { return "Day08"; }
   static constexpr const char * data_file_name() { return "data/day08.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day08.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "26"; }
   static constexpr const char * expected_sample_part_2_result() { return "61229"; }
   static constexpr const char * solved_part_1_result() { return "278"; }
   static constexpr const char * solved_part_2_result() { return "986179"; }
};

#endif // __DAY08_H__
