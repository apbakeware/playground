#ifndef __SOLVER_REGISTRY_H__
#define __SOLVER_REGISTRY_H__

#include <algorithm>
#include <functional>
#include <iostream>
#include <unordered_map>
#include <vector>

#include "solve.hpp"
#include "solver_result.h"
#include "i_result_processor.h"

// TODO: What I would like this to be eventually is templated with the function
// to call. The function is templated with the structure spec to run.

// Does not manage processors...just uses them.

// std::function...
template<class Solver_Result_T>
class Solver_Registry_Single {
public:

   using Result_Type = Solver_Result_T;
   using Solver_Function = std::function<Result_Type()>;
   using Result_Processor = IResult_Processor<Result_Type>;

   //template<class Solver_Spec>
   bool register_solver(const std::string & key, Solver_Function solver) {

      // Would be great to not have "day" as it can be more generic
      //const std::string key(Solver_Spec::day());
      
      if( solver_table.count( key ) ) {
         SPDLOG_ERROR("Cannot register solver {}, name already registered", key );
         return false;
      }

      solver_table[key] = solver;
      return true;
   }

   bool try_run_solver(const std::string & key) {
      const auto iter = solver_table.find(key);
      if( iter == solver_table.end() ) {
         return false;
      }
   
      const auto & result = iter -> second();
      std::for_each( 
         result_processors.begin(),
         result_processors.end(),
         [&](auto processor) {
            processor -> process( result );
         }
      );

      //std::cout << result << "\n";
      return true;
   }

   void add_result_processor( Result_Processor * processor ) {
      result_processors.push_back( processor );
   }

private:

   using Solver_Table = std::unordered_map<std::string, Solver_Function>;

   using Results_Processors = std::vector<Result_Processor*>;

   Solver_Table solver_table;
   Results_Processors result_processors;

};


class Solver_Registry {
public:

   template<class Solver_T>
   static bool register_solver() {

      SPDLOG_INFO("Registering solver: {}", Solver_T::day());

      auto & tables = _op_tables();
      const std::string day_key(Solver_T::day());

      auto solve_func = solve<Solver_T>;
      auto solve_sample_func = solve_and_evaluate_sample_cases<Solver_T>;
      auto solve_refactor_func = refactor_and_ensure_result<Solver_T>;

      if( !tables.solver_table.register_solver(day_key, solve_func) ) {
         SPDLOG_ERROR("Cannot register solver {}, name already registered", day_key );
         return false;
      } else if( !tables.sample_solver_table.register_solver(day_key, solve_sample_func) ) {
         SPDLOG_ERROR("Cannot register sample solver {}, name already registered", day_key );
         return false;
      }  else if( !tables.refactor_solver_table.register_solver(day_key, solve_refactor_func) ) {
         SPDLOG_ERROR("Cannot register refactor solver {}, name already registered", day_key );
         return false;
      }

      _solver_names().push_back( day_key );
      std::sort( _solver_names().begin(), _solver_names().end() );
      return true;
   }

   static void add_result_processor( IResult_Processor<Solver_Result> * processor ) {
      _op_tables().solver_table.add_result_processor( processor );
   }

   static void add_result_processor( IResult_Processor<Expected_Solver_Result> * processor ) {
      _op_tables().sample_solver_table.add_result_processor( processor );
      _op_tables().refactor_solver_table.add_result_processor( processor );
   }

   static std::vector<std::string> get_solver_names() {
      return _solver_names();
   }

   static bool try_run_solver(const std::string & key) {
      return _op_tables().solver_table.try_run_solver( key );
   }

   static bool try_run_sample_solver(const std::string & key) {
      return _op_tables().sample_solver_table.try_run_solver( key );
   }

   static bool try_run_refactor_solver(const std::string & key) {
      return _op_tables().refactor_solver_table.try_run_solver( key );
   }

private:

   using Checker_Op = std::function<Expected_Solver_Result()>;

   using Checker_Table = std::unordered_map<std::string, Checker_Op>;

   using Solver_Names = std::vector<std::string>;

   struct Op_Tables {
      Solver_Registry_Single<Solver_Result> solver_table;
      Solver_Registry_Single<Expected_Solver_Result> sample_solver_table;
      Solver_Registry_Single<Expected_Solver_Result> refactor_solver_table;
   };

   Solver_Registry() = delete;
   Solver_Registry(const Solver_Registry & ) = delete;
   Solver_Registry & operator=(const Solver_Registry &) = delete;

   static Op_Tables & _op_tables() {
      static Op_Tables op_tables;
      return op_tables;
   }

   static Solver_Names & _solver_names() {
      static Solver_Names solver_names;
      return solver_names;
   }

};

#endif // __SOLVER_REGISTRY_H__