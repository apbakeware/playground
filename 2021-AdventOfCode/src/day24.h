#ifndef __DAY24_H__
#define __DAY24_H__

#include <string>
#include <vector>

struct Day24Data {
   // TODO: Set data type
   typedef int Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day24Part1 : public Day24Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day24Part2 : public Day24Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day24Spec {
   using Data_Spec = Day24Data;
   using Part_1 = Day24Part1;
   using Part_2 = Day24Part2;
   static constexpr const char * day() { return "Day24"; }
   static constexpr const char * data_file_name() { return "data/day24.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day24.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * expected_sample_part_2_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * solved_part_1_result() { return "undefined"; }
   static constexpr const char * solved_part_2_result() { return "undefined"; }
};

#endif // __DAY24_H__
