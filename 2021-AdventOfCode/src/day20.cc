#include "day20.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day20Spec>();
}

const bool _was_registered = _do_registration();

}

std::string Day20Part1::name() const {
   return "Day20-Part1";
}

std::string Day20Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}


std::string Day20Part2::name() const {
   return "Day20-Part2";
}

std::string Day20Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}

