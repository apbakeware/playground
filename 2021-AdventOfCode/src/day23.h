#ifndef __DAY23_H__
#define __DAY23_H__

#include <string>
#include <vector>

struct Day23Data {
   // TODO: Set data type
   typedef int Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day23Part1 : public Day23Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day23Part2 : public Day23Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day23Spec {
   using Data_Spec = Day23Data;
   using Part_1 = Day23Part1;
   using Part_2 = Day23Part2;
   static constexpr const char * day() { return "Day23"; }
   static constexpr const char * data_file_name() { return "data/day23.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day23.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * expected_sample_part_2_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * solved_part_1_result() { return "undefined"; }
   static constexpr const char * solved_part_2_result() { return "undefined"; }
};

#endif // __DAY23_H__
