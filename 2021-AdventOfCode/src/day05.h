#ifndef __DAY05_H__
#define __DAY05_H__

#include <string>
#include <vector>
#include "grid.h"

// TODO: better locaiton for x,y...row column is mental jump
struct Line_Spec {
   Grid_Location point_a;
   Grid_Location point_b;

   bool is_vertical() const {
      return point_a.col == point_b.col;
   }

   bool is_horizontal() const {
      return point_a.row == point_b.row;
   }

   template<typename OStream>
    friend OStream &operator<<(OStream &os, const Line_Spec &rec)
    {
        return os << "[point_a: " << rec.point_a << ", point_b: " << rec.point_b << "]";
    }
};

std::istream & operator>>(std::istream & instr, Line_Spec & record );

struct Day05Data {
   typedef Line_Spec Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day05Part1 : public Day05Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day05Part2 : public Day05Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day05Spec {
   using Data_Spec = Day05Data;
   using Part_1 = Day05Part1;
   using Part_2 = Day05Part2;
   static constexpr const char * day() { return "Day05"; }
   static constexpr const char * data_file_name() { return "data/day05.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day05.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "5"; }
   static constexpr const char * expected_sample_part_2_result() { return "12"; }
   static constexpr const char * solved_part_1_result() { return "6225"; }
   static constexpr const char * solved_part_2_result() { return "22116"; }
};

#endif // __DAY05_H__
