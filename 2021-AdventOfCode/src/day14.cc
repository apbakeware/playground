#include <algorithm>
#include <limits>
#include <iterator>
#include "day14.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day14Spec>();
}

const bool _was_registered = _do_registration();

// modifies pylmer generator
// 

// Also tried inserting inline and increment iterator.
// There has to be a better way

void polymer_injection_phase( Polymer_Generator & polymer_generator ) {
      
   auto & poly_seq = polymer_generator.polymer_sequence;
   std::string inserts;

   inserts.reserve(poly_seq.size());

   auto iter = poly_seq.begin();
   const auto end = std::next(poly_seq.end(), -1);
   while(iter != end) {
      const std::string poly_pair( iter, std::next(iter, 2) );
      const auto pair_iter = polymer_generator.pair_insertions.find(poly_pair);

      if( pair_iter != polymer_generator.pair_insertions.end() ) {
         char inserted = (pair_iter -> second);
         inserts += inserted;
      } else {
         std::cout << "Could not find: " << poly_pair << "\n";
      }
      ++iter;
   }

   std::string new_sequence;
   new_sequence.reserve( 1.5 * poly_seq.size() );
   for(auto idx = 0u; idx < inserts.size(); ++idx) {
      new_sequence += poly_seq[idx];
      new_sequence += inserts[idx];
   }
   new_sequence += poly_seq.back();

   poly_seq = std::move(new_sequence);
}

std::string generate_polymer(int phases, Polymer_Generator && polymer_generator ) {
   
   while(phases > 0) {
      --phases;
      polymer_injection_phase( polymer_generator );
   }

   return polymer_generator.polymer_sequence;
}

unsigned long compute_min_max_count_difference( const std::string & str ) {
   
   std::unordered_map<char, unsigned long> counts_by_type;
   unsigned long max_count = 0;
   unsigned long min_count = std::numeric_limits<unsigned long>::max();
   std::for_each( 
      str.begin(),
      str.end(),
      [&]( const auto & val ) {
         counts_by_type[val] += 1;
      }
   );

   std::for_each( 
      counts_by_type.begin(), 
      counts_by_type.end(),
      [&](const auto & val) {
         max_count = std::max(max_count, val.second);
         min_count = std::min(min_count, val.second);
      }
   );

   return max_count - min_count;
}

}


std::istream& operator>>
(std::istream & instr, Polymer_Generator & record ) {

   std::string index;
   std::string junk;
   char insertion;

   instr >> record.polymer_sequence;
   while(instr) {
      instr >> index >> junk >> insertion;
      record.pair_insertions[index] = insertion;
   }

   return instr;
}



std::string Day14Part1::name() const {
   return "Day14-Part1";
}

std::string Day14Part1::solve( const Input_Data_Collection & data ) {
   
   auto polymerization_generator = data;
   const auto num_steps = 10;

   auto polymer = generate_polymer( num_steps, std::move(polymerization_generator) );
   
   return std::to_string(compute_min_max_count_difference(polymer));
}


std::string Day14Part2::name() const {
   return "Day14-Part2";
}

std::string Day14Part2::solve( const Input_Data_Collection & data ) {
   auto polymerization_generator = data;
   const auto num_steps = 40;

   return "CANNOT SOLVE WITH CURRENT SOLUTION. NEED FREQUENCY MAPPINGS WITHOUT CREATING THE STRING";
   auto polymer = generate_polymer( num_steps, std::move(polymerization_generator) );
   
   return std::to_string(compute_min_max_count_difference(polymer));
}

