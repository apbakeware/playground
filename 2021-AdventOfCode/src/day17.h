#ifndef __DAY17_H__
#define __DAY17_H__

#include <iostream>
#include <string>
#include <vector>

struct Target_Bounds {
   int x_min;
   int x_max;
   int y_min;
   int y_max;
};

template<class Ostream>
Ostream & operator<<(Ostream & ostr, const Target_Bounds & bounds) {
   ostr << "[Target bounds: X:<" << bounds.x_min << ", " << bounds.x_max << ">   Y<" << bounds.y_min << ", " << bounds.y_max << ">]";
   return ostr;
}

std::istream & operator>>
(std::istream & instr, Target_Bounds & bounds );


struct Day17Data {
   typedef Target_Bounds Input_Data_Type;
   typedef Input_Data_Type Input_Data_Collection;  // Load a structure not a collection 
   // TODO: setup the data so its not trying to load a collection all the time
};

class Day17Part1 : public Day17Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day17Part2 : public Day17Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day17Spec {
   using Data_Spec = Day17Data;
   using Part_1 = Day17Part1;
   using Part_2 = Day17Part2;
   static constexpr const char * day() { return "Day17"; }
   static constexpr const char * data_file_name() { return "data/day17.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day17.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "45"; }
   static constexpr const char * expected_sample_part_2_result() { return "112"; }
   static constexpr const char * solved_part_1_result() { return "17766"; }
   static constexpr const char * solved_part_2_result() { return "1733"; }
};

#endif // __DAY17_H__
