#include <algorithm>
#include <sstream>
#include "day04.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"




namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day04Spec>();
}

const bool _was_registered = _do_registration();

template<class T>
class QueueAdapter {
public:

   using value_type = T;

    QueueAdapter(std::queue<T>& q) : _q(q) {}
    void push_back(const T& t) { _q.push(t); }

private:
    std::queue<T>& _q;
};


struct Bingo_Setup {
   std::queue<int> numbers_to_call;
   std::vector<Bingo_Card> bingo_cards;
};


// TODO: nicer "no winner"
struct Bingo_Winners {

   std::vector<Bingo_Card *> winning_cards;
   int called_number;

   operator bool() const {
      return !winning_cards.empty();
   }
};


int compute_winning_value( const Bingo_Card & winning_card, int winning_draw ) {

   int sum_of_unmarked = 0;

   auto op = [&]( const auto & location, const auto & cell ) {
      if( !cell.marked ) {
         sum_of_unmarked += cell.value;
      }
   };

   winning_card.for_each_cell( op );
   return sum_of_unmarked * winning_draw;
}


Day04Data::Input_Data_Collection::const_iterator extract_numbers_to_call(
  Day04Data::Input_Data_Collection::const_iterator input,
  std::queue<int> & out_numbers_to_call ) {

   std::string sequence = *input;
   std::replace( sequence.begin(), sequence.end(), ',', ' ');
   std::istringstream instr(sequence);
   QueueAdapter<int> q_wrapper( out_numbers_to_call );

   SPDLOG_INFO("Numbers to call: {}. Extracting them now.", sequence);
   std::copy(std::istream_iterator<int>(instr),
      std::istream_iterator<int>(),
      std::back_inserter(q_wrapper) 
   );

   return ++input;
}


Day04Data::Input_Data_Collection::const_iterator 
extract_bingo_board(
  Day04Data::Input_Data_Collection::const_iterator input,
  std::vector<Bingo_Card> & out_bingo_cards ) {

   Bingo_Card::Card_Grid grid;

   // Data is read top to bottom so insert correctly
   // in the grid
   for( int row = (int)grid.number_of_rows() - 1; row >= 0; --row ) {
      for( int col = 0; col < (int)grid.number_of_cols(); ++col ) {
         const std::string row_input = *input;
         const int in_int = stoi( row_input );
         ++input;
         grid.set( {(unsigned int)row, (unsigned int)col}, { in_int, false} );
      }
   }

   SPDLOG_DEBUG("Loaded bingo card\n{}", grid);
   out_bingo_cards.emplace_back( std::move(grid) );

   SPDLOG_INFO("Completed building bingo card");
   return input;
}


Bingo_Setup create_bingo_setup
( const Day04Data::Input_Data_Collection & input ) {

   Bingo_Setup bingo_setup;
   auto data_iter = extract_numbers_to_call( input.begin(), bingo_setup.numbers_to_call );
   while( data_iter != input.end() ) {
      data_iter = extract_bingo_board( data_iter, bingo_setup.bingo_cards );
   }

   return bingo_setup;
}

// Returns a structure with the iterator to the wining board and the drawn number.
Bingo_Winners play_bingo( 
   std::vector<Bingo_Card>::iterator bingo_cards_iter_start, 
   std::vector<Bingo_Card>::iterator bingo_cards_iter_end, 
   std::queue<int> & numbers_to_call ) {

   Bingo_Winners winners;
   do {

      if( numbers_to_call.empty() ) {
         return winners;
      }

      winners.called_number = numbers_to_call.front();
      numbers_to_call.pop();

      std::for_each(
         bingo_cards_iter_start,
         bingo_cards_iter_end,
         [&](auto & card) {
            if( !card.has_bingo() ) {
               if( card.value_called( winners.called_number ) ) {
                  winners.winning_cards.push_back( &card );
               }
            }
         }
      );

   } while(!winners);

   return winners;
}

}


std::string Day04Part1::name() const { return "Day04-Part1"; }

std::string Day04Part1::solve(const Input_Data_Collection &data) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   auto bingo_setup = create_bingo_setup( data );
   numbers_to_call = bingo_setup.numbers_to_call;
   bingo_cards = bingo_setup.bingo_cards;
   
   SPDLOG_INFO("Count of numbers to call {}", numbers_to_call.size());
   SPDLOG_INFO("Number of bingo cards: {}", bingo_cards.size());

   const auto & winner = play_bingo( bingo_cards.begin(), bingo_cards.end(), numbers_to_call );

   if( winner.winning_cards.size() != 1 ) {
      std::cout << "expected single winner" << std::endl;
   }

   int result = compute_winning_value( *(winner.winning_cards.front()), winner.called_number );

   return std::to_string(result);
}











std::string Day04Part2::name() const { return "Day04-Part2"; }

std::string Day04Part2::solve(const Input_Data_Collection &data) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   auto bingo_setup = create_bingo_setup( data );
   numbers_to_call = bingo_setup.numbers_to_call;
   bingo_cards = bingo_setup.bingo_cards;
   
   SPDLOG_INFO("Count of numbers to call {}", numbers_to_call.size());
   SPDLOG_INFO("Number of bingo cards: {}", bingo_cards.size());

   int result = 0;
   while( !numbers_to_call.empty() && !bingo_cards.empty()) {
      const auto & winner = play_bingo( bingo_cards.begin(), bingo_cards.end(), numbers_to_call );
      if(winner) {

         // TODO: check for more than 1
         result = compute_winning_value( *(winner.winning_cards.front()), winner.called_number );
      }      
   }

   return std::to_string(result);
}
