#include "pch.h"
#include <sstream>
#include <string>
#include <vector>
#include "gtest/gtest.h"
#include "../Solutions/Rectangle.h"
#include "../Solutions/ExpandableWorkspaceCutCalculator.h"

TEST(ExpandableWorkspaceCutCalculatorTest, Day03Data){

   std::vector<std::string> inputs = {
      "#1 @ 1,3: 4x4",
      "#2 @ 3,1: 4x4",
      "#3 @ 5,5: 2x2"
   };

   types::ExpandableWorkspaceCutCalculator sut;

   for(auto const& input : inputs){
      std::istringstream str(input);
      types::Rectangle rect;
      str >> rect;
      std::cout << "Parsed: " << rect << "\n";
      sut.cutout(rect);
   }

   std::cout << "Cut Space\n" << sut << "\n";

   // uncomment to render in the Test Explorer
   // FAIL();
}