#include "pch.h"
#include "gtest/gtest.h"

#include <ctime>
#include <iomanip>
#include <sstream>
#include <string>
#include "../Solutions/Timestamped.h"

TEST(TimestampedTest, StreamExtractionOperator) {
   const std::string input = "[1518-11-01 07:13] This is content";
   std::istringstream istr(input);

   utils::Timestamped sut;

   istr >> sut;
   ASSERT_EQ(1518, sut.year()) << "Year parsing failure";
   ASSERT_EQ(11, sut.month()) << "Month parsing failure";
   ASSERT_EQ(1, sut.day()) << "Day parsing failure";
   ASSERT_EQ(7, sut.hour()) << "Hour parsing failure";
   ASSERT_EQ(13, sut.minute()) << "Minute parsing failure";
   ASSERT_EQ("This is content", sut.content()) << "Content parsing failure";
}

// TODO: Validation | error in stream processing.
