#include "pch.h"

#include "../Solutions/Rectangle.h"
#include "../Solutions/ExpandableWorkspaceCutCalculator.h"

TEST(CutoutArea, Day3SampleDataTest){

   auto const r1 = types::Rectangle::from_origin_and_size(1, 3, 4, 4);
   auto const r2 = types::Rectangle::from_origin_and_size(3, 1, 4, 4);
   auto const r3 = types::Rectangle::from_origin_and_size(5, 5, 2, 2);

   types::ExpandableWorkspaceCutCalculator sut;

   sut.cutout(r1);
   sut.cutout(r2);
   sut.cutout(r3);

   EXPECT_EQ(4, sut.get_cutout_overlap_area());
}