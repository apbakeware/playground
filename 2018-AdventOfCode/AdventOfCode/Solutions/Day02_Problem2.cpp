#include "pch.h"
#include <iterator>
#include "Day02_Problem2.h"
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "Utils.h"

namespace {
ProblemFactoryRegisterer<Day02_Problem2> _problem("Day02_Problem2");
}

Day02_Problem2::~Day02_Problem2() {

}

std::string Day02_Problem2::name() const {
   return "Day02 - Problem 2";
}

ISolution* Day02_Problem2::execute(std::istream& data_set) {
   auto input_data = load_data <std::string>(data_set);
   std::cout << "Loaded data elements: " << input_data.size() << std::endl;

   auto const END = input_data.end();
   auto iter = input_data.begin();

   std::string intersection;
   while (iter != END) {
      auto test_iter = iter;
      while (++test_iter != END ) {
         auto const str1 = *iter;
         auto const str2 = *test_iter;
         intersection.clear();
         auto differint_chars = utils::linear_intersection(
            str1.begin(),
            str1.end(),
            str2.begin(),
            std::back_inserter(intersection)
         );

         auto const size_diff = iter->size() - intersection.size();
         if( size_diff == 1) {
            return new SingleValueSolution<std::string>(intersection);
         }
      }
      ++iter;
   }
   
   // Should not get here
   return new SingleValueSolution<std::string>("");
}