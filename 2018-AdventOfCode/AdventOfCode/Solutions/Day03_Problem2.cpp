#include "pch.h"
#include "Day03_Problem2.h"
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "Rectangle.h"
#include "TrackedType.h"

namespace{
ProblemFactoryRegisterer<Day03_Problem2> _problem("Day03_Problem2");
}

Day03_Problem2::~Day03_Problem2(){

}

std::string Day03_Problem2::name() const{
   return "Day03 - Problem 2";
}

ISolution* Day03_Problem2::execute(std::istream& data_set){
   typedef types::TrackedType<types::Rectangle> Tracked_Rectangle;
   auto input_data = load_data <Tracked_Rectangle>(data_set);

   std::cout << "Loaded data elements: " << input_data.size() << std::endl;
   auto const begin = input_data.begin();
   auto const end = input_data.end();

   for(auto oiter = begin; oiter != end; ++oiter){

      const auto& r1 = oiter -> get_obj();
      bool has_overlap = false;

      for(auto iiter = begin; iiter != end; ++iiter){
         if(iiter == oiter){
            continue;
         }

         has_overlap = types::intersects(r1, iiter->get_obj());
         if(has_overlap){
            break;
         }
      }

      if(!has_overlap){
         return new SingleValueSolution<int>(oiter->get_id());
      }
   }

   return new SingleValueSolution<int>(-1);
}
