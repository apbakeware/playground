#pragma once

#include <iosfwd>
#include <vector>

namespace types{

class Rectangle;

/**
 * This class maintains a rectagular workspace from which cutouts can
 * be taken. The workspace grows to accomodate the origin and size
 * of the cutouts.
 *
 * If cutouts overlap with other cutouts, the overlapping area is
 * tracked.
 * 
 */
class ExpandableWorkspaceCutCalculator {
public:

   ExpandableWorkspaceCutCalculator();

   bool workspace_encompasses_cut(const Rectangle& cutout) const;

   // Cutout the shape. Track if the cutout overlaps existing cutouts (return false)
   void cutout(const Rectangle& shape);

   int get_cutout_overlap_area() const;

   friend std::ostream& operator<<(std::ostream& str, 
      const ExpandableWorkspaceCutCalculator& obj);

private:

   

   // Enlarge an maintain rectagnle sizes.
   void enlarge_workspace_to_encompass(const Rectangle& cutout);

   // Will dyamically grow based on cutout shapes.
   // value is the number of cuts requesting the position.
   // Work space units in terms of x, y

   // Use direct indexing (ie 1=1 so 0 row and 0 column are unused)
   int currently_supported_x;
   int currently_supported_y;

   // each element represents a 1 unit square
   std::vector<std::vector<char> > workspace;

};

}