#pragma once
#include "ISolution.h"


class Day02_Problem2 : public IProblem {
public:
   virtual ~Day02_Problem2();

   virtual std::string name() const;

   virtual ISolution* execute(std::istream& data_set) override;
};

