#include "pch.h"
#include <algorithm>
#include <iterator>
#include "Day04_Problem2.h"
#include "GuardShift.h"
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "Timestamped.h"

namespace {
ProblemFactoryRegisterer<Day04_Problem2> _problem("Day04_Problem2");

struct CountMinute {
   int guard_id;
   int minute;
   int count;
};

struct GuardSleepCountByMinute {
   int guard_id;
   std::vector<int> sleep_count;
};

CountMinute get_most_slept_minute(const GuardSleepCountByMinute& record) {
   auto max = std::max_element(record.sleep_count.begin(), record.sleep_count.end());
   CountMinute result = {
      record.guard_id,
      std::distance(record.sleep_count.begin(), max),
      *max
   };
   return result;
}

bool sort_by_ascending_count(const CountMinute& lhs, const CountMinute& rhs) {
   return lhs.count < rhs.count;
}

std::ostream& operator<<(std::ostream& str, const CountMinute& obj) {
   str << "{ GuardID: " << obj.guard_id << " most slept minute: " << obj.minute
      << "  count: " << obj.count << "}";
   return str;
}

std::ostream& operator<<(std::ostream& str, const GuardSleepCountByMinute& obj) {
   str << "{ GuardID: " << obj.guard_id << " Count By Minute: ";
   for(auto cnt : obj.sleep_count) {
      str << cnt << " ";
   }
   str << "}";
   return str;
}

template<typename Iter_T>
GuardSleepCountByMinute create_from_guards_records(Iter_T begin, Iter_T end) {
   GuardSleepCountByMinute result = {begin->get_guard_id(), std::vector<int>(60, 0)};

   while(begin != end) {
      for(size_t idx = 0; idx < 60; ++idx) {
         if(begin->is_asleep_at(idx)) {
            ++result.sleep_count[idx];
         }
      }
      ++begin;
   }

   return result;
}

}

Day04_Problem2::~Day04_Problem2() {
}

std::string Day04_Problem2::name() const {
   return "Day04 - Problem 2";
}

ISolution* Day04_Problem2::execute(std::istream& data_set) {

   auto input_data = load_data<utils::Timestamped>(data_set);
   std::sort(input_data.begin(), input_data.end());

   auto guard_shift_records = types::create_guard_sleep_schedule
   (input_data.begin(), input_data.end());

   std::vector<GuardSleepCountByMinute> guards_sleep_by_minute;

   std::sort(
      guard_shift_records.begin(),
      guard_shift_records.end(),
      [](const auto& lhs, const auto& rhs) {
         return lhs.get_guard_id() < rhs.get_guard_id();
      });

   auto start_iter = guard_shift_records.begin();
   while(start_iter != guard_shift_records.end()) {
      auto end_iter = start_iter;
      while(end_iter != guard_shift_records.end()) {
         if(start_iter->get_guard_id() == end_iter->get_guard_id()) {
            ++end_iter;
         } else {
            break;
         }
      }

      auto rec = create_from_guards_records(start_iter, end_iter);
      guards_sleep_by_minute.push_back(rec);
      start_iter = end_iter;
   }

   std::vector<CountMinute> max_minute_by_guard;
   std::transform(
      guards_sleep_by_minute.begin(),
      guards_sleep_by_minute.end(),
      std::back_inserter(max_minute_by_guard),
      get_most_slept_minute
   );

   std::sort(
      max_minute_by_guard.begin(),
      max_minute_by_guard.end(),
      sort_by_ascending_count
   );

   const auto& guard_record = max_minute_by_guard.back();
   
   return new SingleValueSolution<int>(guard_record.guard_id * guard_record.minute);
}
