#pragma once

#include <iterator>
#include <iostream>
#include <vector>

template<class Loaded_T>
std::vector<Loaded_T> load_data(std::istream& instream) {
   return std::vector<Loaded_T>(
      std::istream_iterator<Loaded_T>(instream),
      std::istream_iterator<Loaded_T>()
   );
}