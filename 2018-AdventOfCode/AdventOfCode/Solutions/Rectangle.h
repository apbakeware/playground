#pragma once

#include <iosfwd>

namespace types {

// origin is upper left. no negative values
class Rectangle {
public:

   static Rectangle from_origin_and_size(int origin_x, int origin_y, int width, int height);

   Rectangle();
   Rectangle(int x1, int y1, int x2, int y2);

   int x1() const{ return m_x1; }
   int y1() const{ return m_y1; }
   int x2() const{ return m_x2; }
   int y2() const{ return m_y2; }
   int width() const{ return m_x2 - m_x1; }
   int height() const{ return m_y2 - m_y1; }

   int area() const;

   friend std::ostream& operator<<(std::ostream& str, const types::Rectangle& obj);

   // Parses the input line as illustrated from the example // ID is ignored
   // #1 @ 1,3: 4x4
   friend std::istream& operator>>(std::istream& str, types::Rectangle& obj);

   friend bool operator==(const Rectangle& lhs, const Rectangle& rhs);

   // Challenge is to exclude those that are covered by multiple areas

private:

   int m_x1;
   int m_y1;
   int m_x2;
   int m_y2;
};

Rectangle intersection(const Rectangle& r1, const Rectangle& r2);

bool intersects(const Rectangle& r1, const Rectangle& r2);

}

