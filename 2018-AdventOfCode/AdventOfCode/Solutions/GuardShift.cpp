#include "pch.h"
#include <iomanip>
#include "GuardShift.h"


namespace types {

GuardSleepSpan::GuardSleepSpan
(MinuteBits& sleep_bits, int minute_fell_asleep)
   : sleep_bits(sleep_bits),
   minute_fell_asleep(minute_fell_asleep),
   minute_wakes_up(60) {

}

GuardSleepSpan::~GuardSleepSpan() {
   for(int idx = get_minute_fell_asleep();
      idx < get_minute_wakes_up();
      ++idx) {
      sleep_bits.set(idx);
   }
}

void GuardSleepSpan::set_minute_wakes_up(int minute) {
   minute_wakes_up = minute;
}




GuardShiftRecord::GuardShiftRecord(int guard_id)
   : guard_id(guard_id) {

}

GuardSleepSpan* GuardShiftRecord::create_sleep_record
(int minute_fell_asleep) {
   return new GuardSleepSpan(sleep_minutes, minute_fell_asleep);
}

int GuardShiftRecord::get_minutes_asleep() const {
   return sleep_minutes.count();
}

std::ostream& operator<<(std::ostream& str, const GuardShiftRecord& record) {
   str << "{Guard: " << std::setw(4) << record.guard_id
      << ": " << record.sleep_minutes.to_string()
      << "  Sleep Minutes: " << std::setw(2) << record.sleep_minutes.count()
      << "}";
   return str;
}



}