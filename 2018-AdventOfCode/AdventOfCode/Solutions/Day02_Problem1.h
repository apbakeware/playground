#pragma once
#include <string>
#include "ISolution.h"

class Day02_Problem1 : public IProblem {
public:
   virtual ~Day02_Problem1();

   virtual std::string name() const;

   virtual ISolution* execute(std::istream& data_set) override;
};

