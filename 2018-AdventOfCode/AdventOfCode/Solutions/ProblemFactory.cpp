#include "pch.h"
#include "ProblemFactory.h"
#include <iostream>

//https://stackoverflow.com/questions/39336778/how-to-force-include-static-objects-from-a-static-library-in-c-msvc-11
// needs /WHOLEARCHIVE:Solutions to force all objcts for autoreg to load

ProblemFactory& ProblemFactory::factory() {
   static ProblemFactory instance;
   return instance;
}

ProblemFactory::ProblemFactory() {

}

bool ProblemFactory::register_builder
(const std::string& name, Concrete_Builder builder) {
   std::cout << "Factory registering: " << name << "\n";
   auto result = builders.insert
      (std::make_pair(name, builder));
   
   return result.second;
}

IProblem* ProblemFactory::build(const std::string& name) {
   auto builder = builders.find(name);
   return builder == builders.end() ?
      nullptr : (builder->second)();
}

std::vector<std::string> ProblemFactory::all_keys() const {
   std::vector<std::string> result;
   for (auto const builder : builders) {
      result.push_back(builder.first);
   }
   return result;
}