#include "pch.h"
#include <iostream>
#include <numeric>
#include "Day01_Problem1.h"
#include "DataLoader.h"
#include "SingleValueSolution.h"
#include "ProblemFactory.h"

namespace {
ProblemFactoryRegisterer<Day01_Problem1> _problem("Day01_Problem1");

}

Day01_Problem1::~Day01_Problem1() {

}

std::string Day01_Problem1::name() const {
   return "Day01 - Problem 1";
}

ISolution* Day01_Problem1::execute(std::istream& data_set) {
   auto input_data = load_data<int>(data_set);
   std::cout << "Loaded data elements: " << input_data.size() << std::endl;
   int frequency = std::accumulate(input_data.begin(), input_data.end(), 0);
   return new SingleValueSolution<int>(frequency);
}