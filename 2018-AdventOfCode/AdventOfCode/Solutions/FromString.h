#pragma once

#include <string>
#include <sstream>

namespace utils{

template<class T>
T from_string(const std::string& str){
   T val;
   std::istringstream s(str);
   s >> val;
   return val;
}

}