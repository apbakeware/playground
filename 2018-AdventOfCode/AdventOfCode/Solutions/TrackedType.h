#pragma once

#include <iostream>
#include <regex>
#include <string>
#include "FromString.h"

namespace types{

typedef int ID;

static const ID UNKNOWN_ID = -1;

template<class T>
class TrackedType {
public:

   typedef T Tracked;

   TrackedType() : id(UNKNOWN_ID) {

   }

   TrackedType(ID id, const T& obj)
      : id(id), obj(obj){

   }

   ID get_id() const{
      return id;
   }

   Tracked get_obj() const{
      return obj;
   }

   Tracked& get_obj(){
      return obj;
   }

   
   friend std::istream& operator>>(std::istream& str, TrackedType<T>& obj){
      const std::regex capture("#(\\d+)*");
      std::string line;
      std::getline(str, line);
      std::smatch matches;

      obj.id = UNKNOWN_ID;
      if(std::regex_search(line, matches, capture)){
        obj.id = utils::from_string<int>(matches[1]);
         std::istringstream wrapped(line);
         wrapped >> obj.obj;
      }

      return str;
   }

   friend std::ostream& operator<<(std::ostream& str, TrackedType<T>& obj){
      str << "{ ID: " << obj.id << "  obj: " << obj.obj << "}";
      return str;
   }

private:

   ID id;
   Tracked obj;
};

}