#include "pch.h"
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include "Timestamped.h"
#include "FromString.h"

namespace utils {

Timestamped::Timestamped()
   : m_year(0), m_month(0), m_day(0), m_hour(0), m_minute(0) {

}

int Timestamped::year() const {
   return m_year;
}

int Timestamped::month() const {
   return m_month;
}

int Timestamped::day() const {
   return m_day;
}

int Timestamped::hour() const {
   return m_hour;
}

int Timestamped::minute() const {
   return m_minute;
}

std::string Timestamped::content() const {
   return m_content;
}

std::ostream& operator<<(std::ostream& str, const Timestamped& obj) {
   str << obj.m_year << "-" << obj.m_month << "-"
      << obj.m_day << " " << obj.m_hour << ":" << obj.m_minute
      << " '" << obj.m_content << "'";
   return str;
}

// TODO:
// Not sure if the getline is best approach, but its simple and easy.
// Need to learn how to handle failures and put data back in stream 
// to support chaining to further on a line rather than 'content'.
std::istream& operator>>(std::istream& str, Timestamped& obj) {
   const std::regex capture("(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2}). (.*)");
   std::smatch matches;
   std::string line;
   std::getline(str, line);
   if(std::regex_search(line, matches, capture)) {
      obj.m_year = utils::from_string<int>(matches[1]);
      obj.m_month = utils::from_string<int>(matches[2]);
      obj.m_day = utils::from_string<int>(matches[3]);
      obj.m_hour = utils::from_string<int>(matches[4]);
      obj.m_minute = utils::from_string<int>(matches[5]);
      obj.m_content = matches[6];
   }
   return str;
}

bool operator<(const Timestamped& lhs, const Timestamped& rhs) {

   if(&lhs == &rhs) return false;

   if(lhs.year() < rhs.year()) return true;
   if(lhs.year() > rhs.year()) return false;

   if(lhs.month() < rhs.month()) return true;
   if(lhs.month() > rhs.month()) return false;

   if(lhs.day() < rhs.day()) return true;
   if(lhs.day() > rhs.day()) return false;

   if(lhs.hour() < rhs.hour()) return true;
   if(lhs.hour() > rhs.hour()) return false;

   return lhs.minute() < rhs.minute();
}

}