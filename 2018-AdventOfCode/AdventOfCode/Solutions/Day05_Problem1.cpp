#include "pch.h"
#include "Day05_Problem1.h"
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "Utils.h"

namespace {
ProblemFactoryRegisterer<Day05_Problem1> _problem("Day05_Problem1");
}

Day05_Problem1::~Day05_Problem1() {
}

std::string Day05_Problem1::name() const {
   return "Day05 - Problem 1";
}

ISolution* Day05_Problem1::execute(std::istream& data_set) {
   std::string input_data;
   data_set >> input_data;
   
   auto processed = utils::remove_dups_with_opposite_case(input_data);

   return new SingleValueSolution<int>(processed.size());
}
