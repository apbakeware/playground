﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Serilog;

using aoc2020.Problems;


namespace aoc2020.Problems.Tests
{
    public class BinarySpacePartitionTest
    {
        public BinarySpacePartitionTest(ITestOutputHelper output)
        {
            Log.Logger = new Serilog.LoggerConfiguration()
                .WriteTo.TestOutput(output)
                .CreateLogger();
        }

        [Theory]
        [InlineData("BFFFBBF", 70)]
        [InlineData("FFFBBBF", 14)]
        [InlineData("BBFFBBF", 102)]
        public void TestBSP128FB(string path, int expected)
        {
            int value = BinarySpacePartion.ComputePath(path.GetEnumerator(), 0, 128, 'F', 'B');
            Assert.Equal(expected, value);
        }

        [Theory]
        [InlineData("RRR", 7)]
        [InlineData("RLL", 4)]
        public void TestBSP8RL(string path, int expected)
        {
            int value = BinarySpacePartion.ComputePath(path.GetEnumerator(), 0, 8, 'L', 'R');
            Assert.Equal(expected, value);
        }
    }

}
