﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Serilog;
using aoc2020.Problems;

namespace aoc2020.Problems.Tests
{
    public class Day14Test
    {
        public Day14Test(ITestOutputHelper output)
        {
            Log.Logger = new Serilog.LoggerConfiguration()
                .WriteTo.TestOutput(output)
                .CreateLogger();
        }

        [Fact]
        public void TestMaskToAddrStrings_ExampleCase1()
        {
            int addr = 42;
            string mask = "000000000000000000000000000000X1001X";
            string[] expected =
            {
                "000000000000000000000000000000011010",
                "000000000000000000000000000000011011",
                "000000000000000000000000000000111010",
                "000000000000000000000000000000111011"
            };

            var actual = Day14.CreateAddrsFromMask(mask, addr);
            
            Array.Sort(actual);
            Array.Sort(expected);
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void TestMaskToAddrStrings_ExampleCase2()
        {
            int addr = 26;
            string mask = "00000000000000000000000000000000X0XX";
            string[] expected =
            {
                "000000000000000000000000000000010000",
                "000000000000000000000000000000010001",
                "000000000000000000000000000000010010",
                "000000000000000000000000000000010011",
                "000000000000000000000000000000011000",
                "000000000000000000000000000000011001",
                "000000000000000000000000000000011010",
                "000000000000000000000000000000011011"
            };

            var actual = Day14.CreateAddrsFromMask(mask, addr);

            Array.Sort(actual);
            Array.Sort(expected);
            Assert.Equal(actual, expected);
        }
    }
}
