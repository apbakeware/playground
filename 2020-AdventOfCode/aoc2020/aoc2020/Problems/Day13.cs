﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;
using aoc2020.Utils;

namespace aoc2020.Problems
{

    public class Day13 : DayBase
    {

        private ILogger logger;
        private long departureStamp;
        private string[] busIdsStr;
        private List<long> busIds;

        public Day13() : base("Day13")
        {
            logger = Log.ForContext<Day13>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            departureStamp = long.Parse(inputStream.ReadLine());
            busIdsStr = inputStream.ReadLine().Split(",");
            busIds = new List<long>();
            for (long idx = 0; idx < busIdsStr.Length; idx++)
            {
                if(busIdsStr[idx] != "x")
                { 
                    busIds.Add(long.Parse(busIdsStr[idx]));
                } 
            }

            return 1 + busIds.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            long timeTilDeparture = long.MaxValue;
            long busToTake = -1;
            foreach(long busId in busIds)
            {
                long timeTilNextDeparture = busId - (departureStamp % busId);
                logger.Information("BusId: {0}  tilNext {1}", busId, timeTilNextDeparture);
                if(timeTilNextDeparture < timeTilDeparture)
                {
                    busToTake = busId;
                    timeTilDeparture = timeTilNextDeparture;
                }
            }

            logger.Information("Best is bus {0} leaving in {1}", busToTake, timeTilDeparture);

            return new ToStringSolution<long>(NamePart1, busToTake * timeTilDeparture );
        }


        protected override ISolution SolvePart2Impl()
        {
            List<long> minAfter = new List<long>();

            for(long idx = 0; idx < busIdsStr.Length; idx++ )
            {
                if (busIdsStr[idx] != "x")
                {
                    minAfter.Add(idx);
                }
            }

            long crt = ChineseRemainderTheorem.Compute(minAfter.ToArray(), busIds.ToArray());

            return new ToStringSolution<long>(NamePart2, crt + 1);
        }

    }

}
