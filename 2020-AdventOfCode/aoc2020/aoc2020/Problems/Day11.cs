﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{

    public class Day11 : DayBase
    {

        private ILogger logger;
        private int numCols;
        private Dictionary<(int row, int col), char> seattingChart;
        private List<(int row, int col)> keys;
        readonly char LIMIT = '*';
        readonly char FLOOR = '.';
        readonly char OCCUPIED = '#';
        readonly char UNOCCUPIED = 'L';

        readonly (int row, int col)[] offsets = { (-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1), };

        public Day11() : base("Day11")
        {
            logger = Log.ForContext<Day11>();
            seattingChart = new Dictionary<(int row, int col), char>();
            keys = new List<(int row, int col)>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            (int row, int col) key = (0, -1);
            while ((line = inputStream.ReadLine()) != null)
            {
                numCols = line.Length;
                seattingChart.Add(key, LIMIT);
                foreach (char c in line)
                {
                    key.col++;
                    seattingChart.Add(key, c);
                    keys.Add(key);
                    
                }
                key.col++;
                seattingChart.Add(key, LIMIT);
                key = (key.row + 1, -1);

            }

            for (int idx = -1; idx <= numCols; idx++)
            {
                var bottomLimitKey = (-1, idx);
                key.col = idx;
                //logger.Information("Addding keys: {0}  {1}", bottomLimitKey.ToString(), key.ToString());
                seattingChart.Add(bottomLimitKey, LIMIT);
                seattingChart.Add(key, LIMIT);
            }

            return seattingChart.Count;
        }

        // Process the whol block in 1 loop...dont account for changine each step
        protected override ISolution SolvePart1Impl()
        {

            //return new ToStringSolution<int>(NamePart1, -1);

            int prevValue = -1;
            int currentValue = 0;
            //logger.Information("S T A R T");
            //PrintSeatingChart();

            Dictionary<(int row, int col), char> originalChart = new Dictionary<(int row, int col), char>(seattingChart);
            while (currentValue != prevValue)
            {
                prevValue = currentValue;
                currentValue = 0;

                Dictionary<(int row, int col), char> nextSeattingChart = new Dictionary<(int row, int col), char>(seattingChart);

                //for (int row = numRows - 1; row >= 0; row--) { logger.Information("{row}", seatRows[row]); }
                foreach (var key in keys)
                {
                    var seatStatus = AnalyzeAdjacentSeats(key.row, key.col);
                    var seat = seattingChart[key];
                    if (seat == UNOCCUPIED && seatStatus.occupiedSeat == 0)
                    {
                        //logger.Information("Occupying seat {row} {col}", key.row, key.col);
                        nextSeattingChart[key] = OCCUPIED;
                    }
                    else if (seat == OCCUPIED && seatStatus.occupiedSeat >= 4)
                    {
                        //logger.Information("Vacating seat {row} {col}", key.row, key.col);
                        nextSeattingChart[key] = UNOCCUPIED;
                    }
                    currentValue += nextSeattingChart[key] == OCCUPIED ? 1 : 0;
                }
                //logger.Information("Nubmer of occupied seats: {count}", currentValue);

                seattingChart = nextSeattingChart;
                //logger.Information("I T E R A T I O N");
                //PrintSeatingChart();
            }

            seattingChart = originalChart;
            return new ToStringSolution<int>(NamePart1, currentValue);
        }

        protected override ISolution SolvePart2Impl()
        {
            // MUST REPARSE CUZ THE DATA SET IS DESTRUCITVE IN EACH OPERATION
            //return new ToStringSolution<int>(NamePart1, -1);

            int prevValue = -1;
            int currentValue = 0;
            //logger.Information("S T A R T");
            //PrintSeatingChart();
            while (currentValue != prevValue)
            {
                prevValue = currentValue;
                currentValue = 0;

                Dictionary<(int row, int col), char> nextSeattingChart = new Dictionary<(int row, int col), char>(seattingChart);

                //for (int row = numRows - 1; row >= 0; row--) { logger.Information("{row}", seatRows[row]); }
                foreach (var key in keys)
                {
                    var seatStatus = AnalyzeVisibleSeats(key.row, key.col);
                    var seat = seattingChart[key];
                    if (seat == UNOCCUPIED && seatStatus.occupiedSeat == 0)
                    {
                        //logger.Information("Occupying seat {row} {col}", key.row, key.col);
                        nextSeattingChart[key] = OCCUPIED;
                    }
                    else if (seat == OCCUPIED && seatStatus.occupiedSeat >= 5)
                    {
                        //logger.Information("Vacating seat {row} {col}", key.row, key.col);
                        nextSeattingChart[key] = UNOCCUPIED;
                    }
                    currentValue += nextSeattingChart[key] == OCCUPIED ? 1 : 0;
                }
                //logger.Information("Nubmer of occupied seats: {count}", currentValue);

                seattingChart = nextSeattingChart;
                //logger.Information("I T E R A T I O N");
                //PrintSeatingChart();
            }

            return new ToStringSolution<int>(NamePart2, currentValue);
        }

        private (int unoccupiedSeat, int occupiedSeat) AnalyzeAdjacentSeats(int row, int col)
        {
            (int unoccupiedSeat, int occupiedSeat) result = (0, 0);

            foreach (var offset in offsets)
            {
                var key = (row + offset.row, col + offset.col);
                // Better than exceptions
                try
                {
                    char seat = seattingChart[key];
                    if (seat == UNOCCUPIED) { result.unoccupiedSeat++; }
                    else if (seat == OCCUPIED) { result.occupiedSeat++; }
                }
                catch (Exception)
                {
                    var i = 0;
                }

            }
            return result;
        }

        private (int unoccupiedSeat, int occupiedSeat) AnalyzeVisibleSeats(int row, int col)
        {
            (int unoccupiedSeat, int occupiedSeat) result = (0, 0);

            foreach (var offset in offsets)
            {
                var key = (row, col);
                char seat = FLOOR;
                do
                {
                    key = (key.row + offset.row, key.col + offset.col);
                    seat = seattingChart[key];
                } while (seat == FLOOR);
                
                if (seat == UNOCCUPIED) { result.unoccupiedSeat++; }
                else if (seat == OCCUPIED) { result.occupiedSeat++; }
            }
            return result;
        }


        private void PrintSeatingChart()
        {
            foreach (var key in keys)
            {
                Console.Write(seattingChart[key]);
                if (key.col == numCols - 1)
                {
                    Console.WriteLine();
                }
            }
        }
    }
}
