﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{
    

    public class Passport
    {
        
        static private readonly string[] REQUIRED_FIELDS =
        {
            "byr",
            "iyr",
            "eyr",
            "hgt",
            "hcl",
            "ecl",
            "pid"
        };

        static private readonly Regex hgtValidator = new Regex(@"^(?<hgt>\d+)(?<unit>(cm|in))$");
        static private readonly Regex hclValidator = new Regex(@"^#[0-9a-f]{6}$");
        static private readonly Regex pidValidator = new Regex(@"^\d{9}$");

        static IEnumerable<string> EyeColors()
        {
            yield return "amb";
            yield return "blu";
            yield return "brn";
            yield return "gry";
            yield return "grn";
            yield return "hzl";
            yield return "oth";
        }

        public Dictionary<string, string> Fields { get; set; }

        public Passport()
        {
            Fields = new Dictionary<string, string>();
        }

        public bool IsSimpleValidated()
        {
            foreach(string reqField in REQUIRED_FIELDS)
            {
                if (!Fields.ContainsKey(reqField))
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsComplexValidated()
        {
            return IsSimpleValidated()
                && ValidateNumberRange(int.Parse(Fields["byr"]), 1920, 2002) && Fields["byr"].Count() == 4
                && ValidateNumberRange(int.Parse(Fields["iyr"]), 2010, 2020) && Fields["iyr"].Count() == 4
                && ValidateNumberRange(int.Parse(Fields["eyr"]), 2020, 2030) && Fields["eyr"].Count() == 4
                && ValidateHgt()
                && IsSatisfied(hclValidator, "hcl")
                && EyeColors().Contains(Fields["ecl"])
                && IsSatisfied(pidValidator, "pid");
        }

        private bool ValidateNumberRange(int val, int min, int max)
        {
            bool result = max >= val && val >= min;
            //Console.WriteLine($"{max} >= {val} >= {min}  ==> {result}");
            return result;
        }

        private bool ValidateHgt()
        {
            bool result = false;
            Match validator = hgtValidator.Match(Fields["hgt"]);
            if(validator.Success)
            {
                int hgt = int.Parse(validator.Groups["hgt"].Value);
                if (validator.Groups["unit"].Value == "cm")
                {
                    result = ValidateNumberRange(hgt, 150, 193);
                }
                else if(validator.Groups["unit"].Value == "in")
                {
                    result = ValidateNumberRange(hgt, 59, 76);
                }
            }

            //Console.WriteLine($"Validate hgt: {result}");
            return result;
        }

        private bool IsSatisfied(Regex regex, string key)
        {
            bool result = regex.IsMatch(Fields[key]);
            //Console.WriteLine($"Satisifed:  {key} {Fields[key]}  --  {result}");
            return result;
        }
    }



    public class Day04 : DayBase
    {
        public List<Passport> Records { get; }
        static private readonly Regex keyValueRe = new Regex(@"(?<key>\S+):(?<val>\S+)");

        private ILogger logger;
        
        public Day04() : base("Day04")
        {
            logger = Log.ForContext<Day04>();
            Records = new List<Passport>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            Passport passport = new Passport();
            while ((line = inputStream.ReadLine()) != null)
            {
                if(line == "")
                {
                    Records.Add(passport);
                    passport = new Passport();
                }
                else
                {
                    foreach(string keyVal in line.Split(" "))
                    {
                        Match regMatch = keyValueRe.Match(keyVal);
                        if (regMatch.Success)
                        {
                            passport.Fields.Add(regMatch.Groups["key"].Value, regMatch.Groups["val"].Value);
                        }
                    }
                }
                 
            }
            Records.Add(passport);
            return Records.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            int result = Records.Where(record => record.IsSimpleValidated()).Count();
            return new ToStringSolution<int>(NamePart1, result);
        }

        protected override ISolution SolvePart2Impl()
        {
            int result = Records.Where(record => record.IsComplexValidated()).Count();
            return new ToStringSolution<int>(NamePart2, result);
        }

    }
}
