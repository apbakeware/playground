﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{

    public class Day09 : DayBase
    {

        private ILogger logger;
        private List<long> values;

        public Day09() : base("Day09")
        {
            logger = Log.ForContext<Day09>();
            values = new List<long>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            while ((line = inputStream.ReadLine()) != null)
            {
                values.Add(long.Parse(line));
            }
            return values.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            return new ToStringSolution<long>(NamePart1, FindInvalidValue());
        }

        protected override ISolution SolvePart2Impl()
        {
            long targetValue = FindInvalidValue();

            // 00:00:00.0000732
            //var indicies = FindContinuousSumIndicies(targetValue);
            //var valueRange = values.GetRange(indicies.MinIndex, indicies.MaxIndex - indicies.MinIndex);
            //return new ToStringSolution<long>(NamePart2, valueRange.Min() + valueRange.Max()) ;

            //  00:00:00.0000673
            var minMax = FindContinuousRunMinMax(targetValue);
            return new ToStringSolution<long>(NamePart2, minMax.Min + minMax.Max);
        }

        private long FindInvalidValue()
        {
            const int PREAMBLE_LENGTH = 25;
            HashSet<long> testSamples = new HashSet<long>();

            long result = 0;
            int index = 0;
            bool found = false;

            for (index = 0; index < PREAMBLE_LENGTH; index++)
            {
                testSamples.Add(values[index]);
            }

            do
            {
                found = false;
                result = values[index];
                foreach (long test in testSamples)
                {
                    long needed = result - test;
                    if (needed != test && testSamples.Contains(needed))
                    {
                        found = true;
                        break;
                    }
                }

                testSamples.Remove(values[index - PREAMBLE_LENGTH]);
                testSamples.Add(values[index]);
                ++index;
            } while (index < values.Count && found);

            return result;
        }

        private (bool IsValid, int MinIndex, int MaxIndex) FindContinuousSumIndicies(long targetSum)
        {
            for (int index = 0; index < values.Count; index++)
            {
                long sum = values[index];
                int runIndex = index + 1;
                do
                {
                    sum += values[runIndex];
                    if( sum == targetSum )
                    {
                        //Log.Information("Found min index: {0}  max index: {1}", index, runIndex);
                        return (true, index, runIndex);
                    }

                    runIndex++;
                }
                while (sum < targetSum);
            }
            return (false, -1, -1);
        }

        private (bool IsValid, long Min, long Max) FindContinuousRunMinMax(long targetSum)
        {
            long max = 0;
            long min = Int64.MaxValue;

            for (int index = 0; index < values.Count; index++)
            {
                long sum = values[index];
                max = values[index];
                min = values[index];

                int runIndex = index + 1;
                do
                {
                    min = Math.Max(min, values[runIndex]);
                    max = Math.Max(max, values[runIndex]);
                    sum += values[runIndex];
                    if (sum == targetSum)
                    {
                        return (true, min, max);
                    }

                    runIndex++;
                }
                while (sum < targetSum);
            }
            return (false, -1, -1);
        }
    }
}
