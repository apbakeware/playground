﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;
using aoc2020.Utils;

namespace aoc2020.Problems
{

    public class Day16 : DayBase
    {

        private ILogger logger;
        HashSet<long> allValidValues;
        private Dictionary<string, HashSet<long>> constraints;
        private long[] myTicketValse;
        private List<long[]> otherTicketVals;
        readonly Regex constrainRegex = new Regex(@"^(?<key>.+): (?<low1>\d+)-(?<high1>\d+) or (?<low2>\d+)-(?<high2>\d+)$");


        public Day16() : base("Day16")
        {
            logger = Log.ForContext<Day16>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            Action<string> lineParser = ParseConstralongLine;
            allValidValues = new HashSet<long>();
            constraints = new Dictionary<string, HashSet<long>>();
            otherTicketVals = new List<long[]>();
            string line;
            int count = 0;
            while ((line = inputStream.ReadLine()) != null)
            {
                ++count;
                if (line.Length == 0) continue;

                if (line == "your ticket:")
                {
                    lineParser = ParseMyTicketLine;
                }
                else if (line == "nearby tickets:")
                {
                    lineParser = ParseOtherTicketLine;
                }
                else
                {
                    lineParser(line);
                }
            }
            return count;
        }

        protected override ISolution SolvePart1Impl()
        {
            long sum = 0;
            foreach(var otherTickets in otherTicketVals)
            {
                foreach(var val in otherTickets)
                {
                    sum += allValidValues.Contains(val) ? 0 : val;
                }
            }
                      
            return new ToStringSolution<long>(NamePart1, sum);
        }

        protected override ISolution SolvePart2Impl()
        {
            List<long[]> validTickets = new List<long[]>
            {
                myTicketValse
            };

            foreach (var otherTickets in otherTicketVals)
            {
                if (IsValidTicket(otherTickets))
                {
                    validTickets.Add(otherTickets);
                }
            }

            var determinationTable = CreateDeterminationTable(validTickets);
            var colForConstraint = ReduceDeterminationTable(determinationTable);

            long product = colForConstraint
                .Where(v => v.Key.StartsWith("departure"))
                .Select(v => myTicketValse[v.Value])
                .Aggregate(1L, (a, b) => a * b);

            return new ToStringSolution<long>(NamePart2, product);
        }

        private static Dictionary<string, long> ReduceDeterminationTable(Dictionary<string, HashSet<long>> determination)
        {
            Dictionary<string, long> colForConstraint = new Dictionary<string, long>();
            string constraintName = "";
            long colNum = 0;
            while (determination.Count > 0)
            {
                foreach (var record in determination)
                {
                    if (record.Value.Count == 1)
                    {
                        constraintName = record.Key;
                        colNum = record.Value.ToArray<long>()[0];
                        break;
                    }
                }

                colForConstraint[constraintName] = colNum;
                determination.Remove(constraintName);
                foreach (var allowabls in determination.Values)
                {
                    allowabls.Remove(colNum);
                }
            }

            return colForConstraint;
        }

        private Dictionary<string, HashSet<long>> CreateDeterminationTable(List<long[]> validTickets)
        {
            Dictionary<string, HashSet<long>> determination = new Dictionary<string, HashSet<long>>();
            foreach (var constraint in constraints)
            {
                HashSet<long> col = IdentifyPossibleColumns(constraint.Value, validTickets);
                determination.Add(constraint.Key, col);
            }

            return determination;
        }

        private void ParseConstralongLine(string line)
        {
            Match match = constrainRegex.Match(line);
            if(!match.Success)
            {
                throw new Exception("Failed to parse line");
            }

            HashSet<long> vals = new HashSet<long>();

            long min = long.Parse(match.Groups["low1"].Value);
            long max = long.Parse(match.Groups["high1"].Value);
            for(long val = min; val <= max; val++)
            {
                vals.Add(val);
                allValidValues.Add(val);
            }

            min = long.Parse(match.Groups["low2"].Value);
            max = long.Parse(match.Groups["high2"].Value);
            for (long val = min; val <= max; val++)
            {
                vals.Add(val);
                allValidValues.Add(val);
            }

            constraints.Add(match.Groups["key"].Value, vals);
        }

        private void ParseMyTicketLine(string line)
        {
            myTicketValse = line.Split(",").Select(s => long.Parse(s)).ToArray();
        }

        private void ParseOtherTicketLine(string line)
        {
            otherTicketVals.Add(line.Split(",").Select(s => long.Parse(s)).ToArray());
        }

        private bool IsValidTicket(long[] vals)
        {
            foreach (var val in vals)
            {
                if(!allValidValues.Contains(val))
                {
                    return false;
                }
            }

            return true;
        }

        private HashSet<long> IdentifyPossibleColumns(HashSet<long> allowableValues, List<long[]> tickts)
        {
            long numCols = (long)tickts[0].Length;
            HashSet<long> possibleColumns = new HashSet<long>();

            for(long col = 0; col < numCols; col++)
            {
                bool found = true;
                foreach(var ticket in tickts)
                {
                    var testVal = ticket[col];
                    if (!allowableValues.Contains(testVal))
                    {
                        found = false;
                        break;
                    }
                }

                if(found)
                {
                    possibleColumns.Add(col);
                }
            }

            return possibleColumns;
        }
    }
}
