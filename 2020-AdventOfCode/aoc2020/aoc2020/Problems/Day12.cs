﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{

    public class Day12 : DayBase
    {
        private ILogger logger;
        readonly private Regex commandRegex = new Regex(@"^(?<cmd>\w)(?<dist>\d+)$");
        private List<(string Cmd, int Dist)> commandList;

        public Day12() : base("Day12")
        {
            logger = Log.ForContext<Day12>();
            commandList = new List<(string Cmd, int Dist)>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            while ((line = inputStream.ReadLine()) != null)
            {
                Match match = commandRegex.Match(line);
                if(match.Success)
                {
                    var cmd = (match.Groups["cmd"].Value, int.Parse(match.Groups["dist"].Value));
                    commandList.Add(cmd);
                }
            }

            return commandList.Count;
        }

        // Process the whol block in 1 loop...dont account for changine each step
        protected override ISolution SolvePart1Impl()
        {
            (int X, int Y) start = (0,0);
            (int X, int Y) position = (0, 0);
            int facing = 0;

            foreach (var cmd in commandList)
            {
                switch(cmd.Cmd)
                {
                    case "N":
                        position.Y += cmd.Dist;
                        break;
                    case "E":
                        position.X += cmd.Dist;
                        break;
                    case "S":
                        position.Y -= cmd.Dist;
                        break;
                    case "W":
                        position.X -= cmd.Dist;
                        break;
                    case "L":
                        facing = (((facing + cmd.Dist ) % 360) + 360) % 360;
                        //logger.Information("Facing: {0}", facing);
                        break;
                    case "R":
                        facing = (((facing - cmd.Dist) % 360) + 360) % 360;
                        //logger.Information("Facing: {0}", facing);
                        break;
                    case "F":
                        var vector = GetVector(facing, cmd.Dist);
                        position.X += vector.X;
                        position.Y += vector.Y;
                        break;
                }
            }
            int result = Math.Abs(position.X) + Math.Abs(position.Y);
            //logger.Information("X: {0}  Y: {1}", Math.Abs(position.X), Math.Abs(position.Y));
            return new ToStringSolution<int>(NamePart1, result);
        }

        protected override ISolution SolvePart2Impl()
        {
            (int X, int Y) position = (0, 0);
            (int X, int Y) waypoint = (10, 1);

            foreach (var cmd in commandList)
            {
                switch (cmd.Cmd)
                {
                    case "N":
                        waypoint.Y += cmd.Dist;
                        break;
                    case "E":
                        waypoint.X += cmd.Dist;
                        break;
                    case "S":
                        waypoint.Y -= cmd.Dist;
                        break;
                    case "W":
                        waypoint.X -= cmd.Dist;
                        break;
                    case "L":
                        waypoint = RotateWaypointLeft(waypoint, cmd.Dist);
                        break;
                    case "R":
                        waypoint = RotateWaypointRight(waypoint, cmd.Dist);
                        break;
                    case "F":
                        position.X += waypoint.X * cmd.Dist;
                        position.Y += waypoint.Y * cmd.Dist;
                        break;
                }

                //logger.Information("Cmd: {2}  Waypoint: {0}    Position: {1}", waypoint, position, cmd);
            }
            int result = Math.Abs(position.X) + Math.Abs(position.Y);
            //logger.Information("X: {0}  Y: {1}", Math.Abs(waypoint.X), Math.Abs(waypoint.Y));


            return new ToStringSolution<int>(NamePart2, result);
        }

        public static (int X, int Y) RotateWaypointLeft((int X, int Y) waypoint, int dist)
        {
            if (dist == 90) { waypoint = (-1 * waypoint.Y, waypoint.X); }
            else if (dist == 180) { waypoint = (-1* waypoint.X, -1 * waypoint.Y); }
            else if (dist == 270) { waypoint = (waypoint.Y, -1 * waypoint.X); }
            else { throw new Exception("failure"); }

            return waypoint;
        }

        public static (int X, int Y) RotateWaypointRight((int X, int Y) waypoint, int dist)
        {
            if (dist == 90) { waypoint = (waypoint.Y, -1 * waypoint.X); }
            else if (dist == 180) { waypoint = (-1 * waypoint.X, -1 * waypoint.Y); }
            else if (dist == 270) { waypoint = (-1 * waypoint.Y, waypoint.X); }
            else { throw new Exception("failure"); }

            return waypoint;
        }

        (int X, int Y) GetVector(int facing, int distance)
        {
            if (facing == 0) { return (distance, 0); }
            if (facing == 90) { return (0, distance); }
            if (facing == 180) { return (-1 * distance, 0); }
            return (0, -1 * distance); // assumes 270
        }
    }
}
