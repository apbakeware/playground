#include <algorithm>
#include <numeric>
#include <string>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"


namespace {

class Day1Prob1 : public IProblem {
public:

   virtual ~Day1Prob1();

   virtual std::string name() const override;

   // Solution object?
   virtual ISolution* execute(std::istream& data_set) override;
};


ProblemFactoryRegisterer<Day1Prob1> _problem("Day1Prob1");


Day1Prob1::~Day1Prob1() {

}

std::string Day1Prob1::name() const {
   return "Day 01 - Problem 1";
}

ISolution* Day1Prob1::execute(std::istream& data_set) {
   const auto input_data = load_data<int>(data_set);
   std::vector<int> fuel_required(input_data.size(), 0);

   std::transform(
      input_data.begin(),
      input_data.end(),
      fuel_required.begin(),
      [](const auto mass) {
         return (mass / 3) - 2;
      }
   );
   
   auto sum = std::accumulate(fuel_required.begin(), fuel_required.end(), 0);

   return new SingleValueSolution<int>(sum);
}

}
