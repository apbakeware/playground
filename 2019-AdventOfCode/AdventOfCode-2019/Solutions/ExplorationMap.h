#pragma once

#include <cassert>
#include <vector>
#include "Cartesian.h"

typedef std::vector<cartesian::Point2> DirectionList;

const DirectionList QUAD_ORIDINAL_DIRECTIONS = {{1,0}, {0,1}, {-1,0}, {0,-1}};

template<class DiscoveryType, DiscoveryType InitialValue>
class QuadDirectionExlorationRecord {
public:

   QuadDirectionExlorationRecord() :
      is_explored(false),
      discovery(InitialValue),
      pending_explorations(QUAD_ORIDINAL_DIRECTIONS),
      exploration_idx(0) {

   }

   DiscoveryType discovered() const {
      return discovery;
   }

   bool has_been_explored() const {
      return is_explored;
   }

   bool exploration_completed() const {
      return exploration_idx == pending_explorations.size();
   }

   void discover(DiscoveryType val) {
      is_explored = true;
      discovery = val;
   }

   void mark_explored() {
      is_explored = true;
   }

   cartesian::Point2 next_exploration() {
      const auto next = pending_explorations.at(exploration_idx);
      ++exploration_idx;
      return next;
   }

   void reset_keeping_discovery() {
      is_explored = false;
      exploration_idx = 0;
   }

private:

   bool is_explored;
   DiscoveryType discovery;
   DirectionList pending_explorations;
   size_t exploration_idx;
};
