#include <algorithm>
#include <iterator>
#include <sstream>
#include "StringBuilderIntecodeComputerOutput.h"

void StringBuilderIntecodeComputerOutput::handle_output(IntcodeType output) {
   outputs.push_back(output);

}

std::string StringBuilderIntecodeComputerOutput::get_output() const {
   std::ostringstream str;
   std::copy(
      outputs.begin(),
      outputs.end(),
      std::ostream_iterator<IntcodeType>(str, ",")
   );
   return str.str();
}