#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "FuelNanoFactory.h"
#include "ReactionEquation.h"

namespace {

class Day14Prob1 : public IProblem {
public:

   virtual ~Day14Prob1() = default;

   virtual std::string name() const override {
      return "Day 14 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day14Prob2 : public IProblem {
public:

   virtual ~Day14Prob2() = default;

   virtual std::string name() const override {
      return "Day 14 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

ProblemFactoryRegisterer<Day14Prob1> _problem1("Day14Prob1");
ProblemFactoryRegisterer<Day14Prob2> _problem2("Day14Prob2");


ISolution* Day14Prob1::execute(std::istream& data_set) {

   FuelNanoFactory fuel_factory;
   std::string line;
   while(std::getline(data_set, line)) {
      fuel_factory.extract_reaction_question(line);
   }

   auto fuel = fuel_factory.get_compound("FUEL");
   auto ore = fuel_factory.get_compound("ORE");

   fuel->produce_to_satisfy(1);
   
   return new SingleValueSolution<long long>(ore->get_total_units_produced());
}


ISolution* Day14Prob2::execute(std::istream& data_set) {

   const long long ORE_MAX = 1000000000000;
   long long fuel_count = 0;

   FuelNanoFactory fuel_factory;
   std::string line;
   while(std::getline(data_set, line)) {
      fuel_factory.extract_reaction_question(line);
   }

   auto fuel = fuel_factory.get_compound("FUEL");
   auto ore = fuel_factory.get_compound("ORE");

   fuel->produce_to_satisfy(1);

   // TODO: a better heuristic for incrementing and advancing units to produce will
   // speed this up.
   long long fuel_qty = ORE_MAX / ore -> get_total_units_produced();

   while(true) {
      ++fuel_qty;
      fuel->produce_to_satisfy(fuel_qty);
      if(ore->get_total_units_produced() > ORE_MAX) {
         break;
      }

      if(ore->get_total_units_produced() < ORE_MAX / 2) {
         fuel_qty *= 2;
      } else {
         ++fuel_qty;
      }
      
      fuel_count = fuel->get_total_units_produced();
   }

   return new SingleValueSolution<long long>(fuel_count);
}

}
