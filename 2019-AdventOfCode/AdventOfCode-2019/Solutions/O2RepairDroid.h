#pragma once

//#include <Windows.h>

#include <iosfwd>
#include <stack>
#include <unordered_map>
#include "IIntcodeComputerInput.h"
#include "IIntcodeComputerOutput.h"
#include "Cartesian.h"
#include "ExplorationMap.h"

enum class RepairBotSensing {
   WALL,
   EXPLORED,
   FOUND_GOAL,
   MAP_START,
   UNEXPLORED
};


bool bot_moved_locations(RepairBotSensing sensing_result);
bool is_occupiable(RepairBotSensing value);

std::ostream& operator<<(std::ostream& str, const RepairBotSensing val);

typedef QuadDirectionExlorationRecord<RepairBotSensing, RepairBotSensing::UNEXPLORED> QuadExplorationRecord;
typedef std::unordered_map<cartesian::Point2, QuadExplorationRecord, cartesian::cantor_pairing> ExplorationMap;

//template<class Rendered_T>
//class IRenderer {
//public:
//
//   virtual ~IRenderer() = default;
//
//   virtual void render(const Rendered_T& state) = 0;
//
//};

// https://docs.microsoft.com/en-us/windows/console/console-functions
//class ConsoleDrawRenderer { // : public IRenderer<ExplorationMap> {
//public:
//
//   ConsoleDrawRenderer() : console( GetConsoleWindow() ), console_dc(GetDC(console)) {
//
//   }
//
//   virtual ~ConsoleDrawRenderer() {
//      ReleaseDC(console, console_dc);
//   }
//
//   virtual void render(const ExplorationMap& state) {
//      ::Rectangle(console_dc, 10, 10, 50, 50);
//   }
//
//private:
//
//   HWND console;
//   HDC console_dc;
//
//};


class O2RepairDroid : public IIntcodeComputerInput, public IIntcodeComputerOutput { 
public:

   O2RepairDroid();

   virtual ~O2RepairDroid() = default;

   virtual bool input_available() const;

   virtual IntcodeType get_intput();

   virtual void handle_output(IntcodeType output);

   int get_min_steps_to_repair_site();

   int get_steps_to_fill_room_with_02();

   void render(std::ostream& str) const;

private:

   ExplorationMap exploration_map;
   std::stack<cartesian::Point2> walkback_stack;

   cartesian::Point2 bot_location;
   cartesian::Point2 o2_location;
   cartesian::Point2 movement_dir;
   cartesian::Point2 exploring_location;
   
   // Used for rendering....this is common with my unordered map/set of coordinates
   int min_x;
   int max_x;
   int min_y;
   int max_y;
   
   bool repair_point_found;
   bool was_walkback;
};

