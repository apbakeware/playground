#include <algorithm>
#include <iterator>
#include <memory>
#include <numeric>
#include <string>
#include <sstream>
#include <unordered_map>
#include <vector>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "OrbitalTree.h"

namespace {

class Day6Prob1 : public IProblem {
public:

   virtual ~Day6Prob1() = default;

   virtual std::string name() const override {
      return "Day 06 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day6Prob2 : public IProblem {
public:

   virtual ~Day6Prob2() = default;

   virtual std::string name() const override {
      return "Day 06 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};


ProblemFactoryRegisterer<Day6Prob1> _problem1("Day6Prob1");
ProblemFactoryRegisterer<Day6Prob2> _problem2("Day6Prob2");



ISolution* Day6Prob1::execute(std::istream& data_set) {

   auto master_modes = create_orbital_tree(data_set);
   auto com = master_modes.find("COM");
   NodeLevels node_levels;
   dfs_generate_node_levels(*(com->second), node_levels);

   auto count = std::accumulate(
      node_levels.begin(),
      node_levels.end(),
      0,
      [](const auto&  val, const auto& pw) {
         return val + pw.second;
      }
   );

   return new SingleValueSolution<int>(count);
}


ISolution* Day6Prob2::execute(std::istream& data_set) {

   auto orbital_tree = create_orbital_tree(data_set);
   const auto my_path_string = get_orbital_path(orbital_tree, "YOU");
   const auto santa_path_string = get_orbital_path(orbital_tree, "SAN");
   
   auto diverging_point = find_first_non_common
      (my_path_string.begin(), my_path_string.end(), santa_path_string.begin());

   auto distance_me = std::distance(diverging_point.first, my_path_string.end());
   auto distance_san = std::distance(diverging_point.second, santa_path_string.end());

   return new SingleValueSolution<int>(distance_me + distance_san);
}

}
