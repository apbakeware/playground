#pragma once
#include <vector>
#include "Cartesian.h"

namespace astroid_field {

std::vector<cartesian::Point2> perform_lasing_operations
(const std::vector<cartesian::Point2>& astroid_field, 
 const cartesian::Point2 & laser_pos,
 int number_of_lases);

}