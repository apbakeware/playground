#include <iostream>
#include "ReactionEquation.h"

// number of units produced by 1 production

ReactionEquation::ReactionEquation(const std::string& name)
   : name(name), unit_quantity(1), total_units_produced(0), units_available(0) {
}

ReactionEquation::ReactionEquation(const std::string& name, long long unit_quantity)
   : name(name), unit_quantity(unit_quantity), total_units_produced(0), units_available(0) {
}

void ReactionEquation::update_unit_quantity(long long quantity) {
   unit_quantity = quantity;
}

std::string ReactionEquation::get_name() const {
   return name;
}

long long ReactionEquation::get_unit_quantity() const {
   return unit_quantity;
}

long long ReactionEquation::get_total_units_produced() const {
   return total_units_produced;
}

long long ReactionEquation::get_units_available() const {
   return units_available;
}

void ReactionEquation::add_ingredient(ReactionEquation& ingredient, long long quantity_required) {
   const auto rec = std::make_pair(&ingredient, quantity_required);
   const auto key = ingredient.get_name();
   //std::cout << "Adding ingredient: " << key << "\n";
   ingredients[key] = rec;
}

bool ReactionEquation::can_satisfy_request(long long units_requested) const {
   return units_requested <= units_available;
}

void ReactionEquation::produce_to_satisfy(long long units_required) {

   if(can_satisfy_request(units_required)) {
      return;
   }

   units_required -= units_available;

   const long long factor = units_required / unit_quantity + (units_required % unit_quantity != 0);
   //std::cout << "Producing requested " << units_required << " of " << name << " in "
   //   << factor << " group(s) of " << unit_quantity  << " {" << factor*unit_quantity << "}\n";

   for(auto ingredient : ingredients) {
      const long long qty_req = factor * std::get<1>(ingredient.second);
      auto iptr = std::get<0>(ingredient.second);
      iptr->produce_to_satisfy(qty_req);
      iptr->consume_units(qty_req);
   }

   units_available += factor * unit_quantity;
   total_units_produced += factor * unit_quantity;
}

// all or nothing consumed

bool ReactionEquation::consume_units(long long quantity) {
   if(!can_satisfy_request(quantity)) {
      return false;
   }

   units_available -= quantity;
   return true;
}

std::ostream& operator<<(std::ostream& str, const ReactionEquation& obj) {
   str << "{Reaction Equation -- Name: " << obj.name << "  unit qty: " << obj.unit_quantity
      << "  Total Produced: " << obj.total_units_produced << "  Available: " << obj.units_available
      << "  Ingredients: [ ";

   for(const auto ingredient : obj.ingredients) {
      const auto& rec = ingredient.second;
      str << std::get<0>(rec) -> name << "(" << std::get<1>(rec) << ") ";
   }
   str << "]}";

   return str;
}