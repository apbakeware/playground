#include <algorithm>
#include <numeric>
#include <string>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"


namespace {

class Day1Prob2 : public IProblem {
public:

   virtual ~Day1Prob2();

   virtual std::string name() const override;

   // Solution object?
   virtual ISolution* execute(std::istream& data_set) override;
};


ProblemFactoryRegisterer<Day1Prob2> _problem("Day1Prob2");


Day1Prob2::~Day1Prob2() {

}

std::string Day1Prob2::name() const {
   return "Day 01 - Problem 2";
}

ISolution* Day1Prob2::execute(std::istream& data_set) {
   const auto input_data = load_data<int>(data_set);
   std::vector<int> fuel_required(input_data.size(), 0);

   std::transform(
      input_data.begin(),
      input_data.end(),
      fuel_required.begin(),
      [](const auto mass) {
         int fuel = std::max((mass / 3) - 2, 0);
         int total_fuel = fuel;
         while(fuel > 0) {
            fuel = std::max((fuel / 3) - 2, 0);
            total_fuel += fuel;
         }
         return total_fuel;
      }
   );

   auto sum = std::accumulate(fuel_required.begin(), fuel_required.end(), 0);

   return new SingleValueSolution<int>(sum);
}

}
