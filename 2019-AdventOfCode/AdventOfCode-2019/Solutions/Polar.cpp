#include <cmath>
#include <iostream>
#include "Polar.h"

namespace {

const double PI = 3.14159265;

}

namespace polar {

std::ostream& operator<<(std::ostream& str, const Point2& rec) {
   str << "{POINT -- ang: " << rec.angle << "  dist: " << rec.magnitude << "}";
   return str;
}

Point2 convert_from(const cartesian::Point2& base, const cartesian::Point2& target) {
   return convert_from(base, target, 0.0);
}

Point2 convert_from(const cartesian::Point2& base, const cartesian::Point2& target, double rotation_degrees) {
   const double angle_rad = rotation_degrees * 3.14159265359 / 180.0;
   const double distance = cartesian::distance(base, target);
   const double dx = cartesian::delta_x(base, target);
   const double dy = cartesian::delta_y(base, target);
   const double angle = std::atan2(dy, dx) - angle_rad;
   return Point2{distance, std::fmod(angle * 180 / PI + 360.0, 360.0)};
}

Point2 rotate(const Point2& base, double degrees) {
   return Point2{
      base.magnitude,
      std::fmod(base.angle + degrees, 360.0)
   };
}

}