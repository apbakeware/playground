#pragma once
#include "ISolution.h"

class IntegerSolution : public ISolution {
public:

   IntegerSolution(const std::string& lable, int value);

private:

   int value;
};

