#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "IntcodeComputer.h"
#include "ArcadeGameIO.h"

namespace {

class Day13Prob1 : public IProblem {
public:

   virtual ~Day13Prob1() = default;

   virtual std::string name() const override {
      return "Day 13 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day13Prob2 : public IProblem {
public:

   virtual ~Day13Prob2() = default;

   virtual std::string name() const override {
      return "Day 13 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

ProblemFactoryRegisterer<Day13Prob1> _problem1("Day13Prob1");
ProblemFactoryRegisterer<Day13Prob2> _problem2("Day13Prob2");


ISolution* Day13Prob1::execute(std::istream& data_set) {

   std::vector<IntcodeType> input_data(100000, 0);
   auto piter = input_data.begin();
   std::string token;
   while(std::getline(data_set, token, ',')) {
      std::istringstream istr(token);
      istr >> *piter;
      ++piter;
   }

   ArcadeGameIO arcade_game_io;
   IntcodeComputer computer(input_data, arcade_game_io, arcade_game_io);
   computer.execute();
   return new SingleValueSolution<int>(arcade_game_io.get_number_of_blocks());
}


ISolution* Day13Prob2::execute(std::istream& data_set) {

   std::vector<IntcodeType> input_data(100000, 0);
   auto piter = input_data.begin();
   std::string token;
   while(std::getline(data_set, token, ',')) {
      std::istringstream istr(token);
      istr >> *piter;
      ++piter;
   }

   input_data[0] = 2;
   ArcadeGameIO arcade_game_io;
   IntcodeComputer computer(input_data, arcade_game_io, arcade_game_io);
   computer.execute();
   return new SingleValueSolution<int>(arcade_game_io.get_score());
}

}
