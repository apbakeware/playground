#pragma once
#include <string>
#include <iosfwd>
#include "ISolution.h"


class SubstringLineResult : public ISolution {
public:

   SubstringLineResult(const std::string& str, size_t substr_len);

   virtual ~SubstringLineResult() = default;

   virtual void write_result(std::ostream& outs) override;

private:
   const std::string str;
   const size_t substr_len;
};