#include <iostream>
#include "NbodyMoons.h"

namespace {

int assess_gravity(int base, int other) {
   if(base > other) {
      return -1;
   } else if(base < other) {
      return 1;
   }
   return 0;
}

}

cartesian::Point3 compute_gravity(const cartesian::Point3& base, const cartesian::Point3& other) {
   const cartesian::Point3 gravity = {
      assess_gravity(base.x, other.x),
      assess_gravity(base.y, other.y),
      assess_gravity(base.z, other.z)
   };

   return gravity;
}

std::vector<PositionVelocity> simulate_moon_movement
(const std::vector<cartesian::Point3>& origins, int num_steps) {

   std::vector<cartesian::Point3> positions = origins;
   std::vector<cartesian::Point3> velocities(positions.size());
   std::vector<cartesian::Point3> next_positions = positions;

   for(int step = num_steps; step > 0; --step) {
      for(size_t idx = 0; idx < positions.size(); ++idx) {
         velocities[idx] = velocities[idx] + compute_velocity(positions[idx], positions.begin(), positions.end());
         next_positions[idx] = positions[idx] + velocities[idx];
      }

      positions = next_positions;
   }

   std::vector<PositionVelocity> result(origins.size());
   for(size_t idx = 0; idx < result.size(); ++idx) {
      result[idx].position = positions[idx];
      result[idx].velocity = velocities[idx];
   }

   return result;
}

int compute_moon_energy(const cartesian::Point3& point) {
   return std::abs(point.x) + std::abs(point.y) + std::abs(point.z);
}

int compute_total_energy(const std::vector<PositionVelocity>& moons) {
   return std::accumulate(
      moons.begin(),
      moons.end(),
      0,
      [](int acc, const auto& moon) {
         return acc + compute_moon_energy(moon.position) * compute_moon_energy(moon.velocity);
      }
   );
}
