#pragma once

#include <utility>
#include <vector>

class FFT {
public:

   explicit FFT(int sequence_length);

   std::vector<int> apply_fft(const std::vector<int>& input, int num_phases);

   std::vector<int> compute_phase(const std::vector<int>& input);

   std::pair<std::vector<int>::const_iterator, std::vector<int>::const_iterator>
      get_element_pattern(int element);

private:

   const int fft_len;

   std::vector<std::vector<int> > patterns_to_apply;

};
