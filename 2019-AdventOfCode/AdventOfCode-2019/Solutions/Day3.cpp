#include <algorithm>
#include <numeric>
#include <string>
#include <sstream>
#include <unordered_set>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"


namespace {

class Day3Prob1 : public IProblem {
public:

   virtual ~Day3Prob1() = default;

   virtual std::string name() const override {
      return "Day 03 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day3Prob2 : public IProblem {
public:

   virtual ~Day3Prob2() = default;

   virtual std::string name() const override {
      return "Day 03 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};


ProblemFactoryRegisterer<Day3Prob1> _problem1("Day3Prob1");
ProblemFactoryRegisterer<Day3Prob2> _problem2("Day3Prob2");

struct XY {
   int x;
   int y;
};

XY operator+(const XY& lhs, const XY& rhs) {
   return {
      lhs.x + rhs.x,
      lhs.y + rhs.y
   };
}

bool operator<(const XY& lhs, const XY& rhs) {
   if(lhs.x < rhs.x) return true;
   if(rhs.x < lhs.x) return false;

   if(lhs.y < rhs.y) return true;
   return false;
}

bool operator==(const XY& lhs, const XY& rhs) {
   return &lhs == &rhs ||
      (lhs.x == rhs.x && lhs.y == rhs.y);
}


int manhatten_dist(const XY& orig, const XY& dest) {
   return std::abs(dest.x - orig.x) + std::abs(dest.y - orig.y);
}

const XY ORIGIN = {0,0};
const XY UP = {0,1};
const XY DOWN = {0,-1};
const XY RIGHT = {1,0};
const XY LEFT = {-1,0};


std::vector<XY> plot_path(const std::string path) {
   std::vector<XY> plot_path;
   std::istringstream path_stream(path);
   
   char direction;
   char comma;
   int distance;

   plot_path.push_back(ORIGIN);
   while(path_stream) {
      path_stream >> direction >> distance >> comma;
      XY movement;
      switch(direction) {
      case 'U':
         movement = UP;
         break;
      case 'R':
         movement = RIGHT;
         break;
      case 'L':
         movement = LEFT;
         break;
      case 'D':
         movement = DOWN;
         break;
      default:
         std::cout << "unknown\n";
      }

      for(; distance > 0; --distance) {
         plot_path.push_back(plot_path.back() + movement);
      }
   }

   return plot_path;
}

ISolution* Day3Prob1::execute(std::istream& data_set) {
   
   
   std::string path1;
   std::string path2;
  
   data_set >> path1 >> path2;

   std::vector<XY> plot_path_1 = plot_path(path1);
   std::vector<XY> plot_path_2 = plot_path(path2);
   std::vector<XY> common_points(std::min(plot_path_1.size(), plot_path_2.size()));
   std::sort(plot_path_1.begin(), plot_path_1.end());

   std::sort(plot_path_2.begin(), plot_path_2.end());
   auto it = std::set_intersection(
      plot_path_1.begin(),
      plot_path_1.end(),
      plot_path_2.begin(),
      plot_path_2.end(),
      common_points.begin()
   );
   
   common_points.resize(it - common_points.begin());

   const XY ORIGIN = {0,0};
   int min_dist = std::numeric_limits<int>::max();
   for(const auto& point : common_points) {
      int manh_dist = manhatten_dist(ORIGIN, point);
      if(manh_dist == 0) {
         continue;
      }

      min_dist = manh_dist < min_dist ? manh_dist : min_dist;
   }

   return new SingleValueSolution<int>(min_dist);
}


ISolution* Day3Prob2::execute(std::istream& data_set) {

   std::string path1;
   std::string path2;

   data_set >> path1 >> path2;

   const std::vector<XY> plot_path_1_orig = plot_path(path1);
   const std::vector<XY> plot_path_2_orig = plot_path(path2);
   std::vector<XY> plot_path_1(plot_path_1_orig);
   std::vector<XY> plot_path_2(plot_path_2_orig);

   std::vector<XY> common_points(std::min(plot_path_1.size(), plot_path_2.size()));
   std::sort(plot_path_1.begin(), plot_path_1.end());

   std::sort(plot_path_2.begin(), plot_path_2.end());
   auto it = std::set_intersection(
      plot_path_1.begin(),
      plot_path_1.end(),
      plot_path_2.begin(),
      plot_path_2.end(),
      common_points.begin()
   );

   common_points.resize(it - common_points.begin());

   const XY ORIGIN = {0,0};
   int min_length = std::numeric_limits<int>::max();
   for(const auto& point : common_points) {
      auto p1_id = std::find(plot_path_1_orig.begin(), plot_path_1_orig.end(), point);
      auto p2_id = std::find(plot_path_2_orig.begin(), plot_path_2_orig.end(), point);

      auto length = (p1_id - plot_path_1_orig.begin()) + (p2_id - plot_path_2_orig.begin());
      
      if(length == 0) continue;
      min_length = std::min(length, min_length);
   }

   return new SingleValueSolution<int>(min_length);
}

}
