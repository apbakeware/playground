#pragma once
#include <numeric>
#include <vector>
#include "Cartesian.h"

struct PositionVelocity {
   cartesian::Point3 position;
   cartesian::Point3 velocity;
};

/**
 * Compute the gravity between to 3D points as defined by Day12 Problem 1.
 */
cartesian::Point3 compute_gravity(const cartesian::Point3& base, const cartesian::Point3& other);

/**
 * Compute the velocity as defined by accumulating gravites as defined by Day 12 Problem 1.
 */
template<class Iter_T>
cartesian::Point3 compute_velocity(const cartesian::Point3& base, Iter_T begin, Iter_T end) {

   return std::accumulate(
      begin,
      end,
      cartesian::Point3(),
      [=](const auto& curr, const auto& element) {
         return curr + compute_gravity(base, element);
      }
   );
}

std::vector<PositionVelocity> simulate_moon_movement(const std::vector<cartesian::Point3>& origins, int num_steps);

int compute_moon_energy(const cartesian::Point3& point);

int compute_total_energy(const std::vector<PositionVelocity>& moons);
