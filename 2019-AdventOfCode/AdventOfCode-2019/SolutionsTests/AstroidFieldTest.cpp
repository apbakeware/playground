#include "gtest/gtest.h"
#include "../Solutions/AstroidField.h"
#include "../Solutions/Cartesian.h"

namespace {

const cartesian::Point2 LASER = {11,13};

const std::vector<std::string> TEST_DATA = {
   ".#..##.###...#######",
   "##.############..##.",
   ".#.######.########.#",
   ".###.#######.####.#.",
   "#####.##.#.##.###.##",
   "..#####..#.#########",
   "####################",
   "#.####....###.#.#.##",
   "##.#################",
   "#####.##.###..####..",
   "..######..##.#######",
   "####.##.####...##..#",
   ".#####..#.######.###",
   "##...#.##########...",
   "#.##########.#######",
   ".####.#.###.###.#.##",
   "....##.##.###..#####",
   ".#.#.###########.###",
   "#.#.#.#####.####.###",
   "###.##.####.##.#..##"
};

}

TEST(AstroidFieldLaseTest, SampleInputLaseTestAstroid1   ) {

   const cartesian::Point2 expected = {11,12};

   auto astroids = cartesian::create_points_from_grid(TEST_DATA, '#');

   const auto & laze_seq = astroid_field::perform_lasing_operations
      (astroids, LASER, 1);

   ASSERT_EQ(expected, laze_seq.back());
}

TEST(AstroidFieldLaseTest, SampleInputLaseTestAstroid2) {

   const cartesian::Point2 expected = {12,1};

   auto astroids = cartesian::create_points_from_grid(TEST_DATA, '#');

   const auto& laze_seq = astroid_field::perform_lasing_operations
   (astroids, LASER, 2);

   ASSERT_EQ(expected, laze_seq.back());
}