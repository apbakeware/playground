#include "gtest/gtest.h"
#include "../Solutions/FFT.h"

#include <algorithm>
#include <sstream>
#include <string>
#include <vector>

namespace {


template<class Iter_T>
std::string to_string(Iter_T begin, Iter_T end) {
   typedef typename std::iterator_traits<Iter_T>::value_type value_type;

   std::ostringstream str;
   std::copy(begin, end, std::ostream_iterator<value_type>(str, ", "));
   return str.str();
}


}

TEST(FFT, GetPhaseSequenceForSizeEqualToSequenceLengthElement1) {

   const std::vector<int> exp = {1,0,-1,0};
   FFT sut(exp.size());
   const auto pattern = sut.get_element_pattern(1);
   
   ASSERT_EQ(
      to_string(exp.begin(), exp.end()), 
      to_string(pattern.first, pattern.second)
   );
}

TEST(FFT, GetPhaseSequenceForSizeLessThanSequenceLengthElement1) {

   const std::vector<int> exp = {1,0};
   FFT sut(exp.size());
   const auto pattern = sut.get_element_pattern(1);

   ASSERT_EQ(
      to_string(exp.begin(), exp.end()),
      to_string(pattern.first, pattern.second)
   );
}

TEST(FFT, GetPhaseSequenceForSizeGreaterThanSequenceLengthElement1) {

   const std::vector<int> exp = {1,0,-1,0,1,0,-1};
   FFT sut(exp.size());
   const auto pattern = sut.get_element_pattern(1);

   ASSERT_EQ(
      to_string(exp.begin(), exp.end()),
      to_string(pattern.first, pattern.second)
   );
}
TEST(FFT, GetPhaseSequenceForSizeEqualToSequenceLengthElement2) {

   const std::vector<int> exp = {0,1,1,0};
   FFT sut(exp.size());
   const auto pattern = sut.get_element_pattern(2);

   ASSERT_EQ(
      to_string(exp.begin(), exp.end()),
      to_string(pattern.first, pattern.second)
   );
}

TEST(FFT, GetPhaseSequenceForSizeGreaterThanSequenceLengthElement8) {

   const std::vector<int> exp = {0,0,0,0,0,0,0,1};
   FFT sut(exp.size());
   const auto pattern = sut.get_element_pattern(8);

   ASSERT_EQ(
      to_string(exp.begin(), exp.end()),
      to_string(pattern.first, pattern.second)
   );
}

TEST(FFT, ComputePhase1_AOC_Test_Case) {
   const std::vector<int> input = {1,2,3,4,5,6,7,8};
   const std::vector<int> exp = {4,8,2,2,6,1,5,8};
   FFT sut(input.size());

   const auto phase_result = sut.compute_phase(input);

   ASSERT_EQ(
      to_string(exp.begin(), exp.end()),
      to_string(phase_result.begin(), phase_result.end())
   );
}

TEST(FFT, ApplyFFT_1_Phase_AOC_Test_Case) {
   const std::vector<int> input = {1,2,3,4,5,6,7,8};
   const std::vector<int> exp = {4,8,2,2,6,1,5,8};
   
   FFT sut(input.size());

   const auto phase_result = sut.apply_fft(input, 1);

   ASSERT_EQ(
      to_string(exp.begin(), exp.end()),
      to_string(phase_result.begin(), phase_result.end())
   );
}

TEST(FFT, ApplyFFT_2_Phase_AOC_Test_Case) {
   const std::vector<int> input = {1,2,3,4,5,6,7,8};
   const std::vector<int> exp = {3,4,0,4,0,4,3,8};

   FFT sut(input.size());

   const auto phase_result = sut.apply_fft(input, 2);

   ASSERT_EQ(
      to_string(exp.begin(), exp.end()),
      to_string(phase_result.begin(), phase_result.end())
   );
}

TEST(FFT, ApplyFFT_4_Phase_AOC_Test_Case) {
   const std::vector<int> input = {1,2,3,4,5,6,7,8};
   const std::vector<int> exp = {0,1,0,2,9,4,9,8};

   FFT sut(input.size());

   const auto phase_result = sut.apply_fft(input, 4);

   ASSERT_EQ(
      to_string(exp.begin(), exp.end()),
      to_string(phase_result.begin(), phase_result.end())
   );
}

TEST(FFT, Day16_First_8_Char_Test_Case_1) {

   const std::vector<int> input = {8,0,8,7,1,2,2,4,5,8,5,9,1,4,5,4,6,6,1,9,0,8,3,2,1,8,6,4,5,5,9,5};
   std::vector<int> fft_input_values(input.size());
   FFT fft(input.size());

   std::transform(
      input.begin(),
      input.end(),
      fft_input_values.begin(),
      [](const auto val) {
         return static_cast<int>(val);
      }
   );

   const auto& fft_result = fft.apply_fft(fft_input_values, 100);

   std::string first_8_digits;
   for(size_t idx = 0; idx < 8; ++idx) {
      first_8_digits.append(std::to_string(fft_result[idx]));
   }

   ASSERT_EQ("24176176", first_8_digits);
}

TEST(FFT, Day16_First_8_Char_Test_Case_2) {

   const std::vector<int> input = {1,9,6,1,7,8,0,4,2,0,7,2,0,2,2,0,9,1,4,4,9,1,6,0,4,4,1,8,9,9,1,7};
   std::vector<int> fft_input_values(input.size());
   FFT fft(input.size());

   std::transform(
      input.begin(),
      input.end(),
      fft_input_values.begin(),
      [](const auto val) {
         return static_cast<int>(val);
      }
   );

   const auto& fft_result = fft.apply_fft(fft_input_values, 100);

   std::string first_8_digits;
   for(size_t idx = 0; idx < 8; ++idx) {
      first_8_digits.append(std::to_string(fft_result[idx]));
   }

   ASSERT_EQ("73745418", first_8_digits);
}

TEST(FFT, Day16_First_8_Char_Test_Case_3) {

   const std::vector<int> input = {6,9,3,1,7,1,6,3,4,9,2,9,4,8,6,0,6,3,3,5,9,9,5,9,2,4,3,1,9,8,7,3};
   std::vector<int> fft_input_values(input.size());
   FFT fft(input.size());

   std::transform(
      input.begin(),
      input.end(),
      fft_input_values.begin(),
      [](const auto val) {
         return static_cast<int>(val);
      }
   );

   const auto& fft_result = fft.apply_fft(fft_input_values, 100);

   std::string first_8_digits;
   for(size_t idx = 0; idx < 8; ++idx) {
      first_8_digits.append(std::to_string(fft_result[idx]));
   }

   ASSERT_EQ("52432133", first_8_digits);
}