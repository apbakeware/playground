#include "gtest/gtest.h"
#include "../Solutions/Cartesian.h"




TEST(CartesianTest, PointDistanceTest) {
   const cartesian::Point2 p1 = {0,0};
   const cartesian::Point2 p2 = {1,1};
   const double exp = std::sqrt(2.0);

   ASSERT_DOUBLE_EQ(cartesian::distance(p1, p2), exp);
}
TEST(CartesianTest, SlopeTest) {
   const cartesian::Point2 p1 = {5,7};
   const cartesian::Point2 p2 = {7,8};
   const cartesian::Slope2 exp = { 2,1,0.5 };

   ASSERT_EQ(cartesian::slope(p1, p2), exp);
}
TEST(CartesianTest, AreCollinearTestWithNonCollinearPoints) {
   const cartesian::Point2 p1 = {1,1};
   const cartesian::Point2 p2 = {2,2};
   const cartesian::Point2 p3 = {3,4};

   ASSERT_FALSE(cartesian::are_collinear(p1, p2, p3));
}
TEST(CartesianTest, AreCollinearTestWithCollinearPoints) {
   const cartesian::Point2 p1 = {1,1};
   const cartesian::Point2 p2 = {2,2};
   const cartesian::Point2 p3 = {4,4};

   ASSERT_TRUE(cartesian::are_collinear(p1, p2, p3));
}

TEST(CartesianTest, AreCollinearTestWithReorderedCollinearPoints) {
   const cartesian::Point2 p1 = {1,1};
   const cartesian::Point2 p2 = {2,2};
   const cartesian::Point2 p3 = {4,4};

   ASSERT_TRUE(cartesian::are_collinear(p2, p1, p3));
}
TEST(CartesianTest, TestGetLOSPointsVertical) {
   const std::vector<cartesian::Point2> points = {
      {4,0},
      {4,2},
      {4,3},
      {4,4}
   };

   auto poinst_by_dist = cartesian::points_sorted_by_distance_from({4,0}, points);
   auto los_points = cartesian::get_los_points({4,0}, poinst_by_dist);

   std::copy(
      los_points.begin(), 
      los_points.end(), 
      std::ostream_iterator<cartesian::Point2>(std::cout, "\n")
   );

   ASSERT_EQ(los_points.size(), 1) << "{4,0} Test";
}
TEST(CartesianTest, TestGetLOSPointsCrosses) {

   const std::vector<cartesian::Point2> points = {
      {0,0},
      {1,1},
      {2,2},
      {3,1},
      {3,1},
      {1,3}
   };

   auto poinst_by_dist = cartesian::points_sorted_by_distance_from({ 1,1 }, points);
   auto los_points = cartesian::get_los_points({ 1,1 }, poinst_by_dist);

   ASSERT_EQ(los_points.size(), 4) << "{1,1} Test";
}

TEST(CartesianTest, TestDay10InputList) {
   const std::vector<std::string> sut = {
      ".#..#",
      ".....",
      "#####",
      "....#",
      "...##"
   };

   const std::vector<cartesian::Point2> exp = {
      {1,0},
      {4,0},
      {0,2},
      {1,2},
      {2,2},
      {3,2},
      {4,2},
      {4,3},
      {3,4},
      {4,4}
   };

   auto points = cartesian::create_points_from_grid(sut, '#');
   EXPECT_EQ(points, exp);
}

TEST(CartesianTest, TestDay10Sample1LOSCounts) {
   const std::vector<std::string> sut = {
      ".#..#",
      ".....",
      "#####",
      "....#",
      "...##"
   };

   const auto points = cartesian::create_points_from_grid(sut, '#');
   std::vector<cartesian::DistanceToPoint> points_by_dist;
   std::vector<cartesian::Point2> los_points;

   points_by_dist = cartesian::points_sorted_by_distance_from({1,0}, points);
   los_points = cartesian::get_los_points({1,0}, points_by_dist);
   ASSERT_EQ(los_points.size(), 7) << "{1,0} Test";

   points_by_dist = cartesian::points_sorted_by_distance_from({4,0}, points);
   los_points = cartesian::get_los_points({4,0}, points_by_dist);
   ASSERT_EQ(los_points.size(), 7) << "{4,0} Test";

   points_by_dist = cartesian::points_sorted_by_distance_from({0,2}, points);
   los_points = cartesian::get_los_points({0,2}, points_by_dist);
   ASSERT_EQ(los_points.size(), 6) << "{0,2} Test";

   points_by_dist = cartesian::points_sorted_by_distance_from({1,2}, points);
   los_points = cartesian::get_los_points({1,2}, points_by_dist);
   ASSERT_EQ(los_points.size(), 7) << "{1,2} Test";

   points_by_dist = cartesian::points_sorted_by_distance_from({2,2}, points);
   los_points = cartesian::get_los_points({2,2}, points_by_dist);
   ASSERT_EQ(los_points.size(), 7) << "{2,2} Test";

   points_by_dist = cartesian::points_sorted_by_distance_from({3,4}, points);
   los_points = cartesian::get_los_points({3,4}, points_by_dist);
   ASSERT_EQ(los_points.size(), 8) << "{3,4} Test";
}

TEST(CartesianTest, TestDay10TestCase1) {
   const std::vector<std::string> sut = {
      ".#..#",
      ".....",
      "#####",
      "....#",
      "...##"
   };

   const cartesian::Point2 EXP = {3,4};

   const auto points = cartesian::create_points_from_grid(sut, '#');
   const auto best_choice = cartesian::find_point_with_greatest_los_count(points);

   EXPECT_EQ(std::get<0>(best_choice), EXP);
   EXPECT_EQ(std::get<1>(best_choice), 8);
}