﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// Interface for building concrete SolverExecutor objects based
    /// on a solver Identifier.
    /// </summary>
    public interface ISolverExecutorFactory
    {
        public SolverExecutor CreateSolver(
            IDataCollectorFactory dataCollectorFactory,
            SolverIdentifier solverIdentifier
        );
    }
}
