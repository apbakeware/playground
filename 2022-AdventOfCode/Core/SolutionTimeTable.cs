﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class SolutionTimeTable : ISolutionRenderer
    {
        public void Render(List<SolutionTime> solutions)
        {
            TimeSpan totalTime = new TimeSpan();
            Console.WriteLine(" SOLUTIONS ");
            Console.WriteLine("+==================+============+=================+");
            Console.WriteLine("| Time             | Problem    | Solution        |");
            Console.WriteLine("+------------------+------------+-----------------|");
            foreach (var soln in solutions)
            {
                RenderRow(soln);
                totalTime += soln.TotalDuration;
            }
            Console.WriteLine("+------------------+------------+-----------------+");
            Console.WriteLine(String.Format("| Total Time: {0,10}                    |", totalTime));
            Console.WriteLine("+==================+============+=================+");
        }

        public void RenderRow(SolutionTime solutionTime)
        {
            Console.WriteLine(
                String.Format("| {0,10} | {1,-10} | {2,15} |",
                    solutionTime.TotalDuration,
                    solutionTime.Solution.ProblemName,
                    solutionTime.Solution.Solution()
                )
            );
        }
    }

}
