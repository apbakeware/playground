﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Exceptions
{
    public class FactoryException : Exception
    {
        public FactoryException()
        {
        }

        public FactoryException(string message)
            : base(message)
        {
        }

        public FactoryException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
