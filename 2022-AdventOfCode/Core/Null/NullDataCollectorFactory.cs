﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class NullDataCollectorFactory : IDataCollectorFactory
    {
        public IDataCollector CreateDataCollector(SolverIdentifier solverIdentifier)
        {
            return new NullDataCollector();
        }
    }
}
