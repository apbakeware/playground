namespace Core
{
    public class NullSolution : ISolution
    {
        public string ProblemName => "Null";

        public string Solution() => "NullSolution";
    }
}
