﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// Implementation of the IDataCollection interface
    /// to read all lines from a text file.
    /// </summary>
    public class FileDataCollector : IDataCollector
    {
        protected ILogger logger;
        private readonly string _fileName;

        public FileDataCollector(string fname)
        {
            logger = Log.ForContext<FileDataCollector>();
            _fileName = fname;
        }

        public List<string> GetData()
        {
            logger.Information("Reading content from file: {Filename}", _fileName);
            var lines = File.ReadAllLines(_fileName).ToList<string>();
            logger.Information("Read {NumLines} lines", lines.Count);
            return lines;
        }
    }
}
