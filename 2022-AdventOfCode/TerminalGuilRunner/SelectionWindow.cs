﻿using Core;
using Terminal.Gui;
using Serilog;

// Tasks:
//  Only enable execute button if there is more than 1 list item click
//  Render output
//  Execute selected solvers

namespace TerminalGuilRunner
{
    internal class SelectionWindow
    {
        public Window? Win { get; set; }
        
        public event EventHandler<ExecutionRequestEventArgs> ExecuteSolvers;

        private ILogger logger;
        private Button btnExecute;
        private ListView lstView;
        private List<SolverIdentifier> lstViewData;

        public SelectionWindow()
        {
            logger = Log.ForContext<SelectionWindow>();
        }

        public void Init(Pos WinX, Pos WinY, Dim WinWidth, Dim WinHeight)
        {
            Win = new Window("Solver Selection")
            {
                X = WinX,
                Y = WinY,
                Width = WinWidth,
                Height = WinHeight
            };

            // TODO: Come from enumerable
            lstViewData = new()
            {
                SolverIdentifier.DayTest,
                SolverIdentifier.Day01Part01,
                SolverIdentifier.Day01Part02,
                SolverIdentifier.Day02Part01,
                SolverIdentifier.Day02Part02,
                SolverIdentifier.Day03Part01,
                SolverIdentifier.Day03Part02,
                SolverIdentifier.Day04Part01,
                SolverIdentifier.Day04Part02,
                SolverIdentifier.Day05Part01,
                SolverIdentifier.Day05Part02,
                SolverIdentifier.Day06Part01,
                SolverIdentifier.Day06Part02,
                SolverIdentifier.Day07Part01,
                SolverIdentifier.Day07Part02,
                SolverIdentifier.Day08Part01,
                SolverIdentifier.Day08Part02,
                SolverIdentifier.Day09Part01,
                SolverIdentifier.Day09Part02,
                SolverIdentifier.Day10Part01,
                SolverIdentifier.Day10Part02,
                SolverIdentifier.Day11Part01,
                SolverIdentifier.Day11Part02,
                SolverIdentifier.Day12Part01,
                SolverIdentifier.Day12Part02,
                SolverIdentifier.Day13Part01,
                SolverIdentifier.Day13Part02,
                SolverIdentifier.Day14Part01,
                SolverIdentifier.Day14Part02,
                SolverIdentifier.Day15Part01,
                SolverIdentifier.Day15Part02,
                SolverIdentifier.Day16Part01,
                SolverIdentifier.Day16Part02,
                SolverIdentifier.Day17Part01,
                SolverIdentifier.Day17Part02,
                SolverIdentifier.Day18Part01,
                SolverIdentifier.Day18Part02,
                SolverIdentifier.Day19Part01,
                SolverIdentifier.Day19Part02,
                SolverIdentifier.Day20Part01,
                SolverIdentifier.Day20Part02,
                SolverIdentifier.Day21Part01,
                SolverIdentifier.Day21Part02,
                SolverIdentifier.Day22Part01,
                SolverIdentifier.Day22Part02,
                SolverIdentifier.Day23Part01,
                SolverIdentifier.Day23Part02,
                SolverIdentifier.Day24Part01,
                SolverIdentifier.Day24Part02,
                SolverIdentifier.Day25Part01,
                SolverIdentifier.Day25Part02
            };

            SolverIdentiferListDataSource lstViewSrc = new()
            {
                SolverIdentifiers = lstViewData
            };

            lstView = new(lstViewSrc)
            {
                X = 1,
                Y = 1,
                Width = Dim.Fill(),
                Height = Dim.Percent(60.0f)
            };
            lstView.AllowsMarking = true;

            var btnExecuteSample = new Button("Sample")
            {
                X = 1,
                Y = Pos.Bottom(lstView) + 2,
                Width = Dim.Fill(),
                Height = 1
            };
            btnExecuteSample.Clicked += HandleExecuteTestButton;
            btnExecuteSample.Enabled = false;

            btnExecute = new("Execute")
            {
                X = 1,
                Y = Pos.Bottom(btnExecuteSample),
                Width = Dim.Fill(),
                Height = 1
            };
            btnExecute.Clicked += HandleExecuteButton;
            btnExecute.Enabled = false;

            Button btnSelectAll = new("Select All")
            {
                X = 1,
                Y = Pos.Bottom(btnExecute),
                Width = Dim.Fill(),
                Height = 1
            };
            btnSelectAll.Clicked += SelectAllSolvers;

            Button btnUnselectAll = new("Unselect All")
            {
                X = 1,
                Y = Pos.Bottom(btnSelectAll),
                Width = Dim.Fill(),
                Height = 1
            };
            btnUnselectAll.Clicked += UnselectAllSolvers;

            lstViewSrc.MarksUpdated += (object? sender, MarksUpdatedEventArgs e) => btnExecute.Enabled = e.NumberOfRowsMarked > 0;
            lstViewSrc.MarksUpdated += (object? sender, MarksUpdatedEventArgs e) => btnExecuteSample.Enabled = e.NumberOfRowsMarked > 0;

            Win.Add(lstView, btnExecuteSample, btnExecute, btnSelectAll, btnUnselectAll);
            CreateListViewScrollBars();
        }

        private void CreateListViewScrollBars()
        {
            // TODO: not rendering last item
            var _scrollBar = new ScrollBarView(lstView, true);

            _scrollBar.ChangedPosition += () =>
            {
                lstView.TopItem = _scrollBar.Position;
                if (lstView.TopItem != _scrollBar.Position)
                {
                    _scrollBar.Position = lstView.TopItem;
                }
                lstView.SetNeedsDisplay();
            };

            _scrollBar.OtherScrollBarView.ChangedPosition += () =>
            {
                lstView.LeftItem = _scrollBar.OtherScrollBarView.Position;
                if (lstView.LeftItem != _scrollBar.OtherScrollBarView.Position)
                {
                    _scrollBar.OtherScrollBarView.Position = lstView.LeftItem;
                }
                lstView.SetNeedsDisplay();
            };

            lstView.DrawContent += (e) =>
            {
                _scrollBar.Size = lstView.Source.Count - 1;
                _scrollBar.Position = lstView.TopItem;
                _scrollBar.OtherScrollBarView.Size = lstView.Maxlength - 1;
                _scrollBar.OtherScrollBarView.Position = lstView.LeftItem;
                _scrollBar.Refresh();
            };
        }

        private void SelectAllSolvers()
        {
            for(int idx = 0; idx < lstView.Source.Count; idx++)
            {
                lstView.Source.SetMark(idx, true);
            }
        }

        private void UnselectAllSolvers()
        {
            for (int idx = 0; idx < lstView.Source.Count; idx++)
            {
                lstView.Source.SetMark(idx, false);
            }
        }

        private void HandleExecuteTestButton()
        {
            DoExecute(true);
        }

        private void HandleExecuteButton()
        {
            DoExecute(false);
        }

        private void DoExecute(bool usesSampleData)
        {
            logger.Debug("Handling execute button");
            List<SolverIdentifier> solversToExecute = new();
            var selectedIds = lstView.Source.ToList();
            for (int idx = 0; idx < lstView.Source.Count; idx++)
            {
                if (lstView.Source.IsMarked(idx))
                {
                    // TODO: figure out which to add.
                    // Cool new inline out variable
                    Enum.TryParse(selectedIds[idx].ToString(), out SolverIdentifier solverIdentifer);
                    logger.Debug("Selected .ToString(): {Val}", selectedIds[idx].ToString());
                    solversToExecute.Add(solverIdentifer);
                }
            }

            if (solversToExecute.Count > 0)
            {
                ExecutionRequestEventArgs args = new()
                {
                    SolverIdentifiers = solversToExecute,
                    UseSampleData = usesSampleData
                };
                OnRaiseExecutionRequest(args);
            }
        }

        private void OnRaiseExecutionRequest(ExecutionRequestEventArgs e)
        {
            ExecuteSolvers(this, e);
        }
    }
}