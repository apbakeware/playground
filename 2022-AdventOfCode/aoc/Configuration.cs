﻿using Microsoft.Extensions.Configuration;

namespace AOC
{
    public static class Configuration
    {
        public static IConfigurationRoot BuildAppConfig(IConfigurationBuilder builder)
        {
            return builder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

        }
    }
}
