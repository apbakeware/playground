﻿using Solvers2022;
using Solvers2022.Support;
using Xunit.Abstractions;

namespace Solvers2022Test.Support
{
    public class ElfCrtDisplayTest : UnitTestLoggingBase
    {
        private ElfCrtDisplay sut;

        public ElfCrtDisplayTest(ITestOutputHelper output) : base(output)
        {
            sut = new();
        }

        [Fact]
        public void TestNoOp()
        {
            sut.ExecuteNoop();

            sut.CycleNumber.Should().Be(1);
            sut.Value.Should().Be(1);
        }

        [Fact]
        public void TestAdd3FinalCycleAndValue()
        {
            sut.ExecuteAdd(3);

            sut.CycleNumber.Should().Be(2);
            sut.Value.Should().Be(4);
        }

        [Fact]
        public void TestNoopAdd3AddN5Test()
        {
            sut.ExecuteNoop();
            sut.ExecuteAdd(3);
            sut.ExecuteAdd(-5);

            sut.CycleNumber.Should().Be(5);
            sut.Value.Should().Be(-1);
        }
    }
}
