﻿using Solvers2022.Support.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;
using FluentAssertions;

namespace Solvers2022Test.Support.Grid
{
    public class CoordinateUtilsTest
    {
        public class DiamondNeighborsTest : UnitTestLoggingBase
        {
            public DiamondNeighborsTest(ITestOutputHelper output) : base(output)
            {
            }

            [Fact]
            public void TestBasicCaseSize()
            {
                Coord center = new(3,3);
                int distance = 1;

                var sut = CoordinateUtils.GetDiamondNeighbors(center, distance);

                sut.Should().HaveCount(c => c == 4);
            }

            [Fact]
            public void TestBasicCaseDoesNotContainCenterPoint()
            {
                Coord center = new(3, 3);
                int distance = 1;

                var sut = CoordinateUtils.GetDiamondNeighbors(center, distance);

                sut.Should().NotContain(center);
            }

            [Fact]
            public void TestBasicCaseSequence()
            {
                List<Coord> expected = new()
                {
                    new Coord(3,2),
                    new Coord(2,3),
                    new Coord(4,3),
                    new Coord(3,4)
                };

                Coord center = new(3, 3);
                int distance = 1;

                var sut = CoordinateUtils.GetDiamondNeighbors(center, distance);

                sut.Should().Equal(expected);
            }
        }
    }
}
