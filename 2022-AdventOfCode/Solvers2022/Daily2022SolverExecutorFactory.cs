﻿using Core;
using Core.Exceptions;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Solvers2022
{
    public class Daily2022SolverExecutorFactory : ISolverExecutorFactory
    {
        public SolverExecutor CreateSolver(IDataCollectorFactory dataCollectorFactory, SolverIdentifier solverIdentifier)
        {
            if (dataCollectorFactory is null)
            {
                throw new FactoryException("Null parameter!",
                    new ArgumentNullException(nameof(dataCollectorFactory)));       
            }

            return new SolverExecutor(
                CreateDataCollector(dataCollectorFactory, solverIdentifier),
                CreateSolver(solverIdentifier.ToString())
            );
        }

        private static IDataCollector CreateDataCollector(IDataCollectorFactory dataCollectorFactory, SolverIdentifier solverIdentifier)
        {
            IDataCollector dataCollector = dataCollectorFactory.CreateDataCollector(solverIdentifier);
            if (dataCollector == null)
            {
                throw new FactoryException($"DataCollector is Null for SolverIdentifier: {solverIdentifier.ToString()}");
            }

            return dataCollector;
        }

        private static ISolver CreateSolver(string solverName)
        {
            Log.Debug("CreateSolver( {SolverName} )", solverName);
            var assembly = Assembly.GetExecutingAssembly();
            string fullName = $"{assembly.GetName().Name}.{solverName}";
            Type? solverType = assembly.GetType(fullName);

            if (solverType == null)
            {
                Log.Error("Solver type for '{SolverName}' is null", solverName);
                throw new FactoryException($"Could not find assembly type: {fullName}");
            }

            if (Activator.CreateInstance(solverType) is not ISolver solver)
            {
                Log.Error("Instance of '{SolverName}' is null", solverName);
                throw new FactoryException($"Could not activate instance of: {fullName}");
            }

            return solver;
        }
    }
}
