﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solvers2022.Support.Grid
{
    /// <summary>
    /// This class provides a suite of coordinate transformations
    /// to translate a coordinate. The relative direction methods
    /// assume a coordinate grid with 0,0 in the upper left.
    /// </summary>
    public static class CoordinateTranslation
    {
        public static Coord Translate(Coord coord, int dist_x, int dist_y)
        {
            return new Coord( coord.X + dist_x, coord.Y + dist_y );
        }

        public static Coord Up(Coord coord, int dist = 1)
        {
            return Translate(coord, 0, -1 * dist);
        }

        public static Coord Right(Coord coord, int dist = 1)
        {
            return Translate(coord, dist, 0);
        }

        public static Coord Down(Coord coord, int dist = 1)
        {
            return Translate(coord, 0, dist);
        }

        public static Coord Left(Coord coord, int dist = 1)
        {
            return Translate(coord, -1 * dist, 0);
        }
    }
}
