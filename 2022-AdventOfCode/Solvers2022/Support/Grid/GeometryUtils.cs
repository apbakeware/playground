﻿using  Serilog;

namespace Solvers2022.Support.Grid
{
    // Very basic geometry / linear algebra toolkit centered on Coord
    public static class GeometryUtils
    {
        // Assumes points in clockwise orientation
        //If any point is "left" of the line segments, its outside the polygon
        public static bool PointEnclosedByConvexPolygon(List<Coord> verticies, Coord point)
        {
            Coord start = verticies[^1];

            foreach(var vert in verticies)
            {
                int pointTest = PointLineTest(start, vert, point);
                if(pointTest < 0)
                {
                    return false;
                }
                start = vert;
            }

            return true;
        }

        // Line from A(x1,y1) to B(x2,y2) and test point P(x,y)
        //
        // Derived from "Math for Programmers"
        // 
        // d = (x-x1)(y2-y1)-(y-y1)(x2-x1)
        // 1: left side of line "looking from start to end"
        // -1: right side
        // 0: on the line
        public static int PointLineTest(Coord start, Coord end, Coord point)
        {
            int dot = (point.X - start.X) * (end.Y - start.Y) - (point.Y - start.Y) * (end.X - start.X);
            return Math.Sign(dot);
        }
    }

}
