﻿using Serilog;
using System.Text;

namespace Solvers2022.Support.Grid
{
    // TODO:
    //  - Create a fixed sized grid which cant grow
    //  - Ensure this grid has consistent number of columns
    //  - Optimize for boolean with BitArray

    public class Grid<T>
    {
        public int RowCount { get => grid.Count; }
        public int ColumnCount { get; private set; }

        private ILogger logger;
        private List<T[]> grid;

        public List<T[]> GridRows { get => grid; }


        // Top left is 0,0 
        // Use ReverseRows to invert Y indicies

        public Grid()
        {
            ColumnCount = 0;
            logger = Log.ForContext<Grid<T>>();
            grid = new List<T[]>();
        }

        public Grid(int numRows, int numCols, T defaultVal = default(T))
        {
            ColumnCount = numCols;
            logger = Log.ForContext<Grid<T>>();
            grid = new List<T[]>();

            for(int row = 0; row < numRows; row++)
            {
                T[] rowValues = new T[numCols];
                Array.Fill<T>(rowValues, defaultVal);
                grid.Add(rowValues);
            }
        }


        // First row is 0 which would be "top"...reverse to make it like a coordinte axis
        public void ReverseRows()
        {
            grid.Reverse();
        }

        public T[] AddRow(T[] row)
        {
            ColumnCount = row.Length;
            grid.Add(row);
            return row;
        }

        public T[] AddRow(int num_columns)
        {
            var row = new T[num_columns];
            return AddRow(row);
        }

        // Unchecked access....with throw if invalid values
        public T At(Coord coord)
        {
            return grid[coord.Y][coord.X];
        }

        public bool TryAt(Coord coord, out T value)
        {
            if(!OnGrid(coord))
            {
                value = default(T);
                return false;
            }

            value = At(coord);
            return true;
        }

        // Unchecked access
        public void Set(Coord coord, T value)
        {
            grid[coord.Y][coord.X] = value;
        }

        public bool TrySet(Coord coord, T value)
        {
            if(!OnGrid(coord))
            {
                return false;
            }
            Set(coord, value);
            return true;
        }

        public bool OnGrid(Coord coord)
        {
            return 
                coord.X >= 0 && 
                coord.X < ColumnCount &&
                coord.Y >= 0 &&
                coord.Y < RowCount;
        }

        public IEnumerable<Coord> EachLocation()
        {
            for(int y = 0; y < RowCount; y++)
            {
                for(int x = 0; x < ColumnCount; x++)
                {
                    yield return new Coord(x, y);
                }
            }
        }

        // Renders top down (0,0) at lower coordinates
        public override string ToString()
        {
            StringBuilder myStringBuilder = new StringBuilder("\n");
            foreach(var row in grid)
            { 
                foreach(var val in row)
                {
                    myStringBuilder.Append(val.ToString());
                }
                //myStringBuilder.Append(grid[index].ToString());
                myStringBuilder.Append("\n");
            }
            return myStringBuilder.ToString();
        }
    }
}
