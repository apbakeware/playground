﻿using Serilog;
using System.Collections;

namespace Solvers2022.Support.Grid
{
    public class BreadthFirstMinPathFinder<T>
    {
        private ILogger logger;
        private Grid<T> grid;
        private Queue<Coord> nodeQueue;

        // Indexed by Coordinate applied to IndexFor
        private BitArray visits;
        private int[] travelDistance;

        public Func<T, T, bool> TraversalPermissiblePrediate { get; set; }

        public event EventHandler<PathFoundEvent> PathFound;
        public BreadthFirstMinPathFinder(Grid<T> theGrid)
        {
            logger = Log.ForContext<BreadthFirstMinPathFinder<T>>();
            grid = theGrid;
            nodeQueue = new Queue<Coord>();

            int expandedSize = grid.RowCount * grid.ColumnCount;
            visits = new(expandedSize, false);
            travelDistance = new int[expandedSize];
        }

        public int FindShortestPath(Coord start, Coord end)
        {
            bool endFound = false;
            int indexForNode = IndexFor(start);

            Array.Clear(travelDistance);
            nodeQueue.Clear();
            visits.SetAll(false);

            nodeQueue.Enqueue(start);
            travelDistance[indexForNode] = 0;

            while (nodeQueue.Count > 0 && !endFound)
            {
                var node = nodeQueue.Dequeue();
                indexForNode = IndexFor(node);

                if (node == end)
                {
                    return travelDistance[indexForNode];
                }

                foreach (var neighbor in CoordinateUtils.Neighbors4(node))
                {
                    if (!grid.OnGrid(neighbor))
                    {
                        continue;
                    }

                    int neighborIndex = IndexFor(neighbor);
                    bool doVisit =
                        visits[neighborIndex] == false &&
                        TraversalPermissiblePrediate(grid.At(node), grid.At(neighbor));

                    if (doVisit)
                    {
                        visits[neighborIndex] = true;
                        travelDistance[neighborIndex] = travelDistance[indexForNode] + 1;
                        nodeQueue.Enqueue(neighbor);
                    }
                }
            }

            return int.MaxValue;
        }

        private bool HasBeenVisited(Coord testLoc)
        {
            return visits[IndexFor(testLoc)];
        }

        private void SetVisited(Coord testLoc, bool value)
        {
            visits[IndexFor(testLoc)] = value;
        }

        private int IndexFor(Coord coord)
        {
            return coord.Y * grid.ColumnCount + coord.X;
        }
    }
}
