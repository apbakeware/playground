﻿using System;
using Serilog;
using Serilog.Context;
using Core;

namespace Solvers2022
{
    public class DayTest : ISolver
    {
        protected ILogger logger;

        public DayTest()
        {
            logger = Log.ForContext<DayTest>();
        }

        public void Ingest(List<string> input)
        {
            logger.Warning("Injest data: {Injest}", input);
        }

        public ISolution Solve()
        {
            return new SingleValueSolution<string>("DayTest", "This is a test of the solver system");
        }
    }
}
