using System;
using Serilog;
using Serilog.Context;
using Core;
using System.Collections.Generic;

namespace Solvers2022
{
    public class Day01Part02 : ISolver
    {
        protected ILogger logger;
        private List<string> data;

        public Day01Part02()
        {
            logger = Log.ForContext<Day01Part02>();
        }

        public void Ingest(List<string> input)
        {
            logger.Debug("Ingest() -- Data: {Input}", input);
            data = input.ToList<string>();
        }
    
        public ISolution Solve()
        {
            int elfCount = 0;
            List<int> elfCalorieCounts = new();

            foreach (var row in data)
            {
                if (row.Length == 0)
                {
                    logger.Debug("New elf!!!");
                    elfCalorieCounts.Add(elfCount);
                    elfCount = 0;
                }
                else
                {
                    elfCount += int.Parse(row);
                    logger.Debug("Elf count now: {Count}", elfCount);
                }
            }

            elfCalorieCounts.Sort();
            int total = elfCalorieCounts[^1] + elfCalorieCounts[^2] + elfCalorieCounts[^3];

            return new SingleValueSolution<int>("01-02", total);
        }
    }
  }    
