using System;
using Serilog;
using Serilog.Context;
using Core;
using System.Diagnostics.Metrics;
using FluentAssertions.Formatting;
using System.Text;

namespace Solvers2022
{
    public class CrcCycleEvent : EventArgs
    {
        public int CycleNumber { get; set; }
        public int Value { get; set; }

        public CrcCycleEvent(int cycleNumber, int value)
        {
            CycleNumber = cycleNumber;
            Value = value;
        }
    }


    public class ElfCrtDisplay
    {
        public int CycleNumber { get; set; }
        public int Value { get; set; }

        public event EventHandler<CrcCycleEvent> RaiseCycleStartEvent;
        public event EventHandler<CrcCycleEvent> RaiseCycleInProgressEvent;
        public event EventHandler<CrcCycleEvent> RaiseCycleCompleteEvent;

        private ILogger logger;

        public ElfCrtDisplay()
        {
            logger = Log.ForContext<ElfCrtDisplay>();
            CycleNumber = 0;
            Value = 1;
        }

        public void ExecuteNoop()
        {
            CycleNumber++;
            TriggerCrtCycleStartEvent();
            TriggerCrtCycleInProgressEvent();
            TriggerCrtCycleCompleteEvent();
        }

        public void ExecuteAdd(int value)
        {
            // First cycle
            ExecuteNoop();

            // During 2nd cycle
            CycleNumber++;
            TriggerCrtCycleStartEvent();
            TriggerCrtCycleInProgressEvent();

            // 2nd Cycle Complete
            Value += value;
            TriggerCrtCycleCompleteEvent();
        }

        private void TriggerCrtCycleStartEvent()
        {
            CrcCycleEvent cycleEvent = new(CycleNumber, Value);
            RaiseCycleInProgressEvent?.Invoke(this, cycleEvent);
        }

        private void TriggerCrtCycleInProgressEvent()
        {
            CrcCycleEvent cycleEvent = new(CycleNumber, Value);
            RaiseCycleInProgressEvent?.Invoke(this, cycleEvent);
        }

        private void TriggerCrtCycleCompleteEvent()
        {
            CrcCycleEvent cycleEvent = new(CycleNumber, Value);
            RaiseCycleCompleteEvent?.Invoke(this, cycleEvent);
        }
    }

    public abstract class Day10 : ISolver
    {
        protected List<string> data;
        protected ElfCrtDisplay elfCrtDisplay;

        private ILogger logger;

        public Day10()
        {
            logger = Log.ForContext<Day10>();
            data = new();
            elfCrtDisplay = new();
        }

        public void Ingest(List<string> input)
        {
            data = input;
        }

        public abstract ISolution Solve();
    }

    public class Day10Part01 : Day10
    {
        private ILogger logger;
        private int signalStrength;

        public Day10Part01()
        {
            logger = Log.ForContext<Day10Part01>();
        }

        public override ISolution Solve()
        {
            signalStrength = 0;
            elfCrtDisplay.RaiseCycleInProgressEvent += ElfCrtDisplay_RaiseCycleInProgressEvent;
            foreach (var row in data)
            {
                var parts = row.Split(" ");
                if (parts[0] == "noop")
                {
                    elfCrtDisplay.ExecuteNoop();
                }
                else if (parts[0] == "addx")
                {
                    elfCrtDisplay.ExecuteAdd(int.Parse(parts[1]));
                }
            }

            elfCrtDisplay.RaiseCycleInProgressEvent -= ElfCrtDisplay_RaiseCycleInProgressEvent;
            return new SingleValueSolution<int>("10-01", signalStrength);
        }

        private void ElfCrtDisplay_RaiseCycleInProgressEvent(object? sender, CrcCycleEvent e)
        {
            logger.Warning("Handling in progress event: {Cycle} : {Value}", e.CycleNumber, e.Value);
            if(e.CycleNumber == 20)
            {
                UpdateSignalStrength(e.CycleNumber, e.Value);
            }
            else if((e.CycleNumber - 20) % 40 == 0 )
            {
                UpdateSignalStrength(e.CycleNumber, e.Value);
            }
        }

        private void UpdateSignalStrength(int crtCycleNumber, int crtValue)
        {
            signalStrength += crtCycleNumber * crtValue;
            logger.Debug("Updated Signal strength -- Strength: {Strength}  Cycle: {Cycle}  Value: {Value}",
                signalStrength, crtCycleNumber, crtValue);
        }
    }

    public class Day10Part02 : Day10
    {
        private ILogger logger;

        private char[,] pixels;
        private int pixelCol;
        private int pixelRow;

        public Day10Part02()
        {
            logger = Log.ForContext<Day10Part02>();
            pixels = new char[6,40];
        }

        public override ISolution Solve()
        {
            elfCrtDisplay.RaiseCycleInProgressEvent += ElfCrtDisplay_HandlePhaseEvent;

            pixelCol = 0;
            pixelRow = 0;
            for (int row = 0; row < 6; row++)
            {
                for(int col = 0; col < 40; col++)
                {
                    pixels[row, col] = ' ';
                }
            }

            foreach (var row in data)
            {
                var parts = row.Split(" ");
                if (parts[0] == "noop")
                {
                    elfCrtDisplay.ExecuteNoop();
                }
                else if (parts[0] == "addx")
                {
                    elfCrtDisplay.ExecuteAdd(int.Parse(parts[1]));
                }
            }

            elfCrtDisplay.RaiseCycleInProgressEvent -= ElfCrtDisplay_HandlePhaseEvent;

            StringBuilder builder = new StringBuilder("\n");
            for (int row = 0; row < 6; row++)
            {
                for (int col = 0; col < 40; col++)
                {
                    builder.Append(pixels[row, col]);
                }
                builder.Append("\n");
                logger.Warning("{S}", builder.ToString());
            }

            return new SingleValueSolution<string>("10-02", builder.ToString());
        }

        private void ElfCrtDisplay_HandlePhaseEvent(object? sender, CrcCycleEvent e)
        {
            int idx = e.CycleNumber - 1;
            pixelCol = idx % 40;
            pixelRow = idx / 40;

            logger.Debug("Cycle: {Cycle}  X: {X}", e.CycleNumber, e.Value);

            if( pixelCol >= e.Value - 1 && pixelCol <= e.Value + 1)
            {
                logger.Debug("Marking cell");
                pixels[pixelRow,pixelCol] = '#';
            }
        }

    }
}
