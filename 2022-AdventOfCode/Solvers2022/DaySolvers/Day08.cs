using System;
using Serilog;
using Serilog.Context;
using Core;
using System.Security.Cryptography.X509Certificates;
using System.Reflection.Metadata.Ecma335;
using System.Collections;
using System.Runtime.CompilerServices;
using Solvers2022.Support.Grid;

namespace Solvers2022
{

    // Checklist:
    //  * Implement data variable type
    //  * Implement Injest
    //  * Implement Solve for each class
    //  * Extract classes into dedicated .cs files

    // TODO: Create similar to GridLib library coordinate and translations class

    // Rectangular grid of values. The number of colums is set when the first row is added
    


    public abstract class Day08 : ISolver
    {
        protected Support.Grid.Grid<int> data;

        private ILogger logger;

        public Day08()
        {
            logger = Log.ForContext<Day08>();
            data = new();
        }

        public void Ingest(List<string> input)
        {
            foreach(var input_row in input)
            {
                var char_list = new List<char>(input_row).ConvertAll(x => x.ToString());
                var grid_row = char_list.ConvertAll(x => int.Parse(x)).ToArray();
                logger.Debug("Grid row: {Row}", grid_row);
                data.AddRow(grid_row);
            }
        }

        public abstract ISolution Solve();
    }

    public class Day08Part01 : Day08
    {
        private ILogger logger;


        public Day08Part01()
        {
            logger = Log.ForContext<Day08Part01>();
        }

        public override ISolution Solve()
        {
            // Start with the perimeter trees
            int numVisibleTrees = 2 * data.ColumnCount + 2 * data.RowCount - 4;

            logger.Debug("The Grid: {grid}", data);
            data.ReverseRows();
            for(int y = 1; y < data.RowCount - 1; y++) 
            {
                for(int x = 1; x < data.ColumnCount - 1; x++)
                {
                    Support.Grid.Coord loc = new Support.Grid.Coord(x, y);
                    bool isVisible = true;
                    int currentTreeHeight = data.At(loc);
                    logger.Debug("Testing Cell: {Loc}: {Val}", loc, currentTreeHeight);
                    isVisible =
                        IsVisibleFrom(loc, currentTreeHeight, CoordinateTranslation.Up) ||
                        IsVisibleFrom(loc, currentTreeHeight, CoordinateTranslation.Right) ||
                        IsVisibleFrom(loc, currentTreeHeight, CoordinateTranslation.Down) ||
                        IsVisibleFrom(loc, currentTreeHeight, CoordinateTranslation.Left);

                    numVisibleTrees += isVisible ? 1 : 0;
                }
            }

            logger.Debug("Number of visible trees: {Num}", numVisibleTrees);


            return new SingleValueSolution<int>("08-01", numVisibleTrees);
        }

        private bool IsVisibleFrom(Coord loc, int currentTreeHeight, Func<Coord, int, Coord> mover)
        {
            Coord testLoc = mover(loc, 1);
            bool isVisible = true;
            int testTreeHeight = 0;
            while(data.TryAt(testLoc, out testTreeHeight) && isVisible)
            {
                isVisible = currentTreeHeight > testTreeHeight;
                logger.Debug("Is visible from {Loc}? {Vis}", testLoc, isVisible);
                testLoc = mover(testLoc, 1);
            }
            logger.Debug("\n");
            

            return isVisible;
        }
    }

    public class Day08Part02 : Day08
    {
        private ILogger logger;

        public Day08Part02()
        {
            logger = Log.ForContext<Day08Part02>();
        }

        public override ISolution Solve()
        {
            int scenicScore = -1;
            data.ReverseRows();
            for (int y = 1; y < data.RowCount - 1; y++)
            {
                for (int x = 1; x < data.ColumnCount - 1; x++)
                {
                    int localScenic = GetCellScenicScore(y, x);

                    scenicScore = Math.Max(scenicScore, localScenic);
                }
            }

            //int localScenic = GetCellScenicScore(3,2);

            logger.Debug("Number of visible trees: {Num}", scenicScore);


            return new SingleValueSolution<int>("08-02", scenicScore);
        }

        private int GetCellScenicScore(int y, int x)
        {
            Support.Grid.Coord loc = new Support.Grid.Coord(x, y);
            int currentTreeHeight = data.At(loc);
            logger.Debug("Testing Cell: {Loc}: {Val}", loc, currentTreeHeight);

            int localScenic =
                ScenicCountInDirection(loc, currentTreeHeight, CoordinateTranslation.Up) *
                ScenicCountInDirection(loc, currentTreeHeight, CoordinateTranslation.Right) *
                ScenicCountInDirection(loc, currentTreeHeight, CoordinateTranslation.Down) *
                ScenicCountInDirection(loc, currentTreeHeight, CoordinateTranslation.Left);
            return localScenic;
        }

        private int ScenicCountInDirection(Coord loc, int currentTreeHeight, Func<Coord, int, Coord> mover)
        {
            Coord testLoc = mover(loc, 1);
            bool blocksLos = false;
            int testTreeHeight = 0;
            int score = 0;
            while (data.TryAt(testLoc, out testTreeHeight) && !blocksLos)
            {
                blocksLos =  testTreeHeight >= currentTreeHeight;
                score++;
                logger.Debug("Blocks los {Height} from {Loc}? {Vis}", testTreeHeight, testLoc, blocksLos);
                testLoc = mover(testLoc, 1);
            }
            logger.Debug("Direction Score: {Score}", score);


            return score;
        }
    }
}
