using System;
using Serilog;
using Serilog.Context;
using Core;
using Solvers2022.Support;

namespace Solvers2022
{
    public class Day03Part01 : ISolver
    {
        protected ILogger logger;
        private List<(AlphabeticCharacterMask, AlphabeticCharacterMask)> data;

        public Day03Part01()
        {
            logger = Log.ForContext<Day03Part01>();
            data = new();
        }

        public void Ingest(List<string> input)
        {
            foreach (var line in input)
            {
                int halfLen = line.Length / 2;
                AlphabeticCharacterMask firstMask = new( line.Substring(0, halfLen) );
                AlphabeticCharacterMask secondOneMask = new(line.Substring(halfLen));
                data.Add((firstMask, secondOneMask));
            }
        }
    
        public ISolution Solve()
        {
            int value = 0;
            foreach(var masks in data)
            {
                var common = AlphabeticCharacterMask.CommonCharacters(masks.Item1, masks.Item2);
                if(common.Count > 1)
                {
                    logger.Error("Unexpected data! only 1 character should be common found: {Num}", common.Count);
                    throw new Core.Exceptions.SolverException("Unexpected data processing!");
                }

                value += AlphabeticCharacterMask.IndexFor(common[0]) + 1;
            }

           return new SingleValueSolution<int>("03-01", value);
        }
    }
  }    
