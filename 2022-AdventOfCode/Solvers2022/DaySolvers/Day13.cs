  using System;
  using Serilog;
  using Serilog.Context;
  using Core;
  
  namespace Solvers2022
  {
  
      // Checklist:
      //  * Implement data variable type
      //  * Implement Injest
      //  * Implement Solve for each class
      //  * Extract classes into dedicated .cs files
  
      public abstract class Day13 : ISolver
      {
        // TODO: Update data collection type
        protected List<string> data;
  
        private ILogger logger;
  
        public Day13()
        {
            logger = Log.ForContext<Day13>();
            data = new();
        }
  
        public void Ingest(List<string> input)
        {
            // TODO: Update Injest
            data = input.ConvertAll( x => x );
        }
  
        public abstract ISolution Solve();
      }
  
      public class Day13Part01 : Day13
      {
          private ILogger logger;
  
          public Day13Part01()
          {
            logger = Log.ForContext<Day13Part01>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("13-01", "Unimplemented");
          }
      }
  
      public class Day13Part02 : Day13
      {
          private ILogger logger;
  
          public Day13Part02()
          {
            logger = Log.ForContext<Day13Part02>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("13-02", "Unimplemented");
          }
      }
    }    
