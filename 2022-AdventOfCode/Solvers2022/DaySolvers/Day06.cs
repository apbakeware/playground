using System;
using Serilog;
using Serilog.Context;
using Core;
using System.Collections;

namespace Solvers2022
{

    // Checklist:
    //  * Implement data variable type
    //  * Implement Injest
    //  * Implement Solve for each class
    //  * Extract classes into dedicated .cs files

    public class CommunicationDeviceDecoder
    {
        // todo try an int array counting each char.
        // After the first substring, then just remove head and add the new tail, maintaining the middle
        //   tried bit array but loses duplicates
        private BitArray charsDetected;
        private string message;

        public CommunicationDeviceDecoder(string message)
        {
            this.charsDetected = new(26);
            this.message = message;
        }

        /// <summary>
        /// Find the first occurance of a run of unique characters
        /// in the string. Return the ending position of the unique 
        /// run.
        /// </summary>
        /// <param name="fragmentLength">Number of consecutive unique characters</param>
        /// <returns>Position in message of the end of the unique fragment, -1 if no segment found</returns>
        public int FindUniqueFragment(int fragmentLength)
        {
            // index by char - 'A'
            for (int idx = 0; idx < message.Length; idx++)
            {
                int endIdx = idx + fragmentLength;
                bool isSOFFragment = IsStartOfFrameSequence(message[idx..endIdx]);

                // Because this identifies the start of the frame, need the last, since we need all the characters
                if (isSOFFragment) return endIdx;

            }
            return -1;
        }

        public static int IndexFor(char theChar)
        {
            return (int)(theChar - 'a');
        }

        /// <summary>
        /// Returns true if all the characters in the parameter list is unique.
        /// </summary>
        /// <param name="msgChars">Variable length of char parameters.</param>
        /// <returns></returns>
        public bool IsStartOfFrameSequence(string msgChars)
        {
            // Sure there are optimizations here to clear out the
            // first one if it fails and keep the others set?
            charsDetected.SetAll(false);
            foreach (char msgChar in msgChars)
            {
                int indexForChar = IndexFor(msgChar);
                if (charsDetected[indexForChar])
                {
                    return false;
                }
                charsDetected.Set(indexForChar, true);
            }

            return true;
        }
    }

    public abstract class Day06 : ISolver
    {
        protected CommunicationDeviceDecoder decoder;

        private ILogger logger;

        public Day06()
        {
            logger = Log.ForContext<Day06>();
        }

        public void Ingest(List<string> input)
        {
            decoder = new(input[0]);
        }

        public abstract ISolution Solve();
    }

    public class Day06Part01 : Day06
    {
        private ILogger logger;

        public Day06Part01()
        {
            logger = Log.ForContext<Day06Part01>();
        }

        public override ISolution Solve()
        {
            int sofLoc = decoder.FindUniqueFragment(4);
            return new SingleValueSolution<int>("06-01", sofLoc);
        }
    }

    public class Day06Part02 : Day06
    {
        private ILogger logger;

        public Day06Part02()
        {
            logger = Log.ForContext<Day06Part02>();
        }

        public override ISolution Solve()
        {
            int sofLoc = decoder.FindUniqueFragment(14);
            return new SingleValueSolution<int>("06-02", sofLoc);
        }
    }
}
