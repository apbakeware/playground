using System;
using Serilog;
using Serilog.Context;
using Core;
using Solvers2022.Support;

namespace Solvers2022
{
    public class Day03Part02 : ISolver
    {
        protected ILogger logger;
        private List<AlphabeticCharacterMask> data;

        public Day03Part02()
        {
          logger = Log.ForContext<Day03Part02>();
          data = new();
        }

        public void Ingest(List<string> input)
        {
            data = input.ConvertAll<AlphabeticCharacterMask>( x => new AlphabeticCharacterMask(x));
        }
    
        public ISolution Solve()
        {
            int value = 0;
            for (int idx = 0; idx < data.Count; idx += 3)
            {
                var common = AlphabeticCharacterMask.CommonCharacters(
                    data[idx], data[idx+1], data[idx+2] );

                if (common.Count > 1)
                {
                    logger.Error("Unexpected data! only 1 character should be common found: {Num}", common.Count);
                    throw new Core.Exceptions.SolverException("Unexpected data processing!");
                }

                value += AlphabeticCharacterMask.IndexFor(common[0]) + 1;
            }

           return new SingleValueSolution<int>("03-02", value);
        }
    }
  }    
