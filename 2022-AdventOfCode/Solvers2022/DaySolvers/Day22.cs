  using System;
  using Serilog;
  using Serilog.Context;
  using Core;
  
  namespace Solvers2022
  {
  
      // Checklist:
      //  * Implement data variable type
      //  * Implement Injest
      //  * Implement Solve for each class
      //  * Extract classes into dedicated .cs files
  
      public abstract class Day22 : ISolver
      {
        // TODO: Update data collection type
        protected List<string> data;
  
        private ILogger logger;
  
        public Day22()
        {
            logger = Log.ForContext<Day22>();
            data = new();
        }
  
        public void Ingest(List<string> input)
        {
            // TODO: Update Injest
            data = input.ConvertAll( x => x );
        }
  
        public abstract ISolution Solve();
      }
  
      public class Day22Part01 : Day22
      {
          private ILogger logger;
  
          public Day22Part01()
          {
            logger = Log.ForContext<Day22Part01>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("22-01", "Unimplemented");
          }
      }
  
      public class Day22Part02 : Day22
      {
          private ILogger logger;
  
          public Day22Part02()
          {
            logger = Log.ForContext<Day22Part02>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("22-02", "Unimplemented");
          }
      }
    }    
