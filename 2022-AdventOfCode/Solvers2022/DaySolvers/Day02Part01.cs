using System;
using Serilog;
using Serilog.Context;
using Core;
using System.Runtime.CompilerServices;
using Solvers2022.Support;

namespace Solvers2022
{
    public class Day02Part01 : ISolver
    {
        protected ILogger logger;
        private List<(RockPaperScissors, RockPaperScissors)> data;

        public Day02Part01()
        {
            logger = Log.ForContext<Day02Part01>();
            data = new();
        }

        public void Ingest(List<string> input)
        {
            foreach (var row in input)
            {
                var split = row.Split(' ');
                data.Add((RockPaperScissorsOps.FromChar(split[0][0]), RockPaperScissorsOps.FromChar(split[1][0])));
            }
        }

        public ISolution Solve()
        {
            int totalScore = 0;
            foreach(var round in data)
            {
                int roundScore = RockPaperScissorsOps.ScoreRound(round.Item1, round.Item2);
                totalScore += roundScore;
            }

            return new SingleValueSolution<int>("02-02", totalScore);
        }
    }
}

