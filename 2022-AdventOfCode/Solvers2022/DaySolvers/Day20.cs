  using System;
  using Serilog;
  using Serilog.Context;
  using Core;
  
  namespace Solvers2022
  {
  
      // Checklist:
      //  * Implement data variable type
      //  * Implement Injest
      //  * Implement Solve for each class
      //  * Extract classes into dedicated .cs files
  
      public abstract class Day20 : ISolver
      {
        // TODO: Update data collection type
        protected List<string> data;
  
        private ILogger logger;
  
        public Day20()
        {
            logger = Log.ForContext<Day20>();
            data = new();
        }
  
        public void Ingest(List<string> input)
        {
            // TODO: Update Injest
            data = input.ConvertAll( x => x );
        }
  
        public abstract ISolution Solve();
      }
  
      public class Day20Part01 : Day20
      {
          private ILogger logger;
  
          public Day20Part01()
          {
            logger = Log.ForContext<Day20Part01>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("20-01", "Unimplemented");
          }
      }
  
      public class Day20Part02 : Day20
      {
          private ILogger logger;
  
          public Day20Part02()
          {
            logger = Log.ForContext<Day20Part02>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("20-02", "Unimplemented");
          }
      }
    }    
